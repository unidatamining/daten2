package dataminingpreprocessor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Scanner;

import org.json.JSONObject;

public class CSVSplitter {

	private static int	error;
	private static long	lineCount	= 0;
	private static int	split		= 0;

	public static void main(final String[] args) throws FileNotFoundException {
		final Scanner in = new Scanner(new File("F:/git/daten/redux.csv"));
		PrintStream out = new PrintStream(new File("F:/git/daten/reduxSplit" + split + ".csv"));
		out.append("author;signature;subreddit;body\n");
		String line = null;
		int i = 0;
		if(in.hasNextLine()){
			in.nextLine();
		}
		while (in.hasNextLine()) {
			try {
				if (lineCount % 100000 == 0 && lineCount != 0) {
					out.close();
					split++;
					out = new PrintStream(new File("F:/git/daten/reduxSplit" + split + ".csv"));
					out.append("author;signature;subreddit;body\n");
				}
				lineCount++;
				i++;
				if (i % 1000 == 0) {
					System.out.println(i + " " + CSVSplitter.error);
				}
				 line = in.nextLine();
//				System.out.println("line"+line);
				final String[] strline = (String[]) line.split("\";\"");
//				final String name = strline[0];
				final String author = strline[1];
				final String signature = strline[2];
				final String subreddit = strline[3];
				final String body = strline[4];
//				final String author_flair_css_class = strline[5];

				out.append("\"");
//				out.append(name.replace('\n', ' '));
//				out.append("\";");
//				out.append("\"");
				out.append(author.replace('\n', ' '));
				out.append("\";");
				out.append("\"");
				out.append(signature.replace('\n', ' '));
				out.append("\";");
				out.append("\"");
				out.append(subreddit.replace('\n', ' '));
				out.append("\";");
				out.append("\"");
				out.append(body.replace('\n', ' '));
//				out.append("\";");
//				out.append("\"");
//				out.append(author_flair_css_class.replace('\n', ' '));
				out.append("\"\n");
			} catch (final Exception e) {
				e.printStackTrace();
				System.out.println("line"+line);
				CSVSplitter.error++;
			}
		}
		out.close();
		in.close();

	}
}
