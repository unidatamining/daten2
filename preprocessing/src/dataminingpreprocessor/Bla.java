package dataminingpreprocessor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Scanner;

import org.json.JSONObject;

public class Bla {

	private static int	error;
	private static long	lineCount	= 0;
	private static int	split		= 0;

	public static void main(final String[] args) throws FileNotFoundException {
		final Scanner in = new Scanner(new File("F:/git/daten/dedupped-1-comment-per-line.json"));
		PrintStream out = new PrintStream(new File("F:/git/daten/reduxSplit" + split + ".csv"));
		out.append("author;subreddit;body\n");
		int i = 0;
		while (in.hasNextLine()) {
			try {
				if (lineCount % 100000 == 0 && lineCount != 0) {
					out.close();
					split++;
					out = new PrintStream(new File("F:/git/daten/reduxSplit" + split + ".csv"));
					out.append("author;subreddit;body\n");
				}
				lineCount++;
				i++;
//				if (i % 1000 == 0) {
//					System.out.println(i + " " + Bla.error);
//				}
				final String line = in.nextLine();
				final JSONObject lineobject = new JSONObject(line);
				final String author = lineobject.get("author").toString();
				final String subreddit = lineobject.get("subreddit").toString();
				final String body = lineobject.get("body").toString();

				out.append("\"");
				out.append(author.replace('\n', ' '));
				out.append("\";");
				out.append("\"");
				out.append(subreddit.replace('\n', ' '));
				out.append("\";");
				out.append("\"");
				out.append(body.replace('\n', ' '));
				out.append("\"\n");
			} catch (final Exception e) {
				e.printStackTrace();
				Bla.error++;
			}
		}
		out.close();
		in.close();

	}
}
