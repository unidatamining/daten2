\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Preprocessing}{2}{chapter.2}
\contentsline {section}{\numberline {2.1}Preprocessing of the reddit data}{2}{section.2.1}
\contentsline {section}{\numberline {2.2}Processing of the Preprocessed Data}{3}{section.2.2}
\contentsline {chapter}{\numberline {3}RapidMiner}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}Without Text Mining}{5}{section.3.1}
\contentsline {section}{\numberline {3.2}With Text Mining}{7}{section.3.2}
