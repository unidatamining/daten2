<object-stream>
  <TreeModel id="1" serialization="custom">
    <com.rapidminer.operator.AbstractIOObject>
      <default>
        <source>Decision Tree</source>
      </default>
    </com.rapidminer.operator.AbstractIOObject>
    <com.rapidminer.operator.ResultObjectAdapter>
      <default>
        <annotations id="2">
          <keyValueMap id="3"/>
        </annotations>
      </default>
    </com.rapidminer.operator.ResultObjectAdapter>
    <com.rapidminer.operator.AbstractModel>
      <default>
        <headerExampleSet id="4" serialization="custom">
          <com.rapidminer.operator.ResultObjectAdapter>
            <default>
              <annotations id="5">
                <keyValueMap id="6">
                  <entry>
                    <string>Source</string>
                    <string>//NewLocalRepository/paper/RedditExampleSet300-300</string>
                  </entry>
                </keyValueMap>
              </annotations>
            </default>
          </com.rapidminer.operator.ResultObjectAdapter>
          <com.rapidminer.example.set.AbstractExampleSet>
            <default>
              <idMap id="7"/>
              <statisticsMap id="8"/>
            </default>
          </com.rapidminer.example.set.AbstractExampleSet>
          <com.rapidminer.example.set.HeaderExampleSet>
            <default>
              <attributes class="SimpleAttributes" id="9">
                <attributes class="linked-list" id="10">
                  <AttributeRole id="11">
                    <special>false</special>
                    <attribute class="PolynominalAttribute" id="12" serialization="custom">
                      <com.rapidminer.example.table.AbstractAttribute>
                        <default>
                          <annotations id="13">
                            <keyValueMap id="14"/>
                          </annotations>
                          <attributeDescription id="15">
                            <name>author</name>
                            <valueType>5</valueType>
                            <blockType>1</blockType>
                            <defaultValue>0.0</defaultValue>
                            <index>0</index>
                          </attributeDescription>
                          <constructionDescription>author</constructionDescription>
                          <statistics class="linked-list" id="16">
                            <NominalStatistics id="17">
                              <mode>-1</mode>
                              <maxCounter>0</maxCounter>
                            </NominalStatistics>
                            <UnknownStatistics id="18">
                              <unknownCounter>0</unknownCounter>
                            </UnknownStatistics>
                          </statistics>
                          <transformations id="19"/>
                        </default>
                      </com.rapidminer.example.table.AbstractAttribute>
                      <PolynominalAttribute>
                        <default>
                          <nominalMapping class="PolynominalMapping" id="20">
                            <symbolToIndexMap id="21">
                              <entry>
                                <string>Sharkbear_McPunchy</string>
                                <int>12</int>
                              </entry>
                              <entry>
                                <string>Fatalstryke</string>
                                <int>407</int>
                              </entry>
                              <entry>
                                <string>hershey5914</string>
                                <int>294</int>
                              </entry>
                              <entry>
                                <string>Juan_Solo</string>
                                <int>472</int>
                              </entry>
                              <entry>
                                <string>RocketCow</string>
                                <int>239</int>
                              </entry>
                              <entry>
                                <string>ficarra1002</string>
                                <int>459</int>
                              </entry>
                              <entry>
                                <string>drunkschitzo</string>
                                <int>39</int>
                              </entry>
                              <entry>
                                <string>Eliza_Douchecanoe</string>
                                <int>230</int>
                              </entry>
                              <entry>
                                <string>lazy_elf</string>
                                <int>391</int>
                              </entry>
                              <entry>
                                <string>Jack57d</string>
                                <int>395</int>
                              </entry>
                              <entry>
                                <string>NickN3v3r</string>
                                <int>21</int>
                              </entry>
                              <entry>
                                <string>MyFriendsCallMeSir</string>
                                <int>49</int>
                              </entry>
                              <entry>
                                <string>simonkm</string>
                                <int>197</int>
                              </entry>
                              <entry>
                                <string>TXBear</string>
                                <int>88</int>
                              </entry>
                              <entry>
                                <string>Xraging</string>
                                <int>149</int>
                              </entry>
                              <entry>
                                <string>LadyTronLadyTron</string>
                                <int>289</int>
                              </entry>
                              <entry>
                                <string>wiicontroller</string>
                                <int>80</int>
                              </entry>
                              <entry>
                                <string>Zep_Rocko</string>
                                <int>156</int>
                              </entry>
                              <entry>
                                <string>Joshuncool</string>
                                <int>220</int>
                              </entry>
                              <entry>
                                <string>Deep_Black_Joe</string>
                                <int>292</int>
                              </entry>
                              <entry>
                                <string>dontjustthrowitaway</string>
                                <int>578</int>
                              </entry>
                              <entry>
                                <string>petard</string>
                                <int>135</int>
                              </entry>
                              <entry>
                                <string>SpelignErrir</string>
                                <int>276</int>
                              </entry>
                              <entry>
                                <string>CM_Cali</string>
                                <int>327</int>
                              </entry>
                              <entry>
                                <string>lexani4</string>
                                <int>447</int>
                              </entry>
                              <entry>
                                <string>IAmJaxRagingBoner</string>
                                <int>97</int>
                              </entry>
                              <entry>
                                <string>WhatUpTits</string>
                                <int>336</int>
                              </entry>
                              <entry>
                                <string>EndOfDaysss</string>
                                <int>387</int>
                              </entry>
                              <entry>
                                <string>RoflCow123</string>
                                <int>302</int>
                              </entry>
                              <entry>
                                <string>kayemme</string>
                                <int>208</int>
                              </entry>
                              <entry>
                                <string>deepaktiwarii</string>
                                <int>225</int>
                              </entry>
                              <entry>
                                <string>leetahsuntoucher</string>
                                <int>215</int>
                              </entry>
                              <entry>
                                <string>rrelationshipsthro</string>
                                <int>166</int>
                              </entry>
                              <entry>
                                <string>clanso</string>
                                <int>351</int>
                              </entry>
                              <entry>
                                <string>joshthehappy</string>
                                <int>532</int>
                              </entry>
                              <entry>
                                <string>contrarian</string>
                                <int>168</int>
                              </entry>
                              <entry>
                                <string>crackshot91</string>
                                <int>357</int>
                              </entry>
                              <entry>
                                <string>mxnoob983</string>
                                <int>250</int>
                              </entry>
                              <entry>
                                <string>dragonstalking</string>
                                <int>58</int>
                              </entry>
                              <entry>
                                <string>dUdEmAnBrOcUz</string>
                                <int>547</int>
                              </entry>
                              <entry>
                                <string>redduser4x</string>
                                <int>371</int>
                              </entry>
                              <entry>
                                <string>_DanNYC_</string>
                                <int>575</int>
                              </entry>
                              <entry>
                                <string>tsgmob</string>
                                <int>59</int>
                              </entry>
                              <entry>
                                <string>SER_GIGASCHLONG</string>
                                <int>248</int>
                              </entry>
                              <entry>
                                <string>jebsta1</string>
                                <int>123</int>
                              </entry>
                              <entry>
                                <string>give_le_a_chance</string>
                                <int>228</int>
                              </entry>
                              <entry>
                                <string>FUCK_YOU_DERRICK</string>
                                <int>232</int>
                              </entry>
                              <entry>
                                <string>majinboom</string>
                                <int>301</int>
                              </entry>
                              <entry>
                                <string>NikeSB6</string>
                                <int>41</int>
                              </entry>
                              <entry>
                                <string>jorgelukas</string>
                                <int>540</int>
                              </entry>
                              <entry>
                                <string>ROCK-KNIGHT</string>
                                <int>229</int>
                              </entry>
                              <entry>
                                <string>I_did_not_bow</string>
                                <int>304</int>
                              </entry>
                              <entry>
                                <string>KYLEBISH</string>
                                <int>120</int>
                              </entry>
                              <entry>
                                <string>princess-misandry</string>
                                <int>535</int>
                              </entry>
                              <entry>
                                <string>ParanoidAndroidMan</string>
                                <int>516</int>
                              </entry>
                              <entry>
                                <string>rapgamecheryltunt</string>
                                <int>33</int>
                              </entry>
                              <entry>
                                <string>Tulabean</string>
                                <int>369</int>
                              </entry>
                              <entry>
                                <string>Iamafrayedknot</string>
                                <int>321</int>
                              </entry>
                              <entry>
                                <string>Koofua</string>
                                <int>318</int>
                              </entry>
                              <entry>
                                <string>danielvutran</string>
                                <int>278</int>
                              </entry>
                              <entry>
                                <string>BenignBanan</string>
                                <int>479</int>
                              </entry>
                              <entry>
                                <string>TwoOfSwords</string>
                                <int>400</int>
                              </entry>
                              <entry>
                                <string>ShadNuke</string>
                                <int>577</int>
                              </entry>
                              <entry>
                                <string>clarke187</string>
                                <int>158</int>
                              </entry>
                              <entry>
                                <string>bacon_beer_sports</string>
                                <int>455</int>
                              </entry>
                              <entry>
                                <string>ToastedWonder</string>
                                <int>482</int>
                              </entry>
                              <entry>
                                <string>doubleodoug</string>
                                <int>567</int>
                              </entry>
                              <entry>
                                <string>k0mbine</string>
                                <int>116</int>
                              </entry>
                              <entry>
                                <string>usmc50cal</string>
                                <int>216</int>
                              </entry>
                              <entry>
                                <string>ImmortalTec</string>
                                <int>231</int>
                              </entry>
                              <entry>
                                <string>Vlayue</string>
                                <int>122</int>
                              </entry>
                              <entry>
                                <string>washedrug</string>
                                <int>518</int>
                              </entry>
                              <entry>
                                <string>yeats666</string>
                                <int>340</int>
                              </entry>
                              <entry>
                                <string>epiclogin</string>
                                <int>503</int>
                              </entry>
                              <entry>
                                <string>_SongsForTheDeaf_</string>
                                <int>461</int>
                              </entry>
                              <entry>
                                <string>Jesspandapants</string>
                                <int>500</int>
                              </entry>
                              <entry>
                                <string>Great_White_Slug</string>
                                <int>71</int>
                              </entry>
                              <entry>
                                <string>SpecialAgentClitoris</string>
                                <int>151</int>
                              </entry>
                              <entry>
                                <string>DigitalKing713</string>
                                <int>319</int>
                              </entry>
                              <entry>
                                <string>Jillianmd777</string>
                                <int>129</int>
                              </entry>
                              <entry>
                                <string>saltedd</string>
                                <int>200</int>
                              </entry>
                              <entry>
                                <string>Xurandor</string>
                                <int>286</int>
                              </entry>
                              <entry>
                                <string>thatonekid0909</string>
                                <int>402</int>
                              </entry>
                              <entry>
                                <string>nailphile</string>
                                <int>50</int>
                              </entry>
                              <entry>
                                <string>SerQwaez</string>
                                <int>233</int>
                              </entry>
                              <entry>
                                <string>OnePieceNPC3</string>
                                <int>296</int>
                              </entry>
                              <entry>
                                <string>GloriousNugs</string>
                                <int>536</int>
                              </entry>
                              <entry>
                                <string>TheErez</string>
                                <int>26</int>
                              </entry>
                              <entry>
                                <string>alpharabbit</string>
                                <int>484</int>
                              </entry>
                              <entry>
                                <string>Bobby_Hatts</string>
                                <int>73</int>
                              </entry>
                              <entry>
                                <string>ChiboSempai</string>
                                <int>476</int>
                              </entry>
                              <entry>
                                <string>tofercakes</string>
                                <int>386</int>
                              </entry>
                              <entry>
                                <string>Nimmrod</string>
                                <int>432</int>
                              </entry>
                              <entry>
                                <string>The_Drugstore_Cowboy</string>
                                <int>226</int>
                              </entry>
                              <entry>
                                <string>xillyriax</string>
                                <int>546</int>
                              </entry>
                              <entry>
                                <string>Baconstripz69</string>
                                <int>7</int>
                              </entry>
                              <entry>
                                <string>czyzynsky</string>
                                <int>173</int>
                              </entry>
                              <entry>
                                <string>hondatothecivic</string>
                                <int>256</int>
                              </entry>
                              <entry>
                                <string>carval</string>
                                <int>222</int>
                              </entry>
                              <entry>
                                <string>potato88</string>
                                <int>367</int>
                              </entry>
                              <entry>
                                <string>OldUncleDingus</string>
                                <int>485</int>
                              </entry>
                              <entry>
                                <string>r3dditr3ss</string>
                                <int>511</int>
                              </entry>
                              <entry>
                                <string>penningtonant</string>
                                <int>355</int>
                              </entry>
                              <entry>
                                <string>archaicmotion</string>
                                <int>508</int>
                              </entry>
                              <entry>
                                <string>okcoooli</string>
                                <int>154</int>
                              </entry>
                              <entry>
                                <string>jonesy852</string>
                                <int>569</int>
                              </entry>
                              <entry>
                                <string>itapebats</string>
                                <int>124</int>
                              </entry>
                              <entry>
                                <string>FrugalNinja</string>
                                <int>328</int>
                              </entry>
                              <entry>
                                <string>hackcoder</string>
                                <int>454</int>
                              </entry>
                              <entry>
                                <string>TheNobleThief</string>
                                <int>218</int>
                              </entry>
                              <entry>
                                <string>Savergn</string>
                                <int>325</int>
                              </entry>
                              <entry>
                                <string>georgiapeach87</string>
                                <int>316</int>
                              </entry>
                              <entry>
                                <string>big_ol_boners</string>
                                <int>176</int>
                              </entry>
                              <entry>
                                <string>GaikokuJohn</string>
                                <int>237</int>
                              </entry>
                              <entry>
                                <string>Logaline</string>
                                <int>424</int>
                              </entry>
                              <entry>
                                <string>BobTheGuardian</string>
                                <int>274</int>
                              </entry>
                              <entry>
                                <string>NGU-Ben</string>
                                <int>212</int>
                              </entry>
                              <entry>
                                <string>Still_fn_waterOK</string>
                                <int>63</int>
                              </entry>
                              <entry>
                                <string>StrictScrutiny</string>
                                <int>505</int>
                              </entry>
                              <entry>
                                <string>Chainmail_Sam</string>
                                <int>192</int>
                              </entry>
                              <entry>
                                <string>cozzpuch</string>
                                <int>430</int>
                              </entry>
                              <entry>
                                <string>darkdutchess04</string>
                                <int>111</int>
                              </entry>
                              <entry>
                                <string>TossedRightOut</string>
                                <int>67</int>
                              </entry>
                              <entry>
                                <string>hammalahoo</string>
                                <int>79</int>
                              </entry>
                              <entry>
                                <string>SSJ_Leonardo</string>
                                <int>185</int>
                              </entry>
                              <entry>
                                <string>hawkeye_pizza_dog</string>
                                <int>499</int>
                              </entry>
                              <entry>
                                <string>terrorTrain</string>
                                <int>480</int>
                              </entry>
                              <entry>
                                <string>I__Know__Things</string>
                                <int>271</int>
                              </entry>
                              <entry>
                                <string>princesskiki</string>
                                <int>353</int>
                              </entry>
                              <entry>
                                <string>TWILIGHT_IS_AWESOME</string>
                                <int>157</int>
                              </entry>
                              <entry>
                                <string>fishfilllet</string>
                                <int>384</int>
                              </entry>
                              <entry>
                                <string>Sengura</string>
                                <int>8</int>
                              </entry>
                              <entry>
                                <string>W00ster</string>
                                <int>549</int>
                              </entry>
                              <entry>
                                <string>V1bration</string>
                                <int>106</int>
                              </entry>
                              <entry>
                                <string>mess_is_lore</string>
                                <int>320</int>
                              </entry>
                              <entry>
                                <string>RebornYew</string>
                                <int>313</int>
                              </entry>
                              <entry>
                                <string>dolphinhj</string>
                                <int>90</int>
                              </entry>
                              <entry>
                                <string>redcomet002</string>
                                <int>396</int>
                              </entry>
                              <entry>
                                <string>XPreNN</string>
                                <int>142</int>
                              </entry>
                              <entry>
                                <string>Tomguydude</string>
                                <int>345</int>
                              </entry>
                              <entry>
                                <string>ahmong</string>
                                <int>378</int>
                              </entry>
                              <entry>
                                <string>Goredby21</string>
                                <int>87</int>
                              </entry>
                              <entry>
                                <string>Mosrhun</string>
                                <int>180</int>
                              </entry>
                              <entry>
                                <string>MrSilkyJohnson</string>
                                <int>203</int>
                              </entry>
                              <entry>
                                <string>not-spiderpig</string>
                                <int>131</int>
                              </entry>
                              <entry>
                                <string>GrandmasPenisHurts</string>
                                <int>492</int>
                              </entry>
                              <entry>
                                <string>HatchA115</string>
                                <int>89</int>
                              </entry>
                              <entry>
                                <string>Swifty1377</string>
                                <int>24</int>
                              </entry>
                              <entry>
                                <string>CiscoCertified</string>
                                <int>211</int>
                              </entry>
                              <entry>
                                <string>Clobbertr0n</string>
                                <int>443</int>
                              </entry>
                              <entry>
                                <string>kbo-</string>
                                <int>365</int>
                              </entry>
                              <entry>
                                <string>ThatOneBronyDude</string>
                                <int>161</int>
                              </entry>
                              <entry>
                                <string>fuckbuddyneeded926</string>
                                <int>524</int>
                              </entry>
                              <entry>
                                <string>Vinay92</string>
                                <int>6</int>
                              </entry>
                              <entry>
                                <string>kaseyeeyo</string>
                                <int>331</int>
                              </entry>
                              <entry>
                                <string>Animal31</string>
                                <int>32</int>
                              </entry>
                              <entry>
                                <string>logitecharse</string>
                                <int>415</int>
                              </entry>
                              <entry>
                                <string>chief34</string>
                                <int>448</int>
                              </entry>
                              <entry>
                                <string>Naeture</string>
                                <int>202</int>
                              </entry>
                              <entry>
                                <string>TheStalker75</string>
                                <int>125</int>
                              </entry>
                              <entry>
                                <string>rq4c</string>
                                <int>436</int>
                              </entry>
                              <entry>
                                <string>TheDapperYank</string>
                                <int>580</int>
                              </entry>
                              <entry>
                                <string>somethingelse19</string>
                                <int>145</int>
                              </entry>
                              <entry>
                                <string>kjbigs282</string>
                                <int>583</int>
                              </entry>
                              <entry>
                                <string>iBrad99</string>
                                <int>227</int>
                              </entry>
                              <entry>
                                <string>deedeeiam</string>
                                <int>306</int>
                              </entry>
                              <entry>
                                <string>MasterMarksman</string>
                                <int>1</int>
                              </entry>
                              <entry>
                                <string>cactuscactuscactus</string>
                                <int>528</int>
                              </entry>
                              <entry>
                                <string>9000BeatsPerHour</string>
                                <int>450</int>
                              </entry>
                              <entry>
                                <string>fraincs</string>
                                <int>389</int>
                              </entry>
                              <entry>
                                <string>CUDDLEMASTER2</string>
                                <int>356</int>
                              </entry>
                              <entry>
                                <string>garblesnarky</string>
                                <int>561</int>
                              </entry>
                              <entry>
                                <string>Hapistoric</string>
                                <int>53</int>
                              </entry>
                              <entry>
                                <string>tikitikitikiroom</string>
                                <int>352</int>
                              </entry>
                              <entry>
                                <string>stormotron91</string>
                                <int>253</int>
                              </entry>
                              <entry>
                                <string>hates_cunts_deeply</string>
                                <int>199</int>
                              </entry>
                              <entry>
                                <string>tenoclockrobot</string>
                                <int>330</int>
                              </entry>
                              <entry>
                                <string>LinuxErrror</string>
                                <int>477</int>
                              </entry>
                              <entry>
                                <string>claywebb7</string>
                                <int>570</int>
                              </entry>
                              <entry>
                                <string>Wild_Marker</string>
                                <int>523</int>
                              </entry>
                              <entry>
                                <string>alcpwn3d</string>
                                <int>37</int>
                              </entry>
                              <entry>
                                <string>Hayves</string>
                                <int>329</int>
                              </entry>
                              <entry>
                                <string>neither_party</string>
                                <int>506</int>
                              </entry>
                              <entry>
                                <string>unleashthek</string>
                                <int>48</int>
                              </entry>
                              <entry>
                                <string>jecmoore</string>
                                <int>85</int>
                              </entry>
                              <entry>
                                <string>LeadHero</string>
                                <int>408</int>
                              </entry>
                              <entry>
                                <string>beastly_beauty</string>
                                <int>427</int>
                              </entry>
                              <entry>
                                <string>bootyscrew</string>
                                <int>110</int>
                              </entry>
                              <entry>
                                <string>JUST_LOGGED_IN</string>
                                <int>127</int>
                              </entry>
                              <entry>
                                <string>FuzzedLogic</string>
                                <int>143</int>
                              </entry>
                              <entry>
                                <string>SinnerOfAttention</string>
                                <int>507</int>
                              </entry>
                              <entry>
                                <string>Astara104</string>
                                <int>281</int>
                              </entry>
                              <entry>
                                <string>michaeloftarth</string>
                                <int>46</int>
                              </entry>
                              <entry>
                                <string>josh_trick</string>
                                <int>348</int>
                              </entry>
                              <entry>
                                <string>FreeAsInFreedoooooom</string>
                                <int>19</int>
                              </entry>
                              <entry>
                                <string>s4r9am</string>
                                <int>34</int>
                              </entry>
                              <entry>
                                <string>amanaboutahorse</string>
                                <int>137</int>
                              </entry>
                              <entry>
                                <string>Fallout2x</string>
                                <int>257</int>
                              </entry>
                              <entry>
                                <string>Bobby_Marks</string>
                                <int>465</int>
                              </entry>
                              <entry>
                                <string>ManzielThough</string>
                                <int>101</int>
                              </entry>
                              <entry>
                                <string>MuSiCiSmYGF</string>
                                <int>247</int>
                              </entry>
                              <entry>
                                <string>Hulkkis</string>
                                <int>529</int>
                              </entry>
                              <entry>
                                <string>varyshasnoball</string>
                                <int>54</int>
                              </entry>
                              <entry>
                                <string>SunnyRaja2</string>
                                <int>423</int>
                              </entry>
                              <entry>
                                <string>glay913</string>
                                <int>587</int>
                              </entry>
                              <entry>
                                <string>djguerito</string>
                                <int>264</int>
                              </entry>
                              <entry>
                                <string>spinflux</string>
                                <int>381</int>
                              </entry>
                              <entry>
                                <string>TheTiltster</string>
                                <int>558</int>
                              </entry>
                              <entry>
                                <string>TheFeverFrenzy</string>
                                <int>36</int>
                              </entry>
                              <entry>
                                <string>CronosDage</string>
                                <int>114</int>
                              </entry>
                              <entry>
                                <string>HandsOfJazz</string>
                                <int>563</int>
                              </entry>
                              <entry>
                                <string>joe_zaza</string>
                                <int>525</int>
                              </entry>
                              <entry>
                                <string>notLOL</string>
                                <int>5</int>
                              </entry>
                              <entry>
                                <string>LrrrOPersei8</string>
                                <int>188</int>
                              </entry>
                              <entry>
                                <string>bigups23</string>
                                <int>416</int>
                              </entry>
                              <entry>
                                <string>jdlee3</string>
                                <int>112</int>
                              </entry>
                              <entry>
                                <string>skychok</string>
                                <int>272</int>
                              </entry>
                              <entry>
                                <string>mushyz</string>
                                <int>460</int>
                              </entry>
                              <entry>
                                <string>monsterrod</string>
                                <int>11</int>
                              </entry>
                              <entry>
                                <string>alzyee</string>
                                <int>368</int>
                              </entry>
                              <entry>
                                <string>VotedBestDressed</string>
                                <int>136</int>
                              </entry>
                              <entry>
                                <string>Finglonger76</string>
                                <int>267</int>
                              </entry>
                              <entry>
                                <string>SchecterSLS</string>
                                <int>115</int>
                              </entry>
                              <entry>
                                <string>aerialspace</string>
                                <int>414</int>
                              </entry>
                              <entry>
                                <string>bradyh8</string>
                                <int>552</int>
                              </entry>
                              <entry>
                                <string>papaslurch</string>
                                <int>431</int>
                              </entry>
                              <entry>
                                <string>theineffablebob</string>
                                <int>23</int>
                              </entry>
                              <entry>
                                <string>vaman0sPest</string>
                                <int>16</int>
                              </entry>
                              <entry>
                                <string>dabordman</string>
                                <int>138</int>
                              </entry>
                              <entry>
                                <string>Zapper72</string>
                                <int>205</int>
                              </entry>
                              <entry>
                                <string>CaptainCoque</string>
                                <int>164</int>
                              </entry>
                              <entry>
                                <string>Xellith</string>
                                <int>393</int>
                              </entry>
                              <entry>
                                <string>FlyingTinOpener</string>
                                <int>100</int>
                              </entry>
                              <entry>
                                <string>SamIV</string>
                                <int>521</int>
                              </entry>
                              <entry>
                                <string>DirtySnakes</string>
                                <int>15</int>
                              </entry>
                              <entry>
                                <string>DRUNKEN_GLASSES</string>
                                <int>25</int>
                              </entry>
                              <entry>
                                <string>SecretSadConnor</string>
                                <int>139</int>
                              </entry>
                              <entry>
                                <string>wojamatas</string>
                                <int>543</int>
                              </entry>
                              <entry>
                                <string>Durpadoo</string>
                                <int>109</int>
                              </entry>
                              <entry>
                                <string>z4lachenko</string>
                                <int>446</int>
                              </entry>
                              <entry>
                                <string>i_did_not_enjoy_that</string>
                                <int>490</int>
                              </entry>
                              <entry>
                                <string>AndrewAtrus</string>
                                <int>571</int>
                              </entry>
                              <entry>
                                <string>luckykarma83</string>
                                <int>140</int>
                              </entry>
                              <entry>
                                <string>Nefelia</string>
                                <int>354</int>
                              </entry>
                              <entry>
                                <string>Kinbensha</string>
                                <int>439</int>
                              </entry>
                              <entry>
                                <string>caneras</string>
                                <int>186</int>
                              </entry>
                              <entry>
                                <string>moonjellies</string>
                                <int>412</int>
                              </entry>
                              <entry>
                                <string>mandoliinimies</string>
                                <int>437</int>
                              </entry>
                              <entry>
                                <string>MirceaChirea</string>
                                <int>314</int>
                              </entry>
                              <entry>
                                <string>Valisk</string>
                                <int>374</int>
                              </entry>
                              <entry>
                                <string>Loverman989</string>
                                <int>81</int>
                              </entry>
                              <entry>
                                <string>Dathem</string>
                                <int>501</int>
                              </entry>
                              <entry>
                                <string>eldercreedjunkie</string>
                                <int>305</int>
                              </entry>
                              <entry>
                                <string>SalamanderSylph</string>
                                <int>372</int>
                              </entry>
                              <entry>
                                <string>SwornFury</string>
                                <int>214</int>
                              </entry>
                              <entry>
                                <string>savoytruffle</string>
                                <int>279</int>
                              </entry>
                              <entry>
                                <string>RexRaider</string>
                                <int>398</int>
                              </entry>
                              <entry>
                                <string>Butterfly_Riot</string>
                                <int>550</int>
                              </entry>
                              <entry>
                                <string>stoopidfuknniggers</string>
                                <int>102</int>
                              </entry>
                              <entry>
                                <string>SurvivorType</string>
                                <int>358</int>
                              </entry>
                              <entry>
                                <string>aznegglover</string>
                                <int>29</int>
                              </entry>
                              <entry>
                                <string>flyingcrayons</string>
                                <int>224</int>
                              </entry>
                              <entry>
                                <string>Jean_Val_Jean</string>
                                <int>544</int>
                              </entry>
                              <entry>
                                <string>Sabbatai</string>
                                <int>586</int>
                              </entry>
                              <entry>
                                <string>YOLOSWAG420_BLAZEIT</string>
                                <int>18</int>
                              </entry>
                              <entry>
                                <string>YoureUsingCoconuts</string>
                                <int>441</int>
                              </entry>
                              <entry>
                                <string>nadiamamacita</string>
                                <int>438</int>
                              </entry>
                              <entry>
                                <string>maltballfalcon</string>
                                <int>545</int>
                              </entry>
                              <entry>
                                <string>NIGGERS_ARE_RAYCIS</string>
                                <int>69</int>
                              </entry>
                              <entry>
                                <string>Squid-drinker</string>
                                <int>241</int>
                              </entry>
                              <entry>
                                <string>Stompinstu</string>
                                <int>20</int>
                              </entry>
                              <entry>
                                <string>geno149</string>
                                <int>429</int>
                              </entry>
                              <entry>
                                <string>EmirSc</string>
                                <int>204</int>
                              </entry>
                              <entry>
                                <string>DrDoesENTDownRon</string>
                                <int>243</int>
                              </entry>
                              <entry>
                                <string>GentleDoc90</string>
                                <int>373</int>
                              </entry>
                              <entry>
                                <string>El_Dubious_Mung</string>
                                <int>282</int>
                              </entry>
                              <entry>
                                <string>Half-Maus</string>
                                <int>538</int>
                              </entry>
                              <entry>
                                <string>Rexcase</string>
                                <int>47</int>
                              </entry>
                              <entry>
                                <string>Jigroo</string>
                                <int>334</int>
                              </entry>
                              <entry>
                                <string>KeeperOfWell</string>
                                <int>341</int>
                              </entry>
                              <entry>
                                <string>GiantSquidd</string>
                                <int>84</int>
                              </entry>
                              <entry>
                                <string>Piccologne</string>
                                <int>335</int>
                              </entry>
                              <entry>
                                <string>scoopeded</string>
                                <int>344</int>
                              </entry>
                              <entry>
                                <string>readforit</string>
                                <int>75</int>
                              </entry>
                              <entry>
                                <string>zhirzzh</string>
                                <int>534</int>
                              </entry>
                              <entry>
                                <string>Vendril</string>
                                <int>433</int>
                              </entry>
                              <entry>
                                <string>strawberrykisses</string>
                                <int>502</int>
                              </entry>
                              <entry>
                                <string>kitjunk</string>
                                <int>195</int>
                              </entry>
                              <entry>
                                <string>TheRulingEmperor</string>
                                <int>347</int>
                              </entry>
                              <entry>
                                <string>IAmDivorced</string>
                                <int>55</int>
                              </entry>
                              <entry>
                                <string>snouchies</string>
                                <int>410</int>
                              </entry>
                              <entry>
                                <string>SeptimusSlut</string>
                                <int>421</int>
                              </entry>
                              <entry>
                                <string>inthe80s</string>
                                <int>481</int>
                              </entry>
                              <entry>
                                <string>aksid</string>
                                <int>380</int>
                              </entry>
                              <entry>
                                <string>hXcBassman</string>
                                <int>288</int>
                              </entry>
                              <entry>
                                <string>Turfew</string>
                                <int>363</int>
                              </entry>
                              <entry>
                                <string>compengineerbarbie</string>
                                <int>94</int>
                              </entry>
                              <entry>
                                <string>imkingdavid</string>
                                <int>210</int>
                              </entry>
                              <entry>
                                <string>ettuaslumiere</string>
                                <int>182</int>
                              </entry>
                              <entry>
                                <string>XTCrispy</string>
                                <int>349</int>
                              </entry>
                              <entry>
                                <string>two_comedians</string>
                                <int>269</int>
                              </entry>
                              <entry>
                                <string>Ninjacherry</string>
                                <int>270</int>
                              </entry>
                              <entry>
                                <string>Mister_Anthony</string>
                                <int>359</int>
                              </entry>
                              <entry>
                                <string>onelovebro</string>
                                <int>113</int>
                              </entry>
                              <entry>
                                <string>Cobalt62</string>
                                <int>219</int>
                              </entry>
                              <entry>
                                <string>ULTM8NIGGA</string>
                                <int>35</int>
                              </entry>
                              <entry>
                                <string>peshkoyy</string>
                                <int>98</int>
                              </entry>
                              <entry>
                                <string>UndergroundPhoenix</string>
                                <int>179</int>
                              </entry>
                              <entry>
                                <string>Nitlaax</string>
                                <int>206</int>
                              </entry>
                              <entry>
                                <string>stillwtnforbmrecords</string>
                                <int>300</int>
                              </entry>
                              <entry>
                                <string>LDM312</string>
                                <int>362</int>
                              </entry>
                              <entry>
                                <string>Russorphan</string>
                                <int>317</int>
                              </entry>
                              <entry>
                                <string>Asuperniceguy</string>
                                <int>147</int>
                              </entry>
                              <entry>
                                <string>Addyct</string>
                                <int>311</int>
                              </entry>
                              <entry>
                                <string>Gets_into_debates</string>
                                <int>403</int>
                              </entry>
                              <entry>
                                <string>MK-007</string>
                                <int>464</int>
                              </entry>
                              <entry>
                                <string>Mcallba</string>
                                <int>581</int>
                              </entry>
                              <entry>
                                <string>toothfairy32</string>
                                <int>382</int>
                              </entry>
                              <entry>
                                <string>Tw0Bit</string>
                                <int>585</int>
                              </entry>
                              <entry>
                                <string>ImArchMageBitch</string>
                                <int>520</int>
                              </entry>
                              <entry>
                                <string>CantSeeShit</string>
                                <int>121</int>
                              </entry>
                              <entry>
                                <string>jimothyyhalpert</string>
                                <int>221</int>
                              </entry>
                              <entry>
                                <string>cardbross</string>
                                <int>284</int>
                              </entry>
                              <entry>
                                <string>shinobi8</string>
                                <int>51</int>
                              </entry>
                              <entry>
                                <string>NinjaToss</string>
                                <int>483</int>
                              </entry>
                              <entry>
                                <string>The_Funky_Shaman</string>
                                <int>162</int>
                              </entry>
                              <entry>
                                <string>Erock11</string>
                                <int>170</int>
                              </entry>
                              <entry>
                                <string>lantheria</string>
                                <int>252</int>
                              </entry>
                              <entry>
                                <string>MikeMo243</string>
                                <int>86</int>
                              </entry>
                              <entry>
                                <string>IRONHain47</string>
                                <int>184</int>
                              </entry>
                              <entry>
                                <string>DjangoSol</string>
                                <int>556</int>
                              </entry>
                              <entry>
                                <string>internet_drunk</string>
                                <int>174</int>
                              </entry>
                              <entry>
                                <string>cadaverco</string>
                                <int>244</int>
                              </entry>
                              <entry>
                                <string>RageX</string>
                                <int>104</int>
                              </entry>
                              <entry>
                                <string>Pinchersofperil</string>
                                <int>133</int>
                              </entry>
                              <entry>
                                <string>InYoHouseEatinToast</string>
                                <int>193</int>
                              </entry>
                              <entry>
                                <string>Liv-Julia</string>
                                <int>434</int>
                              </entry>
                              <entry>
                                <string>Hoganbeardy</string>
                                <int>494</int>
                              </entry>
                              <entry>
                                <string>aguywithacellphone</string>
                                <int>52</int>
                              </entry>
                              <entry>
                                <string>whileyousleep2</string>
                                <int>310</int>
                              </entry>
                              <entry>
                                <string>spartacus7173</string>
                                <int>312</int>
                              </entry>
                              <entry>
                                <string>juicyfisher</string>
                                <int>463</int>
                              </entry>
                              <entry>
                                <string>TehSnowman</string>
                                <int>181</int>
                              </entry>
                              <entry>
                                <string>That_Guy_In_The_Room</string>
                                <int>2</int>
                              </entry>
                              <entry>
                                <string>Sprocketlord</string>
                                <int>539</int>
                              </entry>
                              <entry>
                                <string>esyples</string>
                                <int>385</int>
                              </entry>
                              <entry>
                                <string>Hushpuppy6</string>
                                <int>474</int>
                              </entry>
                              <entry>
                                <string>horridhumour</string>
                                <int>565</int>
                              </entry>
                              <entry>
                                <string>nuovo_donna</string>
                                <int>411</int>
                              </entry>
                              <entry>
                                <string>Tocasvil</string>
                                <int>338</int>
                              </entry>
                              <entry>
                                <string>DaveMills</string>
                                <int>45</int>
                              </entry>
                              <entry>
                                <string>tgraefj</string>
                                <int>160</int>
                              </entry>
                              <entry>
                                <string>Abedeus</string>
                                <int>190</int>
                              </entry>
                              <entry>
                                <string>MUPP3T375</string>
                                <int>457</int>
                              </entry>
                              <entry>
                                <string>GesusKhrist</string>
                                <int>537</int>
                              </entry>
                              <entry>
                                <string>New_Slurm</string>
                                <int>342</int>
                              </entry>
                              <entry>
                                <string>Lunar96</string>
                                <int>103</int>
                              </entry>
                              <entry>
                                <string>kinboyatuwo</string>
                                <int>401</int>
                              </entry>
                              <entry>
                                <string>Sweyoa</string>
                                <int>207</int>
                              </entry>
                              <entry>
                                <string>the_fascist</string>
                                <int>83</int>
                              </entry>
                              <entry>
                                <string>Harmania</string>
                                <int>339</int>
                              </entry>
                              <entry>
                                <string>olaf_from_norweden</string>
                                <int>93</int>
                              </entry>
                              <entry>
                                <string>keniluck</string>
                                <int>275</int>
                              </entry>
                              <entry>
                                <string>BlackLetterLaw</string>
                                <int>399</int>
                              </entry>
                              <entry>
                                <string>Buffaluffasaurus</string>
                                <int>266</int>
                              </entry>
                              <entry>
                                <string>GS059</string>
                                <int>377</int>
                              </entry>
                              <entry>
                                <string>not_a_famous_actor</string>
                                <int>496</int>
                              </entry>
                              <entry>
                                <string>LeGemBot</string>
                                <int>76</int>
                              </entry>
                              <entry>
                                <string>ktappe</string>
                                <int>167</int>
                              </entry>
                              <entry>
                                <string>jistlerummies</string>
                                <int>263</int>
                              </entry>
                              <entry>
                                <string>aaabigayle</string>
                                <int>420</int>
                              </entry>
                              <entry>
                                <string>stmbtrev</string>
                                <int>265</int>
                              </entry>
                              <entry>
                                <string>brassified</string>
                                <int>458</int>
                              </entry>
                              <entry>
                                <string>Caitlinface</string>
                                <int>159</int>
                              </entry>
                              <entry>
                                <string>limitbroken</string>
                                <int>307</int>
                              </entry>
                              <entry>
                                <string>awkwardIRL</string>
                                <int>488</int>
                              </entry>
                              <entry>
                                <string>monkeygame7</string>
                                <int>209</int>
                              </entry>
                              <entry>
                                <string>kunteater</string>
                                <int>74</int>
                              </entry>
                              <entry>
                                <string>LiliRainicorn</string>
                                <int>360</int>
                              </entry>
                              <entry>
                                <string>throwitawayfool2</string>
                                <int>471</int>
                              </entry>
                              <entry>
                                <string>another_old_fart</string>
                                <int>512</int>
                              </entry>
                              <entry>
                                <string>kingofkyrgyzstan</string>
                                <int>413</int>
                              </entry>
                              <entry>
                                <string>SpartanSauce</string>
                                <int>315</int>
                              </entry>
                              <entry>
                                <string>Mytushilol</string>
                                <int>392</int>
                              </entry>
                              <entry>
                                <string>NeonPiggy</string>
                                <int>554</int>
                              </entry>
                              <entry>
                                <string>Blacksburg</string>
                                <int>14</int>
                              </entry>
                              <entry>
                                <string>SoBraev</string>
                                <int>153</int>
                              </entry>
                              <entry>
                                <string>rajeshkumargiri</string>
                                <int>30</int>
                              </entry>
                              <entry>
                                <string>VincetheMadMan</string>
                                <int>495</int>
                              </entry>
                              <entry>
                                <string>anothernewstart</string>
                                <int>388</int>
                              </entry>
                              <entry>
                                <string>HPLoveshack</string>
                                <int>541</int>
                              </entry>
                              <entry>
                                <string>Weltall82</string>
                                <int>155</int>
                              </entry>
                              <entry>
                                <string>HBcubs</string>
                                <int>293</int>
                              </entry>
                              <entry>
                                <string>proROKexpat</string>
                                <int>107</int>
                              </entry>
                              <entry>
                                <string>UprisingSharingan</string>
                                <int>469</int>
                              </entry>
                              <entry>
                                <string>HarlemShakesSoCash</string>
                                <int>236</int>
                              </entry>
                              <entry>
                                <string>noeatnosleep</string>
                                <int>519</int>
                              </entry>
                              <entry>
                                <string>floodsymalone</string>
                                <int>171</int>
                              </entry>
                              <entry>
                                <string>benjamy</string>
                                <int>70</int>
                              </entry>
                              <entry>
                                <string>miamiron</string>
                                <int>333</int>
                              </entry>
                              <entry>
                                <string>sardu1</string>
                                <int>530</int>
                              </entry>
                              <entry>
                                <string>MidKnight007</string>
                                <int>117</int>
                              </entry>
                              <entry>
                                <string>itsmckenney</string>
                                <int>555</int>
                              </entry>
                              <entry>
                                <string>thebiggestdick</string>
                                <int>260</int>
                              </entry>
                              <entry>
                                <string>pagaladmee</string>
                                <int>346</int>
                              </entry>
                              <entry>
                                <string>skhisma</string>
                                <int>177</int>
                              </entry>
                              <entry>
                                <string>IAMA_dragon-AMA</string>
                                <int>191</int>
                              </entry>
                              <entry>
                                <string>ponchoandy</string>
                                <int>105</int>
                              </entry>
                              <entry>
                                <string>porquehuevo</string>
                                <int>235</int>
                              </entry>
                              <entry>
                                <string>RinCakes</string>
                                <int>376</int>
                              </entry>
                              <entry>
                                <string>Chris_Judd</string>
                                <int>183</int>
                              </entry>
                              <entry>
                                <string>strallweat</string>
                                <int>574</int>
                              </entry>
                              <entry>
                                <string>nucleardemon</string>
                                <int>579</int>
                              </entry>
                              <entry>
                                <string>THECOACH0742</string>
                                <int>493</int>
                              </entry>
                              <entry>
                                <string>12sea21</string>
                                <int>517</int>
                              </entry>
                              <entry>
                                <string>ChaplnGrillSgt</string>
                                <int>551</int>
                              </entry>
                              <entry>
                                <string>hijinksobserved</string>
                                <int>562</int>
                              </entry>
                              <entry>
                                <string>omgwtFANTASTIC</string>
                                <int>40</int>
                              </entry>
                              <entry>
                                <string>AquilaHeliaca</string>
                                <int>42</int>
                              </entry>
                              <entry>
                                <string>Cyn0nymous</string>
                                <int>251</int>
                              </entry>
                              <entry>
                                <string>bodhisatvajr</string>
                                <int>498</int>
                              </entry>
                              <entry>
                                <string>tiramisu_king</string>
                                <int>22</int>
                              </entry>
                              <entry>
                                <string>MrToocool101</string>
                                <int>409</int>
                              </entry>
                              <entry>
                                <string>m3atwad</string>
                                <int>280</int>
                              </entry>
                              <entry>
                                <string>peachypixie</string>
                                <int>442</int>
                              </entry>
                              <entry>
                                <string>key_lime_pie</string>
                                <int>361</int>
                              </entry>
                              <entry>
                                <string>kingbarnaby</string>
                                <int>394</int>
                              </entry>
                              <entry>
                                <string>TimeTourism</string>
                                <int>283</int>
                              </entry>
                              <entry>
                                <string>MattAdams53</string>
                                <int>425</int>
                              </entry>
                              <entry>
                                <string>hellrazzer24</string>
                                <int>322</int>
                              </entry>
                              <entry>
                                <string>cjdcastro</string>
                                <int>560</int>
                              </entry>
                              <entry>
                                <string>d0nkeyBOB</string>
                                <int>390</int>
                              </entry>
                              <entry>
                                <string>HAL4500</string>
                                <int>444</int>
                              </entry>
                              <entry>
                                <string>thegarate</string>
                                <int>515</int>
                              </entry>
                              <entry>
                                <string>chrisUKeuw</string>
                                <int>217</int>
                              </entry>
                              <entry>
                                <string>96363</string>
                                <int>91</int>
                              </entry>
                              <entry>
                                <string>Diamond_joe</string>
                                <int>422</int>
                              </entry>
                              <entry>
                                <string>16_F_CALI</string>
                                <int>148</int>
                              </entry>
                              <entry>
                                <string>pixis-4950</string>
                                <int>9</int>
                              </entry>
                              <entry>
                                <string>carpediem2012</string>
                                <int>95</int>
                              </entry>
                              <entry>
                                <string>Only_use_me_TORGUE</string>
                                <int>287</int>
                              </entry>
                              <entry>
                                <string>ScorpionBlue1989</string>
                                <int>60</int>
                              </entry>
                              <entry>
                                <string>flyingcatpotato</string>
                                <int>10</int>
                              </entry>
                              <entry>
                                <string>gentlepornstar</string>
                                <int>82</int>
                              </entry>
                              <entry>
                                <string>kirkkommander</string>
                                <int>514</int>
                              </entry>
                              <entry>
                                <string>NoOne0507</string>
                                <int>343</int>
                              </entry>
                              <entry>
                                <string>EpicGuard</string>
                                <int>466</int>
                              </entry>
                              <entry>
                                <string>gabbagool</string>
                                <int>223</int>
                              </entry>
                              <entry>
                                <string>Dat_Bass1</string>
                                <int>0</int>
                              </entry>
                              <entry>
                                <string>arenek3</string>
                                <int>522</int>
                              </entry>
                              <entry>
                                <string>mbwasigh</string>
                                <int>527</int>
                              </entry>
                              <entry>
                                <string>BobbySchroeder</string>
                                <int>28</int>
                              </entry>
                              <entry>
                                <string>badger035</string>
                                <int>435</int>
                              </entry>
                              <entry>
                                <string>Wrath_Of_Aguirre</string>
                                <int>564</int>
                              </entry>
                              <entry>
                                <string>King_trident</string>
                                <int>255</int>
                              </entry>
                              <entry>
                                <string>SixWireS</string>
                                <int>486</int>
                              </entry>
                              <entry>
                                <string>jewfly</string>
                                <int>118</int>
                              </entry>
                              <entry>
                                <string>88</string>
                                <int>201</int>
                              </entry>
                              <entry>
                                <string>afrowarriornabe</string>
                                <int>405</int>
                              </entry>
                              <entry>
                                <string>mlh2388</string>
                                <int>92</int>
                              </entry>
                              <entry>
                                <string>ChefSteph407</string>
                                <int>245</int>
                              </entry>
                              <entry>
                                <string>ShovelFace</string>
                                <int>77</int>
                              </entry>
                              <entry>
                                <string>does_not_post_ever</string>
                                <int>542</int>
                              </entry>
                              <entry>
                                <string>TXRazorback</string>
                                <int>126</int>
                              </entry>
                              <entry>
                                <string>GRIMMnM</string>
                                <int>553</int>
                              </entry>
                              <entry>
                                <string>houdini_died_of_aids</string>
                                <int>510</int>
                              </entry>
                              <entry>
                                <string>sae1</string>
                                <int>573</int>
                              </entry>
                              <entry>
                                <string>lexgrub</string>
                                <int>4</int>
                              </entry>
                              <entry>
                                <string>Burn4Crimes</string>
                                <int>337</int>
                              </entry>
                              <entry>
                                <string>Asdyc</string>
                                <int>487</int>
                              </entry>
                              <entry>
                                <string>DocRude</string>
                                <int>468</int>
                              </entry>
                              <entry>
                                <string>DisMyAccount</string>
                                <int>262</int>
                              </entry>
                              <entry>
                                <string>TehNoff</string>
                                <int>38</int>
                              </entry>
                              <entry>
                                <string>SamIsWhite</string>
                                <int>513</int>
                              </entry>
                              <entry>
                                <string>Mr_Flippers</string>
                                <int>66</int>
                              </entry>
                              <entry>
                                <string>DoubleMintx</string>
                                <int>291</int>
                              </entry>
                              <entry>
                                <string>Amplitude</string>
                                <int>509</int>
                              </entry>
                              <entry>
                                <string>notacrackheadofficer</string>
                                <int>128</int>
                              </entry>
                              <entry>
                                <string>BigRiver4</string>
                                <int>31</int>
                              </entry>
                              <entry>
                                <string>ronfromcny</string>
                                <int>397</int>
                              </entry>
                              <entry>
                                <string>Raven0520</string>
                                <int>99</int>
                              </entry>
                              <entry>
                                <string>Nefandi</string>
                                <int>169</int>
                              </entry>
                              <entry>
                                <string>Slick775</string>
                                <int>533</int>
                              </entry>
                              <entry>
                                <string>knightwave</string>
                                <int>299</int>
                              </entry>
                              <entry>
                                <string>Thing-a-manigger</string>
                                <int>198</int>
                              </entry>
                              <entry>
                                <string>redaemon</string>
                                <int>548</int>
                              </entry>
                              <entry>
                                <string>emlgsh</string>
                                <int>189</int>
                              </entry>
                              <entry>
                                <string>allhollows415</string>
                                <int>152</int>
                              </entry>
                              <entry>
                                <string>RnRaintnoisepolution</string>
                                <int>478</int>
                              </entry>
                              <entry>
                                <string>kustomdeluxe</string>
                                <int>3</int>
                              </entry>
                              <entry>
                                <string>SPESSMEHREN</string>
                                <int>64</int>
                              </entry>
                              <entry>
                                <string>nosy_coyote</string>
                                <int>379</int>
                              </entry>
                              <entry>
                                <string>RandomGuy572</string>
                                <int>141</int>
                              </entry>
                              <entry>
                                <string>faxwellington</string>
                                <int>194</int>
                              </entry>
                              <entry>
                                <string>TheLegendofLloyd</string>
                                <int>43</int>
                              </entry>
                              <entry>
                                <string>Mordilaa</string>
                                <int>451</int>
                              </entry>
                              <entry>
                                <string>clownparade</string>
                                <int>187</int>
                              </entry>
                              <entry>
                                <string>Kafke</string>
                                <int>261</int>
                              </entry>
                              <entry>
                                <string>RotmgTrent</string>
                                <int>65</int>
                              </entry>
                              <entry>
                                <string>PropMonkey</string>
                                <int>78</int>
                              </entry>
                              <entry>
                                <string>RecklessKelly</string>
                                <int>456</int>
                              </entry>
                              <entry>
                                <string>danrennt98</string>
                                <int>426</int>
                              </entry>
                              <entry>
                                <string>DankDarko</string>
                                <int>406</int>
                              </entry>
                              <entry>
                                <string>Hoobleton</string>
                                <int>475</int>
                              </entry>
                              <entry>
                                <string>MrsRickyRicardo</string>
                                <int>132</int>
                              </entry>
                              <entry>
                                <string>czarukus</string>
                                <int>584</int>
                              </entry>
                              <entry>
                                <string>Disciple_of_Crom</string>
                                <int>375</int>
                              </entry>
                              <entry>
                                <string>CaptionBot</string>
                                <int>165</int>
                              </entry>
                              <entry>
                                <string>Zappulon</string>
                                <int>285</int>
                              </entry>
                              <entry>
                                <string>loli123</string>
                                <int>72</int>
                              </entry>
                              <entry>
                                <string>_coffeebean</string>
                                <int>130</int>
                              </entry>
                              <entry>
                                <string>Dont_Squeeze_me</string>
                                <int>370</int>
                              </entry>
                              <entry>
                                <string>chaos0510</string>
                                <int>309</int>
                              </entry>
                              <entry>
                                <string>DammitJosh</string>
                                <int>178</int>
                              </entry>
                              <entry>
                                <string>Khenir</string>
                                <int>557</int>
                              </entry>
                              <entry>
                                <string>katzekitteh</string>
                                <int>172</int>
                              </entry>
                              <entry>
                                <string>laur7620</string>
                                <int>440</int>
                              </entry>
                              <entry>
                                <string>I_Read_an_article</string>
                                <int>428</int>
                              </entry>
                              <entry>
                                <string>IAMBLACKANDDANGEROUS</string>
                                <int>108</int>
                              </entry>
                              <entry>
                                <string>IncognitoChrome</string>
                                <int>144</int>
                              </entry>
                              <entry>
                                <string>dspman11</string>
                                <int>240</int>
                              </entry>
                              <entry>
                                <string>JordanBlythe</string>
                                <int>470</int>
                              </entry>
                              <entry>
                                <string>cccpcharm</string>
                                <int>526</int>
                              </entry>
                              <entry>
                                <string>digiorknow</string>
                                <int>452</int>
                              </entry>
                              <entry>
                                <string>cbarrett1989</string>
                                <int>175</int>
                              </entry>
                              <entry>
                                <string>FromPakistanwithlove</string>
                                <int>453</int>
                              </entry>
                              <entry>
                                <string>DaGhostQc</string>
                                <int>417</int>
                              </entry>
                              <entry>
                                <string>secret759</string>
                                <int>259</int>
                              </entry>
                              <entry>
                                <string>dayvOn_cowboy</string>
                                <int>238</int>
                              </entry>
                              <entry>
                                <string>GimmeYourTags</string>
                                <int>449</int>
                              </entry>
                              <entry>
                                <string>vidurnaktis</string>
                                <int>44</int>
                              </entry>
                              <entry>
                                <string>BelugaTaquito</string>
                                <int>383</int>
                              </entry>
                              <entry>
                                <string>whohead63</string>
                                <int>491</int>
                              </entry>
                              <entry>
                                <string>Narissis</string>
                                <int>62</int>
                              </entry>
                              <entry>
                                <string>Antroh</string>
                                <int>566</int>
                              </entry>
                              <entry>
                                <string>FixesRageGrammar</string>
                                <int>350</int>
                              </entry>
                              <entry>
                                <string>NIGGER_PWN</string>
                                <int>119</int>
                              </entry>
                              <entry>
                                <string>Tober04</string>
                                <int>572</int>
                              </entry>
                              <entry>
                                <string>Wankah_</string>
                                <int>445</int>
                              </entry>
                              <entry>
                                <string>iwakun</string>
                                <int>582</int>
                              </entry>
                              <entry>
                                <string>clitneyrears</string>
                                <int>234</int>
                              </entry>
                              <entry>
                                <string>Real_Muthaphukkin_Gs</string>
                                <int>163</int>
                              </entry>
                              <entry>
                                <string>swimingmainstream</string>
                                <int>404</int>
                              </entry>
                              <entry>
                                <string>ekidgunlife</string>
                                <int>27</int>
                              </entry>
                              <entry>
                                <string>KennyGaming</string>
                                <int>303</int>
                              </entry>
                              <entry>
                                <string>jakfischer</string>
                                <int>150</int>
                              </entry>
                              <entry>
                                <string>ohforpete</string>
                                <int>295</int>
                              </entry>
                              <entry>
                                <string>rabbitspade</string>
                                <int>462</int>
                              </entry>
                              <entry>
                                <string>minecraft1787</string>
                                <int>242</int>
                              </entry>
                              <entry>
                                <string>USCswimmer</string>
                                <int>418</int>
                              </entry>
                              <entry>
                                <string>BringItOnHome</string>
                                <int>504</int>
                              </entry>
                              <entry>
                                <string>TotallyNotSuperman</string>
                                <int>146</int>
                              </entry>
                              <entry>
                                <string>Phallenpheather</string>
                                <int>196</int>
                              </entry>
                              <entry>
                                <string>nolimbs</string>
                                <int>268</int>
                              </entry>
                              <entry>
                                <string>v1ty4z1</string>
                                <int>13</int>
                              </entry>
                              <entry>
                                <string>These-Days</string>
                                <int>290</int>
                              </entry>
                              <entry>
                                <string>NikolaiVonToffel</string>
                                <int>254</int>
                              </entry>
                              <entry>
                                <string>HastaLasagna</string>
                                <int>277</int>
                              </entry>
                              <entry>
                                <string>Randoodle</string>
                                <int>68</int>
                              </entry>
                              <entry>
                                <string>Ilorin_Lorati</string>
                                <int>134</int>
                              </entry>
                              <entry>
                                <string>remote_production</string>
                                <int>364</int>
                              </entry>
                              <entry>
                                <string>ghick</string>
                                <int>57</int>
                              </entry>
                              <entry>
                                <string>CochMaestro</string>
                                <int>366</int>
                              </entry>
                              <entry>
                                <string>RiotCatland</string>
                                <int>419</int>
                              </entry>
                              <entry>
                                <string>twooaktrees</string>
                                <int>332</int>
                              </entry>
                              <entry>
                                <string>Voduar</string>
                                <int>17</int>
                              </entry>
                              <entry>
                                <string>CoolHeadedLogician</string>
                                <int>308</int>
                              </entry>
                              <entry>
                                <string>PadAccount</string>
                                <int>96</int>
                              </entry>
                              <entry>
                                <string>I_Love_Biscuits</string>
                                <int>249</int>
                              </entry>
                              <entry>
                                <string>KevRose</string>
                                <int>298</int>
                              </entry>
                              <entry>
                                <string>cloaclacola</string>
                                <int>273</int>
                              </entry>
                              <entry>
                                <string>TostitosGseries</string>
                                <int>246</int>
                              </entry>
                              <entry>
                                <string>Infintinity</string>
                                <int>568</int>
                              </entry>
                              <entry>
                                <string>ellie_gamer_x</string>
                                <int>56</int>
                              </entry>
                              <entry>
                                <string>licked_cupcake</string>
                                <int>297</int>
                              </entry>
                              <entry>
                                <string>cyph3x</string>
                                <int>473</int>
                              </entry>
                              <entry>
                                <string>AnonPaul</string>
                                <int>258</int>
                              </entry>
                              <entry>
                                <string>garbbagebear</string>
                                <int>323</int>
                              </entry>
                              <entry>
                                <string>PLMessiah</string>
                                <int>559</int>
                              </entry>
                              <entry>
                                <string>HerrCorrector</string>
                                <int>576</int>
                              </entry>
                              <entry>
                                <string>OilofOregano</string>
                                <int>531</int>
                              </entry>
                              <entry>
                                <string>drirayn</string>
                                <int>489</int>
                              </entry>
                              <entry>
                                <string>Krotiuz</string>
                                <int>497</int>
                              </entry>
                              <entry>
                                <string>iamnotamarxist</string>
                                <int>324</int>
                              </entry>
                              <entry>
                                <string>rcoe</string>
                                <int>326</int>
                              </entry>
                              <entry>
                                <string>The_Monarch79</string>
                                <int>467</int>
                              </entry>
                              <entry>
                                <string>My3rdTesticle</string>
                                <int>213</int>
                              </entry>
                              <entry>
                                <string>WhiteE350</string>
                                <int>61</int>
                              </entry>
                            </symbolToIndexMap>
                            <indexToSymbolMap id="22">
                              <string>Dat_Bass1</string>
                              <string>MasterMarksman</string>
                              <string>That_Guy_In_The_Room</string>
                              <string>kustomdeluxe</string>
                              <string>lexgrub</string>
                              <string>notLOL</string>
                              <string>Vinay92</string>
                              <string>Baconstripz69</string>
                              <string>Sengura</string>
                              <string>pixis-4950</string>
                              <string>flyingcatpotato</string>
                              <string>monsterrod</string>
                              <string>Sharkbear_McPunchy</string>
                              <string>v1ty4z1</string>
                              <string>Blacksburg</string>
                              <string>DirtySnakes</string>
                              <string>vaman0sPest</string>
                              <string>Voduar</string>
                              <string>YOLOSWAG420_BLAZEIT</string>
                              <string>FreeAsInFreedoooooom</string>
                              <string>Stompinstu</string>
                              <string>NickN3v3r</string>
                              <string>tiramisu_king</string>
                              <string>theineffablebob</string>
                              <string>Swifty1377</string>
                              <string>DRUNKEN_GLASSES</string>
                              <string>TheErez</string>
                              <string>ekidgunlife</string>
                              <string>BobbySchroeder</string>
                              <string>aznegglover</string>
                              <string>rajeshkumargiri</string>
                              <string>BigRiver4</string>
                              <string>Animal31</string>
                              <string>rapgamecheryltunt</string>
                              <string>s4r9am</string>
                              <string>ULTM8NIGGA</string>
                              <string>TheFeverFrenzy</string>
                              <string>alcpwn3d</string>
                              <string>TehNoff</string>
                              <string>drunkschitzo</string>
                              <string>omgwtFANTASTIC</string>
                              <string>NikeSB6</string>
                              <string>AquilaHeliaca</string>
                              <string>TheLegendofLloyd</string>
                              <string>vidurnaktis</string>
                              <string>DaveMills</string>
                              <string>michaeloftarth</string>
                              <string>Rexcase</string>
                              <string>unleashthek</string>
                              <string>MyFriendsCallMeSir</string>
                              <string>nailphile</string>
                              <string>shinobi8</string>
                              <string>aguywithacellphone</string>
                              <string>Hapistoric</string>
                              <string>varyshasnoball</string>
                              <string>IAmDivorced</string>
                              <string>ellie_gamer_x</string>
                              <string>ghick</string>
                              <string>dragonstalking</string>
                              <string>tsgmob</string>
                              <string>ScorpionBlue1989</string>
                              <string>WhiteE350</string>
                              <string>Narissis</string>
                              <string>Still_fn_waterOK</string>
                              <string>SPESSMEHREN</string>
                              <string>RotmgTrent</string>
                              <string>Mr_Flippers</string>
                              <string>TossedRightOut</string>
                              <string>Randoodle</string>
                              <string>NIGGERS_ARE_RAYCIS</string>
                              <string>benjamy</string>
                              <string>Great_White_Slug</string>
                              <string>loli123</string>
                              <string>Bobby_Hatts</string>
                              <string>kunteater</string>
                              <string>readforit</string>
                              <string>LeGemBot</string>
                              <string>ShovelFace</string>
                              <string>PropMonkey</string>
                              <string>hammalahoo</string>
                              <string>wiicontroller</string>
                              <string>Loverman989</string>
                              <string>gentlepornstar</string>
                              <string>the_fascist</string>
                              <string>GiantSquidd</string>
                              <string>jecmoore</string>
                              <string>MikeMo243</string>
                              <string>Goredby21</string>
                              <string>TXBear</string>
                              <string>HatchA115</string>
                              <string>dolphinhj</string>
                              <string>96363</string>
                              <string>mlh2388</string>
                              <string>olaf_from_norweden</string>
                              <string>compengineerbarbie</string>
                              <string>carpediem2012</string>
                              <string>PadAccount</string>
                              <string>IAmJaxRagingBoner</string>
                              <string>peshkoyy</string>
                              <string>Raven0520</string>
                              <string>FlyingTinOpener</string>
                              <string>ManzielThough</string>
                              <string>stoopidfuknniggers</string>
                              <string>Lunar96</string>
                              <string>RageX</string>
                              <string>ponchoandy</string>
                              <string>V1bration</string>
                              <string>proROKexpat</string>
                              <string>IAMBLACKANDDANGEROUS</string>
                              <string>Durpadoo</string>
                              <string>bootyscrew</string>
                              <string>darkdutchess04</string>
                              <string>jdlee3</string>
                              <string>onelovebro</string>
                              <string>CronosDage</string>
                              <string>SchecterSLS</string>
                              <string>k0mbine</string>
                              <string>MidKnight007</string>
                              <string>jewfly</string>
                              <string>NIGGER_PWN</string>
                              <string>KYLEBISH</string>
                              <string>CantSeeShit</string>
                              <string>Vlayue</string>
                              <string>jebsta1</string>
                              <string>itapebats</string>
                              <string>TheStalker75</string>
                              <string>TXRazorback</string>
                              <string>JUST_LOGGED_IN</string>
                              <string>notacrackheadofficer</string>
                              <string>Jillianmd777</string>
                              <string>_coffeebean</string>
                              <string>not-spiderpig</string>
                              <string>MrsRickyRicardo</string>
                              <string>Pinchersofperil</string>
                              <string>Ilorin_Lorati</string>
                              <string>petard</string>
                              <string>VotedBestDressed</string>
                              <string>amanaboutahorse</string>
                              <string>dabordman</string>
                              <string>SecretSadConnor</string>
                              <string>luckykarma83</string>
                              <string>RandomGuy572</string>
                              <string>XPreNN</string>
                              <string>FuzzedLogic</string>
                              <string>IncognitoChrome</string>
                              <string>somethingelse19</string>
                              <string>TotallyNotSuperman</string>
                              <string>Asuperniceguy</string>
                              <string>16_F_CALI</string>
                              <string>Xraging</string>
                              <string>jakfischer</string>
                              <string>SpecialAgentClitoris</string>
                              <string>allhollows415</string>
                              <string>SoBraev</string>
                              <string>okcoooli</string>
                              <string>Weltall82</string>
                              <string>Zep_Rocko</string>
                              <string>TWILIGHT_IS_AWESOME</string>
                              <string>clarke187</string>
                              <string>Caitlinface</string>
                              <string>tgraefj</string>
                              <string>ThatOneBronyDude</string>
                              <string>The_Funky_Shaman</string>
                              <string>Real_Muthaphukkin_Gs</string>
                              <string>CaptainCoque</string>
                              <string>CaptionBot</string>
                              <string>rrelationshipsthro</string>
                              <string>ktappe</string>
                              <string>contrarian</string>
                              <string>Nefandi</string>
                              <string>Erock11</string>
                              <string>floodsymalone</string>
                              <string>katzekitteh</string>
                              <string>czyzynsky</string>
                              <string>internet_drunk</string>
                              <string>cbarrett1989</string>
                              <string>big_ol_boners</string>
                              <string>skhisma</string>
                              <string>DammitJosh</string>
                              <string>UndergroundPhoenix</string>
                              <string>Mosrhun</string>
                              <string>TehSnowman</string>
                              <string>ettuaslumiere</string>
                              <string>Chris_Judd</string>
                              <string>IRONHain47</string>
                              <string>SSJ_Leonardo</string>
                              <string>caneras</string>
                              <string>clownparade</string>
                              <string>LrrrOPersei8</string>
                              <string>emlgsh</string>
                              <string>Abedeus</string>
                              <string>IAMA_dragon-AMA</string>
                              <string>Chainmail_Sam</string>
                              <string>InYoHouseEatinToast</string>
                              <string>faxwellington</string>
                              <string>kitjunk</string>
                              <string>Phallenpheather</string>
                              <string>simonkm</string>
                              <string>Thing-a-manigger</string>
                              <string>hates_cunts_deeply</string>
                              <string>saltedd</string>
                              <string>88</string>
                              <string>Naeture</string>
                              <string>MrSilkyJohnson</string>
                              <string>EmirSc</string>
                              <string>Zapper72</string>
                              <string>Nitlaax</string>
                              <string>Sweyoa</string>
                              <string>kayemme</string>
                              <string>monkeygame7</string>
                              <string>imkingdavid</string>
                              <string>CiscoCertified</string>
                              <string>NGU-Ben</string>
                              <string>My3rdTesticle</string>
                              <string>SwornFury</string>
                              <string>leetahsuntoucher</string>
                              <string>usmc50cal</string>
                              <string>chrisUKeuw</string>
                              <string>TheNobleThief</string>
                              <string>Cobalt62</string>
                              <string>Joshuncool</string>
                              <string>jimothyyhalpert</string>
                              <string>carval</string>
                              <string>gabbagool</string>
                              <string>flyingcrayons</string>
                              <string>deepaktiwarii</string>
                              <string>The_Drugstore_Cowboy</string>
                              <string>iBrad99</string>
                              <string>give_le_a_chance</string>
                              <string>ROCK-KNIGHT</string>
                              <string>Eliza_Douchecanoe</string>
                              <string>ImmortalTec</string>
                              <string>FUCK_YOU_DERRICK</string>
                              <string>SerQwaez</string>
                              <string>clitneyrears</string>
                              <string>porquehuevo</string>
                              <string>HarlemShakesSoCash</string>
                              <string>GaikokuJohn</string>
                              <string>dayvOn_cowboy</string>
                              <string>RocketCow</string>
                              <string>dspman11</string>
                              <string>Squid-drinker</string>
                              <string>minecraft1787</string>
                              <string>DrDoesENTDownRon</string>
                              <string>cadaverco</string>
                              <string>ChefSteph407</string>
                              <string>TostitosGseries</string>
                              <string>MuSiCiSmYGF</string>
                              <string>SER_GIGASCHLONG</string>
                              <string>I_Love_Biscuits</string>
                              <string>mxnoob983</string>
                              <string>Cyn0nymous</string>
                              <string>lantheria</string>
                              <string>stormotron91</string>
                              <string>NikolaiVonToffel</string>
                              <string>King_trident</string>
                              <string>hondatothecivic</string>
                              <string>Fallout2x</string>
                              <string>AnonPaul</string>
                              <string>secret759</string>
                              <string>thebiggestdick</string>
                              <string>Kafke</string>
                              <string>DisMyAccount</string>
                              <string>jistlerummies</string>
                              <string>djguerito</string>
                              <string>stmbtrev</string>
                              <string>Buffaluffasaurus</string>
                              <string>Finglonger76</string>
                              <string>nolimbs</string>
                              <string>two_comedians</string>
                              <string>Ninjacherry</string>
                              <string>I__Know__Things</string>
                              <string>skychok</string>
                              <string>cloaclacola</string>
                              <string>BobTheGuardian</string>
                              <string>keniluck</string>
                              <string>SpelignErrir</string>
                              <string>HastaLasagna</string>
                              <string>danielvutran</string>
                              <string>savoytruffle</string>
                              <string>m3atwad</string>
                              <string>Astara104</string>
                              <string>El_Dubious_Mung</string>
                              <string>TimeTourism</string>
                              <string>cardbross</string>
                              <string>Zappulon</string>
                              <string>Xurandor</string>
                              <string>Only_use_me_TORGUE</string>
                              <string>hXcBassman</string>
                              <string>LadyTronLadyTron</string>
                              <string>These-Days</string>
                              <string>DoubleMintx</string>
                              <string>Deep_Black_Joe</string>
                              <string>HBcubs</string>
                              <string>hershey5914</string>
                              <string>ohforpete</string>
                              <string>OnePieceNPC3</string>
                              <string>licked_cupcake</string>
                              <string>KevRose</string>
                              <string>knightwave</string>
                              <string>stillwtnforbmrecords</string>
                              <string>majinboom</string>
                              <string>RoflCow123</string>
                              <string>KennyGaming</string>
                              <string>I_did_not_bow</string>
                              <string>eldercreedjunkie</string>
                              <string>deedeeiam</string>
                              <string>limitbroken</string>
                              <string>CoolHeadedLogician</string>
                              <string>chaos0510</string>
                              <string>whileyousleep2</string>
                              <string>Addyct</string>
                              <string>spartacus7173</string>
                              <string>RebornYew</string>
                              <string>MirceaChirea</string>
                              <string>SpartanSauce</string>
                              <string>georgiapeach87</string>
                              <string>Russorphan</string>
                              <string>Koofua</string>
                              <string>DigitalKing713</string>
                              <string>mess_is_lore</string>
                              <string>Iamafrayedknot</string>
                              <string>hellrazzer24</string>
                              <string>garbbagebear</string>
                              <string>iamnotamarxist</string>
                              <string>Savergn</string>
                              <string>rcoe</string>
                              <string>CM_Cali</string>
                              <string>FrugalNinja</string>
                              <string>Hayves</string>
                              <string>tenoclockrobot</string>
                              <string>kaseyeeyo</string>
                              <string>twooaktrees</string>
                              <string>miamiron</string>
                              <string>Jigroo</string>
                              <string>Piccologne</string>
                              <string>WhatUpTits</string>
                              <string>Burn4Crimes</string>
                              <string>Tocasvil</string>
                              <string>Harmania</string>
                              <string>yeats666</string>
                              <string>KeeperOfWell</string>
                              <string>New_Slurm</string>
                              <string>NoOne0507</string>
                              <string>scoopeded</string>
                              <string>Tomguydude</string>
                              <string>pagaladmee</string>
                              <string>TheRulingEmperor</string>
                              <string>josh_trick</string>
                              <string>XTCrispy</string>
                              <string>FixesRageGrammar</string>
                              <string>clanso</string>
                              <string>tikitikitikiroom</string>
                              <string>princesskiki</string>
                              <string>Nefelia</string>
                              <string>penningtonant</string>
                              <string>CUDDLEMASTER2</string>
                              <string>crackshot91</string>
                              <string>SurvivorType</string>
                              <string>Mister_Anthony</string>
                              <string>LiliRainicorn</string>
                              <string>key_lime_pie</string>
                              <string>LDM312</string>
                              <string>Turfew</string>
                              <string>remote_production</string>
                              <string>kbo-</string>
                              <string>CochMaestro</string>
                              <string>potato88</string>
                              <string>alzyee</string>
                              <string>Tulabean</string>
                              <string>Dont_Squeeze_me</string>
                              <string>redduser4x</string>
                              <string>SalamanderSylph</string>
                              <string>GentleDoc90</string>
                              <string>Valisk</string>
                              <string>Disciple_of_Crom</string>
                              <string>RinCakes</string>
                              <string>GS059</string>
                              <string>ahmong</string>
                              <string>nosy_coyote</string>
                              <string>aksid</string>
                              <string>spinflux</string>
                              <string>toothfairy32</string>
                              <string>BelugaTaquito</string>
                              <string>fishfilllet</string>
                              <string>esyples</string>
                              <string>tofercakes</string>
                              <string>EndOfDaysss</string>
                              <string>anothernewstart</string>
                              <string>fraincs</string>
                              <string>d0nkeyBOB</string>
                              <string>lazy_elf</string>
                              <string>Mytushilol</string>
                              <string>Xellith</string>
                              <string>kingbarnaby</string>
                              <string>Jack57d</string>
                              <string>redcomet002</string>
                              <string>ronfromcny</string>
                              <string>RexRaider</string>
                              <string>BlackLetterLaw</string>
                              <string>TwoOfSwords</string>
                              <string>kinboyatuwo</string>
                              <string>thatonekid0909</string>
                              <string>Gets_into_debates</string>
                              <string>swimingmainstream</string>
                              <string>afrowarriornabe</string>
                              <string>DankDarko</string>
                              <string>Fatalstryke</string>
                              <string>LeadHero</string>
                              <string>MrToocool101</string>
                              <string>snouchies</string>
                              <string>nuovo_donna</string>
                              <string>moonjellies</string>
                              <string>kingofkyrgyzstan</string>
                              <string>aerialspace</string>
                              <string>logitecharse</string>
                              <string>bigups23</string>
                              <string>DaGhostQc</string>
                              <string>USCswimmer</string>
                              <string>RiotCatland</string>
                              <string>aaabigayle</string>
                              <string>SeptimusSlut</string>
                              <string>Diamond_joe</string>
                              <string>SunnyRaja2</string>
                              <string>Logaline</string>
                              <string>MattAdams53</string>
                              <string>danrennt98</string>
                              <string>beastly_beauty</string>
                              <string>I_Read_an_article</string>
                              <string>geno149</string>
                              <string>cozzpuch</string>
                              <string>papaslurch</string>
                              <string>Nimmrod</string>
                              <string>Vendril</string>
                              <string>Liv-Julia</string>
                              <string>badger035</string>
                              <string>rq4c</string>
                              <string>mandoliinimies</string>
                              <string>nadiamamacita</string>
                              <string>Kinbensha</string>
                              <string>laur7620</string>
                              <string>YoureUsingCoconuts</string>
                              <string>peachypixie</string>
                              <string>Clobbertr0n</string>
                              <string>HAL4500</string>
                              <string>Wankah_</string>
                              <string>z4lachenko</string>
                              <string>lexani4</string>
                              <string>chief34</string>
                              <string>GimmeYourTags</string>
                              <string>9000BeatsPerHour</string>
                              <string>Mordilaa</string>
                              <string>digiorknow</string>
                              <string>FromPakistanwithlove</string>
                              <string>hackcoder</string>
                              <string>bacon_beer_sports</string>
                              <string>RecklessKelly</string>
                              <string>MUPP3T375</string>
                              <string>brassified</string>
                              <string>ficarra1002</string>
                              <string>mushyz</string>
                              <string>_SongsForTheDeaf_</string>
                              <string>rabbitspade</string>
                              <string>juicyfisher</string>
                              <string>MK-007</string>
                              <string>Bobby_Marks</string>
                              <string>EpicGuard</string>
                              <string>The_Monarch79</string>
                              <string>DocRude</string>
                              <string>UprisingSharingan</string>
                              <string>JordanBlythe</string>
                              <string>throwitawayfool2</string>
                              <string>Juan_Solo</string>
                              <string>cyph3x</string>
                              <string>Hushpuppy6</string>
                              <string>Hoobleton</string>
                              <string>ChiboSempai</string>
                              <string>LinuxErrror</string>
                              <string>RnRaintnoisepolution</string>
                              <string>BenignBanan</string>
                              <string>terrorTrain</string>
                              <string>inthe80s</string>
                              <string>ToastedWonder</string>
                              <string>NinjaToss</string>
                              <string>alpharabbit</string>
                              <string>OldUncleDingus</string>
                              <string>SixWireS</string>
                              <string>Asdyc</string>
                              <string>awkwardIRL</string>
                              <string>drirayn</string>
                              <string>i_did_not_enjoy_that</string>
                              <string>whohead63</string>
                              <string>GrandmasPenisHurts</string>
                              <string>THECOACH0742</string>
                              <string>Hoganbeardy</string>
                              <string>VincetheMadMan</string>
                              <string>not_a_famous_actor</string>
                              <string>Krotiuz</string>
                              <string>bodhisatvajr</string>
                              <string>hawkeye_pizza_dog</string>
                              <string>Jesspandapants</string>
                              <string>Dathem</string>
                              <string>strawberrykisses</string>
                              <string>epiclogin</string>
                              <string>BringItOnHome</string>
                              <string>StrictScrutiny</string>
                              <string>neither_party</string>
                              <string>SinnerOfAttention</string>
                              <string>archaicmotion</string>
                              <string>Amplitude</string>
                              <string>houdini_died_of_aids</string>
                              <string>r3dditr3ss</string>
                              <string>another_old_fart</string>
                              <string>SamIsWhite</string>
                              <string>kirkkommander</string>
                              <string>thegarate</string>
                              <string>ParanoidAndroidMan</string>
                              <string>12sea21</string>
                              <string>washedrug</string>
                              <string>noeatnosleep</string>
                              <string>ImArchMageBitch</string>
                              <string>SamIV</string>
                              <string>arenek3</string>
                              <string>Wild_Marker</string>
                              <string>fuckbuddyneeded926</string>
                              <string>joe_zaza</string>
                              <string>cccpcharm</string>
                              <string>mbwasigh</string>
                              <string>cactuscactuscactus</string>
                              <string>Hulkkis</string>
                              <string>sardu1</string>
                              <string>OilofOregano</string>
                              <string>joshthehappy</string>
                              <string>Slick775</string>
                              <string>zhirzzh</string>
                              <string>princess-misandry</string>
                              <string>GloriousNugs</string>
                              <string>GesusKhrist</string>
                              <string>Half-Maus</string>
                              <string>Sprocketlord</string>
                              <string>jorgelukas</string>
                              <string>HPLoveshack</string>
                              <string>does_not_post_ever</string>
                              <string>wojamatas</string>
                              <string>Jean_Val_Jean</string>
                              <string>maltballfalcon</string>
                              <string>xillyriax</string>
                              <string>dUdEmAnBrOcUz</string>
                              <string>redaemon</string>
                              <string>W00ster</string>
                              <string>Butterfly_Riot</string>
                              <string>ChaplnGrillSgt</string>
                              <string>bradyh8</string>
                              <string>GRIMMnM</string>
                              <string>NeonPiggy</string>
                              <string>itsmckenney</string>
                              <string>DjangoSol</string>
                              <string>Khenir</string>
                              <string>TheTiltster</string>
                              <string>PLMessiah</string>
                              <string>cjdcastro</string>
                              <string>garblesnarky</string>
                              <string>hijinksobserved</string>
                              <string>HandsOfJazz</string>
                              <string>Wrath_Of_Aguirre</string>
                              <string>horridhumour</string>
                              <string>Antroh</string>
                              <string>doubleodoug</string>
                              <string>Infintinity</string>
                              <string>jonesy852</string>
                              <string>claywebb7</string>
                              <string>AndrewAtrus</string>
                              <string>Tober04</string>
                              <string>sae1</string>
                              <string>strallweat</string>
                              <string>_DanNYC_</string>
                              <string>HerrCorrector</string>
                              <string>ShadNuke</string>
                              <string>dontjustthrowitaway</string>
                              <string>nucleardemon</string>
                              <string>TheDapperYank</string>
                              <string>Mcallba</string>
                              <string>iwakun</string>
                              <string>kjbigs282</string>
                              <string>czarukus</string>
                              <string>Tw0Bit</string>
                              <string>Sabbatai</string>
                              <string>glay913</string>
                            </indexToSymbolMap>
                          </nominalMapping>
                        </default>
                      </PolynominalAttribute>
                    </attribute>
                  </AttributeRole>
                  <AttributeRole id="23">
                    <special>false</special>
                    <attribute class="PolynominalAttribute" id="24" serialization="custom">
                      <com.rapidminer.example.table.AbstractAttribute>
                        <default>
                          <annotations id="25">
                            <keyValueMap id="26"/>
                          </annotations>
                          <attributeDescription id="27">
                            <name>subreddit</name>
                            <valueType>5</valueType>
                            <blockType>1</blockType>
                            <defaultValue>0.0</defaultValue>
                            <index>1</index>
                          </attributeDescription>
                          <constructionDescription>subreddit</constructionDescription>
                          <statistics class="linked-list" id="28">
                            <NominalStatistics id="29">
                              <mode>-1</mode>
                              <maxCounter>0</maxCounter>
                            </NominalStatistics>
                            <UnknownStatistics id="30">
                              <unknownCounter>0</unknownCounter>
                            </UnknownStatistics>
                          </statistics>
                          <transformations id="31"/>
                        </default>
                      </com.rapidminer.example.table.AbstractAttribute>
                      <PolynominalAttribute>
                        <default>
                          <nominalMapping class="PolynominalMapping" id="32">
                            <symbolToIndexMap id="33">
                              <entry>
                                <string>gonewild</string>
                                <int>56</int>
                              </entry>
                              <entry>
                                <string>tf2</string>
                                <int>179</int>
                              </entry>
                              <entry>
                                <string>malehairadvice</string>
                                <int>249</int>
                              </entry>
                              <entry>
                                <string>CHIBears</string>
                                <int>82</int>
                              </entry>
                              <entry>
                                <string>videos</string>
                                <int>5</int>
                              </entry>
                              <entry>
                                <string>teenagers</string>
                                <int>70</int>
                              </entry>
                              <entry>
                                <string>skyrim</string>
                                <int>247</int>
                              </entry>
                              <entry>
                                <string>trees</string>
                                <int>73</int>
                              </entry>
                              <entry>
                                <string>Civcraft</string>
                                <int>117</int>
                              </entry>
                              <entry>
                                <string>TwoXChromosomes</string>
                                <int>235</int>
                              </entry>
                              <entry>
                                <string>awwnime</string>
                                <int>92</int>
                              </entry>
                              <entry>
                                <string>Nationals</string>
                                <int>250</int>
                              </entry>
                              <entry>
                                <string>pics</string>
                                <int>57</int>
                              </entry>
                              <entry>
                                <string>chicago</string>
                                <int>96</int>
                              </entry>
                              <entry>
                                <string>beer</string>
                                <int>234</int>
                              </entry>
                              <entry>
                                <string>ShitRedditSays</string>
                                <int>246</int>
                              </entry>
                              <entry>
                                <string>fffffffuuuuuuuuuuuu</string>
                                <int>18</int>
                              </entry>
                              <entry>
                                <string>AnimalCrossing</string>
                                <int>140</int>
                              </entry>
                              <entry>
                                <string>facedownassup</string>
                                <int>245</int>
                              </entry>
                              <entry>
                                <string>australia</string>
                                <int>210</int>
                              </entry>
                              <entry>
                                <string>unitedkingdom</string>
                                <int>88</int>
                              </entry>
                              <entry>
                                <string>electronic_cigarette</string>
                                <int>162</int>
                              </entry>
                              <entry>
                                <string>geek</string>
                                <int>81</int>
                              </entry>
                              <entry>
                                <string>newzealand</string>
                                <int>211</int>
                              </entry>
                              <entry>
                                <string>CODZombies</string>
                                <int>113</int>
                              </entry>
                              <entry>
                                <string>ClashOfClans</string>
                                <int>169</int>
                              </entry>
                              <entry>
                                <string>CFB</string>
                                <int>14</int>
                              </entry>
                              <entry>
                                <string>Frat</string>
                                <int>40</int>
                              </entry>
                              <entry>
                                <string>Braveryjerk</string>
                                <int>90</int>
                              </entry>
                              <entry>
                                <string>Torontobluejays</string>
                                <int>164</int>
                              </entry>
                              <entry>
                                <string>MURICA</string>
                                <int>151</int>
                              </entry>
                              <entry>
                                <string>entertainment</string>
                                <int>225</int>
                              </entry>
                              <entry>
                                <string>summonerschool</string>
                                <int>141</int>
                              </entry>
                              <entry>
                                <string>yugioh</string>
                                <int>238</int>
                              </entry>
                              <entry>
                                <string>TheBluePill</string>
                                <int>30</int>
                              </entry>
                              <entry>
                                <string>MLPLounge</string>
                                <int>83</int>
                              </entry>
                              <entry>
                                <string>thatHappened</string>
                                <int>105</int>
                              </entry>
                              <entry>
                                <string>Surface</string>
                                <int>244</int>
                              </entry>
                              <entry>
                                <string>vancouver</string>
                                <int>127</int>
                              </entry>
                              <entry>
                                <string>techtheatre</string>
                                <int>44</int>
                              </entry>
                              <entry>
                                <string>golf</string>
                                <int>76</int>
                              </entry>
                              <entry>
                                <string>Cardinals</string>
                                <int>205</int>
                              </entry>
                              <entry>
                                <string>fantasyfootball</string>
                                <int>192</int>
                              </entry>
                              <entry>
                                <string>TrollXChromosomes</string>
                                <int>77</int>
                              </entry>
                              <entry>
                                <string>circlejerk</string>
                                <int>217</int>
                              </entry>
                              <entry>
                                <string>PercyJacksonRP</string>
                                <int>137</int>
                              </entry>
                              <entry>
                                <string>ladybonersgw</string>
                                <int>206</int>
                              </entry>
                              <entry>
                                <string>funny</string>
                                <int>4</int>
                              </entry>
                              <entry>
                                <string>buildapc</string>
                                <int>135</int>
                              </entry>
                              <entry>
                                <string>Sidehugs</string>
                                <int>222</int>
                              </entry>
                              <entry>
                                <string>democrats</string>
                                <int>52</int>
                              </entry>
                              <entry>
                                <string>Liberal</string>
                                <int>126</int>
                              </entry>
                              <entry>
                                <string>sex</string>
                                <int>201</int>
                              </entry>
                              <entry>
                                <string>jobs</string>
                                <int>232</int>
                              </entry>
                              <entry>
                                <string>leagueoflegends</string>
                                <int>75</int>
                              </entry>
                              <entry>
                                <string>paydaytheheist</string>
                                <int>165</int>
                              </entry>
                              <entry>
                                <string>dayz</string>
                                <int>219</int>
                              </entry>
                              <entry>
                                <string>cringepics</string>
                                <int>21</int>
                              </entry>
                              <entry>
                                <string>ShingekiNoKyojin</string>
                                <int>66</int>
                              </entry>
                              <entry>
                                <string>LeagueOfGiving</string>
                                <int>64</int>
                              </entry>
                              <entry>
                                <string>exmormon</string>
                                <int>259</int>
                              </entry>
                              <entry>
                                <string>im14andthisisfunny</string>
                                <int>16</int>
                              </entry>
                              <entry>
                                <string>Cooking</string>
                                <int>139</int>
                              </entry>
                              <entry>
                                <string>Pareidolia</string>
                                <int>132</int>
                              </entry>
                              <entry>
                                <string>SRSHeroes</string>
                                <int>8</int>
                              </entry>
                              <entry>
                                <string>Metal</string>
                                <int>214</int>
                              </entry>
                              <entry>
                                <string>4chan</string>
                                <int>118</int>
                              </entry>
                              <entry>
                                <string>Bakersfield</string>
                                <int>161</int>
                              </entry>
                              <entry>
                                <string>feminisms</string>
                                <int>186</int>
                              </entry>
                              <entry>
                                <string>airsoft</string>
                                <int>27</int>
                              </entry>
                              <entry>
                                <string>InfinityTrades</string>
                                <int>264</int>
                              </entry>
                              <entry>
                                <string>USMC</string>
                                <int>119</int>
                              </entry>
                              <entry>
                                <string>Gunners</string>
                                <int>25</int>
                              </entry>
                              <entry>
                                <string>NetflixBestOf</string>
                                <int>3</int>
                              </entry>
                              <entry>
                                <string>TrayvonMartin</string>
                                <int>107</int>
                              </entry>
                              <entry>
                                <string>FIFA</string>
                                <int>158</int>
                              </entry>
                              <entry>
                                <string>mylittlepony</string>
                                <int>22</int>
                              </entry>
                              <entry>
                                <string>Gold</string>
                                <int>187</int>
                              </entry>
                              <entry>
                                <string>PrettyLittleLiars</string>
                                <int>94</int>
                              </entry>
                              <entry>
                                <string>Boxing</string>
                                <int>159</int>
                              </entry>
                              <entry>
                                <string>coys</string>
                                <int>129</int>
                              </entry>
                              <entry>
                                <string>minerapocalypse</string>
                                <int>253</int>
                              </entry>
                              <entry>
                                <string>savannah</string>
                                <int>242</int>
                              </entry>
                              <entry>
                                <string>hearthstone</string>
                                <int>36</int>
                              </entry>
                              <entry>
                                <string>fatpeoplestories</string>
                                <int>248</int>
                              </entry>
                              <entry>
                                <string>OkCupid</string>
                                <int>196</int>
                              </entry>
                              <entry>
                                <string>battlefield_4</string>
                                <int>45</int>
                              </entry>
                              <entry>
                                <string>Games</string>
                                <int>67</int>
                              </entry>
                              <entry>
                                <string>WatchItForThePlot</string>
                                <int>243</int>
                              </entry>
                              <entry>
                                <string>SquaredCircle</string>
                                <int>255</int>
                              </entry>
                              <entry>
                                <string>asoiaf</string>
                                <int>39</int>
                              </entry>
                              <entry>
                                <string>MinusTheBear</string>
                                <int>239</int>
                              </entry>
                              <entry>
                                <string>woahdude</string>
                                <int>265</int>
                              </entry>
                              <entry>
                                <string>FixedGearBicycle</string>
                                <int>221</int>
                              </entry>
                              <entry>
                                <string>MapPorn</string>
                                <int>252</int>
                              </entry>
                              <entry>
                                <string>comicbooks</string>
                                <int>87</int>
                              </entry>
                              <entry>
                                <string>AFL</string>
                                <int>37</int>
                              </entry>
                              <entry>
                                <string>OaklandAthletics</string>
                                <int>51</int>
                              </entry>
                              <entry>
                                <string>tf2trade</string>
                                <int>204</int>
                              </entry>
                              <entry>
                                <string>BabyBumps</string>
                                <int>157</int>
                              </entry>
                              <entry>
                                <string>Shotguns</string>
                                <int>188</int>
                              </entry>
                              <entry>
                                <string>india</string>
                                <int>114</int>
                              </entry>
                              <entry>
                                <string>redditgetsdrawn</string>
                                <int>215</int>
                              </entry>
                              <entry>
                                <string>buccos</string>
                                <int>38</int>
                              </entry>
                              <entry>
                                <string>TEFL</string>
                                <int>152</int>
                              </entry>
                              <entry>
                                <string>todayilearned</string>
                                <int>79</int>
                              </entry>
                              <entry>
                                <string>TumblrInAction</string>
                                <int>33</int>
                              </entry>
                              <entry>
                                <string>cringe</string>
                                <int>43</int>
                              </entry>
                              <entry>
                                <string>Frugal</string>
                                <int>124</int>
                              </entry>
                              <entry>
                                <string>CanadianTV</string>
                                <int>153</int>
                              </entry>
                              <entry>
                                <string>news</string>
                                <int>68</int>
                              </entry>
                              <entry>
                                <string>mildlyinfuriating</string>
                                <int>143</int>
                              </entry>
                              <entry>
                                <string>pokemon</string>
                                <int>176</int>
                              </entry>
                              <entry>
                                <string>SuperShibe</string>
                                <int>86</int>
                              </entry>
                              <entry>
                                <string>cigars</string>
                                <int>256</int>
                              </entry>
                              <entry>
                                <string>gats</string>
                                <int>34</int>
                              </entry>
                              <entry>
                                <string>Fifa13</string>
                                <int>115</int>
                              </entry>
                              <entry>
                                <string>technology</string>
                                <int>229</int>
                              </entry>
                              <entry>
                                <string>smashbros</string>
                                <int>224</int>
                              </entry>
                              <entry>
                                <string>OCD</string>
                                <int>122</int>
                              </entry>
                              <entry>
                                <string>Libertarian</string>
                                <int>233</int>
                              </entry>
                              <entry>
                                <string>InfrastructurePorn</string>
                                <int>47</int>
                              </entry>
                              <entry>
                                <string>whatsthisplant</string>
                                <int>156</int>
                              </entry>
                              <entry>
                                <string>wrestling</string>
                                <int>178</int>
                              </entry>
                              <entry>
                                <string>mflb</string>
                                <int>223</int>
                              </entry>
                              <entry>
                                <string>QuotesPorn</string>
                                <int>212</int>
                              </entry>
                              <entry>
                                <string>aww</string>
                                <int>98</int>
                              </entry>
                              <entry>
                                <string>guns</string>
                                <int>93</int>
                              </entry>
                              <entry>
                                <string>atheism</string>
                                <int>95</int>
                              </entry>
                              <entry>
                                <string>dataisbeautiful</string>
                                <int>257</int>
                              </entry>
                              <entry>
                                <string>WTF</string>
                                <int>19</int>
                              </entry>
                              <entry>
                                <string>gamegrumps</string>
                                <int>104</int>
                              </entry>
                              <entry>
                                <string>gmod</string>
                                <int>71</int>
                              </entry>
                              <entry>
                                <string>GiftofGames</string>
                                <int>241</int>
                              </entry>
                              <entry>
                                <string>learnprogramming</string>
                                <int>182</int>
                              </entry>
                              <entry>
                                <string>BigBrother</string>
                                <int>91</int>
                              </entry>
                              <entry>
                                <string>movies</string>
                                <int>11</int>
                              </entry>
                              <entry>
                                <string>hometheater</string>
                                <int>170</int>
                              </entry>
                              <entry>
                                <string>devils</string>
                                <int>195</int>
                              </entry>
                              <entry>
                                <string>relationships</string>
                                <int>6</int>
                              </entry>
                              <entry>
                                <string>designthought</string>
                                <int>106</int>
                              </entry>
                              <entry>
                                <string>WritingPrompts</string>
                                <int>175</int>
                              </entry>
                              <entry>
                                <string>hcfactions</string>
                                <int>121</int>
                              </entry>
                              <entry>
                                <string>awakened</string>
                                <int>97</int>
                              </entry>
                              <entry>
                                <string>AdviceAnimals</string>
                                <int>35</int>
                              </entry>
                              <entry>
                                <string>Gore</string>
                                <int>84</int>
                              </entry>
                              <entry>
                                <string>Dexter</string>
                                <int>149</int>
                              </entry>
                              <entry>
                                <string>gifs</string>
                                <int>7</int>
                              </entry>
                              <entry>
                                <string>pokemontrades</string>
                                <int>254</int>
                              </entry>
                              <entry>
                                <string>WhiteRights</string>
                                <int>46</int>
                              </entry>
                              <entry>
                                <string>RotMG</string>
                                <int>49</int>
                              </entry>
                              <entry>
                                <string>gamernews</string>
                                <int>258</int>
                              </entry>
                              <entry>
                                <string>changemyview</string>
                                <int>183</int>
                              </entry>
                              <entry>
                                <string>ffxiv</string>
                                <int>145</int>
                              </entry>
                              <entry>
                                <string>explainlikeIAmA</string>
                                <int>65</int>
                              </entry>
                              <entry>
                                <string>teslore</string>
                                <int>146</int>
                              </entry>
                              <entry>
                                <string>nba</string>
                                <int>261</int>
                              </entry>
                              <entry>
                                <string>redsox</string>
                                <int>61</int>
                              </entry>
                              <entry>
                                <string>creepyPMs</string>
                                <int>54</int>
                              </entry>
                              <entry>
                                <string>HogwartsRP</string>
                                <int>230</int>
                              </entry>
                              <entry>
                                <string>cars</string>
                                <int>200</int>
                              </entry>
                              <entry>
                                <string>ForeverAlone</string>
                                <int>29</int>
                              </entry>
                              <entry>
                                <string>hiphopheads</string>
                                <int>23</int>
                              </entry>
                              <entry>
                                <string>ImGoingToHellForThis</string>
                                <int>100</int>
                              </entry>
                              <entry>
                                <string>pettyrevenge</string>
                                <int>17</int>
                              </entry>
                              <entry>
                                <string>PandR</string>
                                <int>236</int>
                              </entry>
                              <entry>
                                <string>Drugs</string>
                                <int>166</int>
                              </entry>
                              <entry>
                                <string>RandomActsOfGaming</string>
                                <int>202</int>
                              </entry>
                              <entry>
                                <string>Random_Acts_Of_Amazon</string>
                                <int>102</int>
                              </entry>
                              <entry>
                                <string>sandiego</string>
                                <int>251</int>
                              </entry>
                              <entry>
                                <string>gardening</string>
                                <int>108</int>
                              </entry>
                              <entry>
                                <string>wiiu</string>
                                <int>85</int>
                              </entry>
                              <entry>
                                <string>bicycling</string>
                                <int>194</int>
                              </entry>
                              <entry>
                                <string>snakes</string>
                                <int>207</int>
                              </entry>
                              <entry>
                                <string>battlestations</string>
                                <int>171</int>
                              </entry>
                              <entry>
                                <string>hailhailhailcorporate</string>
                                <int>48</int>
                              </entry>
                              <entry>
                                <string>Fallout</string>
                                <int>24</int>
                              </entry>
                              <entry>
                                <string>Hiphopcirclejerk</string>
                                <int>123</int>
                              </entry>
                              <entry>
                                <string>SouthBend</string>
                                <int>209</int>
                              </entry>
                              <entry>
                                <string>evangelion</string>
                                <int>0</int>
                              </entry>
                              <entry>
                                <string>GrandLineRP</string>
                                <int>138</int>
                              </entry>
                              <entry>
                                <string>fcs</string>
                                <int>28</int>
                              </entry>
                              <entry>
                                <string>writing</string>
                                <int>144</int>
                              </entry>
                              <entry>
                                <string>malelivingspace</string>
                                <int>220</int>
                              </entry>
                              <entry>
                                <string>hockey</string>
                                <int>181</int>
                              </entry>
                              <entry>
                                <string>gaming</string>
                                <int>32</int>
                              </entry>
                              <entry>
                                <string>relationship_advice</string>
                                <int>60</int>
                              </entry>
                              <entry>
                                <string>Jeep</string>
                                <int>262</int>
                              </entry>
                              <entry>
                                <string>orioles</string>
                                <int>110</int>
                              </entry>
                              <entry>
                                <string>television</string>
                                <int>130</int>
                              </entry>
                              <entry>
                                <string>keto</string>
                                <int>99</int>
                              </entry>
                              <entry>
                                <string>TheNarutoWorld</string>
                                <int>263</int>
                              </entry>
                              <entry>
                                <string>nexus4</string>
                                <int>148</int>
                              </entry>
                              <entry>
                                <string>Minecraft</string>
                                <int>2</int>
                              </entry>
                              <entry>
                                <string>shibe</string>
                                <int>112</int>
                              </entry>
                              <entry>
                                <string>rage</string>
                                <int>173</int>
                              </entry>
                              <entry>
                                <string>AskReddit</string>
                                <int>1</int>
                              </entry>
                              <entry>
                                <string>pitbulls</string>
                                <int>190</int>
                              </entry>
                              <entry>
                                <string>explainlikeimfive</string>
                                <int>13</int>
                              </entry>
                              <entry>
                                <string>digimon</string>
                                <int>59</int>
                              </entry>
                              <entry>
                                <string>Fitness</string>
                                <int>41</int>
                              </entry>
                              <entry>
                                <string>GoneWildPlus</string>
                                <int>69</int>
                              </entry>
                              <entry>
                                <string>Meditation</string>
                                <int>208</int>
                              </entry>
                              <entry>
                                <string>FanTheories</string>
                                <int>142</int>
                              </entry>
                              <entry>
                                <string>Badfaketexts</string>
                                <int>74</int>
                              </entry>
                              <entry>
                                <string>aves</string>
                                <int>198</int>
                              </entry>
                              <entry>
                                <string>MetalMemes</string>
                                <int>50</int>
                              </entry>
                              <entry>
                                <string>TF2fashionadvice</string>
                                <int>154</int>
                              </entry>
                              <entry>
                                <string>conspiracy</string>
                                <int>155</int>
                              </entry>
                              <entry>
                                <string>argentina</string>
                                <int>147</int>
                              </entry>
                              <entry>
                                <string>AMA</string>
                                <int>216</int>
                              </entry>
                              <entry>
                                <string>breakingbad</string>
                                <int>177</int>
                              </entry>
                              <entry>
                                <string>worldnews</string>
                                <int>12</int>
                              </entry>
                              <entry>
                                <string>makinghiphop</string>
                                <int>203</int>
                              </entry>
                              <entry>
                                <string>Idiots</string>
                                <int>89</int>
                              </entry>
                              <entry>
                                <string>DebateReligion</string>
                                <int>125</int>
                              </entry>
                              <entry>
                                <string>Borderlands</string>
                                <int>133</int>
                              </entry>
                              <entry>
                                <string>circlejerkbreakingbad</string>
                                <int>120</int>
                              </entry>
                              <entry>
                                <string>askcarsales</string>
                                <int>191</int>
                              </entry>
                              <entry>
                                <string>supermoto</string>
                                <int>240</int>
                              </entry>
                              <entry>
                                <string>n64</string>
                                <int>231</int>
                              </entry>
                              <entry>
                                <string>TheFence</string>
                                <int>20</int>
                              </entry>
                              <entry>
                                <string>tinydick</string>
                                <int>136</int>
                              </entry>
                              <entry>
                                <string>AskNYC</string>
                                <int>199</int>
                              </entry>
                              <entry>
                                <string>Android</string>
                                <int>72</int>
                              </entry>
                              <entry>
                                <string>amateurcumsluts</string>
                                <int>63</int>
                              </entry>
                              <entry>
                                <string>AskSciTech</string>
                                <int>180</int>
                              </entry>
                              <entry>
                                <string>BDSMcommunity</string>
                                <int>172</int>
                              </entry>
                              <entry>
                                <string>nfl</string>
                                <int>53</int>
                              </entry>
                              <entry>
                                <string>CityPorn</string>
                                <int>218</int>
                              </entry>
                              <entry>
                                <string>metalgearsolid</string>
                                <int>26</int>
                              </entry>
                              <entry>
                                <string>Swingers</string>
                                <int>184</int>
                              </entry>
                              <entry>
                                <string>getdisciplined</string>
                                <int>185</int>
                              </entry>
                              <entry>
                                <string>bestof</string>
                                <int>55</int>
                              </entry>
                              <entry>
                                <string>schizoaffective</string>
                                <int>189</int>
                              </entry>
                              <entry>
                                <string>whowouldwin</string>
                                <int>168</int>
                              </entry>
                              <entry>
                                <string>YouShouldKnow</string>
                                <int>111</int>
                              </entry>
                              <entry>
                                <string>lifehacks</string>
                                <int>78</int>
                              </entry>
                              <entry>
                                <string>politics</string>
                                <int>160</int>
                              </entry>
                              <entry>
                                <string>MassiveCock</string>
                                <int>103</int>
                              </entry>
                              <entry>
                                <string>bestofworldstar</string>
                                <int>31</int>
                              </entry>
                              <entry>
                                <string>cringejerk</string>
                                <int>101</int>
                              </entry>
                              <entry>
                                <string>AskMen</string>
                                <int>128</int>
                              </entry>
                              <entry>
                                <string>islam</string>
                                <int>226</int>
                              </entry>
                              <entry>
                                <string>childfree</string>
                                <int>9</int>
                              </entry>
                              <entry>
                                <string>socialskills</string>
                                <int>58</int>
                              </entry>
                              <entry>
                                <string>UpliftingNews</string>
                                <int>62</int>
                              </entry>
                              <entry>
                                <string>ImaginaryLandscapes</string>
                                <int>80</int>
                              </entry>
                              <entry>
                                <string>nsfw2</string>
                                <int>10</int>
                              </entry>
                              <entry>
                                <string>soccer</string>
                                <int>15</int>
                              </entry>
                              <entry>
                                <string>Civcraft_Orion</string>
                                <int>228</int>
                              </entry>
                              <entry>
                                <string>amiugly</string>
                                <int>260</int>
                              </entry>
                              <entry>
                                <string>gameofthrones</string>
                                <int>167</int>
                              </entry>
                              <entry>
                                <string>Christianity</string>
                                <int>193</int>
                              </entry>
                              <entry>
                                <string>worldpolitics</string>
                                <int>174</int>
                              </entry>
                              <entry>
                                <string>SultansOfStats</string>
                                <int>42</int>
                              </entry>
                              <entry>
                                <string>GrandTheftAutoV</string>
                                <int>131</int>
                              </entry>
                              <entry>
                                <string>SilkRoad</string>
                                <int>237</int>
                              </entry>
                              <entry>
                                <string>personalfinance</string>
                                <int>116</int>
                              </entry>
                              <entry>
                                <string>Steam</string>
                                <int>197</int>
                              </entry>
                              <entry>
                                <string>Catholicism</string>
                                <int>163</int>
                              </entry>
                              <entry>
                                <string>ACTrade</string>
                                <int>213</int>
                              </entry>
                              <entry>
                                <string>MvC3</string>
                                <int>109</int>
                              </entry>
                              <entry>
                                <string>Destiny</string>
                                <int>134</int>
                              </entry>
                              <entry>
                                <string>talesfromtechsupport</string>
                                <int>150</int>
                              </entry>
                              <entry>
                                <string>anime</string>
                                <int>227</int>
                              </entry>
                            </symbolToIndexMap>
                            <indexToSymbolMap id="34">
                              <string>evangelion</string>
                              <string>AskReddit</string>
                              <string>Minecraft</string>
                              <string>NetflixBestOf</string>
                              <string>funny</string>
                              <string>videos</string>
                              <string>relationships</string>
                              <string>gifs</string>
                              <string>SRSHeroes</string>
                              <string>childfree</string>
                              <string>nsfw2</string>
                              <string>movies</string>
                              <string>worldnews</string>
                              <string>explainlikeimfive</string>
                              <string>CFB</string>
                              <string>soccer</string>
                              <string>im14andthisisfunny</string>
                              <string>pettyrevenge</string>
                              <string>fffffffuuuuuuuuuuuu</string>
                              <string>WTF</string>
                              <string>TheFence</string>
                              <string>cringepics</string>
                              <string>mylittlepony</string>
                              <string>hiphopheads</string>
                              <string>Fallout</string>
                              <string>Gunners</string>
                              <string>metalgearsolid</string>
                              <string>airsoft</string>
                              <string>fcs</string>
                              <string>ForeverAlone</string>
                              <string>TheBluePill</string>
                              <string>bestofworldstar</string>
                              <string>gaming</string>
                              <string>TumblrInAction</string>
                              <string>gats</string>
                              <string>AdviceAnimals</string>
                              <string>hearthstone</string>
                              <string>AFL</string>
                              <string>buccos</string>
                              <string>asoiaf</string>
                              <string>Frat</string>
                              <string>Fitness</string>
                              <string>SultansOfStats</string>
                              <string>cringe</string>
                              <string>techtheatre</string>
                              <string>battlefield_4</string>
                              <string>WhiteRights</string>
                              <string>InfrastructurePorn</string>
                              <string>hailhailhailcorporate</string>
                              <string>RotMG</string>
                              <string>MetalMemes</string>
                              <string>OaklandAthletics</string>
                              <string>democrats</string>
                              <string>nfl</string>
                              <string>creepyPMs</string>
                              <string>bestof</string>
                              <string>gonewild</string>
                              <string>pics</string>
                              <string>socialskills</string>
                              <string>digimon</string>
                              <string>relationship_advice</string>
                              <string>redsox</string>
                              <string>UpliftingNews</string>
                              <string>amateurcumsluts</string>
                              <string>LeagueOfGiving</string>
                              <string>explainlikeIAmA</string>
                              <string>ShingekiNoKyojin</string>
                              <string>Games</string>
                              <string>news</string>
                              <string>GoneWildPlus</string>
                              <string>teenagers</string>
                              <string>gmod</string>
                              <string>Android</string>
                              <string>trees</string>
                              <string>Badfaketexts</string>
                              <string>leagueoflegends</string>
                              <string>golf</string>
                              <string>TrollXChromosomes</string>
                              <string>lifehacks</string>
                              <string>todayilearned</string>
                              <string>ImaginaryLandscapes</string>
                              <string>geek</string>
                              <string>CHIBears</string>
                              <string>MLPLounge</string>
                              <string>Gore</string>
                              <string>wiiu</string>
                              <string>SuperShibe</string>
                              <string>comicbooks</string>
                              <string>unitedkingdom</string>
                              <string>Idiots</string>
                              <string>Braveryjerk</string>
                              <string>BigBrother</string>
                              <string>awwnime</string>
                              <string>guns</string>
                              <string>PrettyLittleLiars</string>
                              <string>atheism</string>
                              <string>chicago</string>
                              <string>awakened</string>
                              <string>aww</string>
                              <string>keto</string>
                              <string>ImGoingToHellForThis</string>
                              <string>cringejerk</string>
                              <string>Random_Acts_Of_Amazon</string>
                              <string>MassiveCock</string>
                              <string>gamegrumps</string>
                              <string>thatHappened</string>
                              <string>designthought</string>
                              <string>TrayvonMartin</string>
                              <string>gardening</string>
                              <string>MvC3</string>
                              <string>orioles</string>
                              <string>YouShouldKnow</string>
                              <string>shibe</string>
                              <string>CODZombies</string>
                              <string>india</string>
                              <string>Fifa13</string>
                              <string>personalfinance</string>
                              <string>Civcraft</string>
                              <string>4chan</string>
                              <string>USMC</string>
                              <string>circlejerkbreakingbad</string>
                              <string>hcfactions</string>
                              <string>OCD</string>
                              <string>Hiphopcirclejerk</string>
                              <string>Frugal</string>
                              <string>DebateReligion</string>
                              <string>Liberal</string>
                              <string>vancouver</string>
                              <string>AskMen</string>
                              <string>coys</string>
                              <string>television</string>
                              <string>GrandTheftAutoV</string>
                              <string>Pareidolia</string>
                              <string>Borderlands</string>
                              <string>Destiny</string>
                              <string>buildapc</string>
                              <string>tinydick</string>
                              <string>PercyJacksonRP</string>
                              <string>GrandLineRP</string>
                              <string>Cooking</string>
                              <string>AnimalCrossing</string>
                              <string>summonerschool</string>
                              <string>FanTheories</string>
                              <string>mildlyinfuriating</string>
                              <string>writing</string>
                              <string>ffxiv</string>
                              <string>teslore</string>
                              <string>argentina</string>
                              <string>nexus4</string>
                              <string>Dexter</string>
                              <string>talesfromtechsupport</string>
                              <string>MURICA</string>
                              <string>TEFL</string>
                              <string>CanadianTV</string>
                              <string>TF2fashionadvice</string>
                              <string>conspiracy</string>
                              <string>whatsthisplant</string>
                              <string>BabyBumps</string>
                              <string>FIFA</string>
                              <string>Boxing</string>
                              <string>politics</string>
                              <string>Bakersfield</string>
                              <string>electronic_cigarette</string>
                              <string>Catholicism</string>
                              <string>Torontobluejays</string>
                              <string>paydaytheheist</string>
                              <string>Drugs</string>
                              <string>gameofthrones</string>
                              <string>whowouldwin</string>
                              <string>ClashOfClans</string>
                              <string>hometheater</string>
                              <string>battlestations</string>
                              <string>BDSMcommunity</string>
                              <string>rage</string>
                              <string>worldpolitics</string>
                              <string>WritingPrompts</string>
                              <string>pokemon</string>
                              <string>breakingbad</string>
                              <string>wrestling</string>
                              <string>tf2</string>
                              <string>AskSciTech</string>
                              <string>hockey</string>
                              <string>learnprogramming</string>
                              <string>changemyview</string>
                              <string>Swingers</string>
                              <string>getdisciplined</string>
                              <string>feminisms</string>
                              <string>Gold</string>
                              <string>Shotguns</string>
                              <string>schizoaffective</string>
                              <string>pitbulls</string>
                              <string>askcarsales</string>
                              <string>fantasyfootball</string>
                              <string>Christianity</string>
                              <string>bicycling</string>
                              <string>devils</string>
                              <string>OkCupid</string>
                              <string>Steam</string>
                              <string>aves</string>
                              <string>AskNYC</string>
                              <string>cars</string>
                              <string>sex</string>
                              <string>RandomActsOfGaming</string>
                              <string>makinghiphop</string>
                              <string>tf2trade</string>
                              <string>Cardinals</string>
                              <string>ladybonersgw</string>
                              <string>snakes</string>
                              <string>Meditation</string>
                              <string>SouthBend</string>
                              <string>australia</string>
                              <string>newzealand</string>
                              <string>QuotesPorn</string>
                              <string>ACTrade</string>
                              <string>Metal</string>
                              <string>redditgetsdrawn</string>
                              <string>AMA</string>
                              <string>circlejerk</string>
                              <string>CityPorn</string>
                              <string>dayz</string>
                              <string>malelivingspace</string>
                              <string>FixedGearBicycle</string>
                              <string>Sidehugs</string>
                              <string>mflb</string>
                              <string>smashbros</string>
                              <string>entertainment</string>
                              <string>islam</string>
                              <string>anime</string>
                              <string>Civcraft_Orion</string>
                              <string>technology</string>
                              <string>HogwartsRP</string>
                              <string>n64</string>
                              <string>jobs</string>
                              <string>Libertarian</string>
                              <string>beer</string>
                              <string>TwoXChromosomes</string>
                              <string>PandR</string>
                              <string>SilkRoad</string>
                              <string>yugioh</string>
                              <string>MinusTheBear</string>
                              <string>supermoto</string>
                              <string>GiftofGames</string>
                              <string>savannah</string>
                              <string>WatchItForThePlot</string>
                              <string>Surface</string>
                              <string>facedownassup</string>
                              <string>ShitRedditSays</string>
                              <string>skyrim</string>
                              <string>fatpeoplestories</string>
                              <string>malehairadvice</string>
                              <string>Nationals</string>
                              <string>sandiego</string>
                              <string>MapPorn</string>
                              <string>minerapocalypse</string>
                              <string>pokemontrades</string>
                              <string>SquaredCircle</string>
                              <string>cigars</string>
                              <string>dataisbeautiful</string>
                              <string>gamernews</string>
                              <string>exmormon</string>
                              <string>amiugly</string>
                              <string>nba</string>
                              <string>Jeep</string>
                              <string>TheNarutoWorld</string>
                              <string>InfinityTrades</string>
                              <string>woahdude</string>
                            </indexToSymbolMap>
                          </nominalMapping>
                        </default>
                      </PolynominalAttribute>
                    </attribute>
                  </AttributeRole>
                  <AttributeRole id="35">
                    <special>false</special>
                    <attribute class="PolynominalAttribute" id="36" serialization="custom">
                      <com.rapidminer.example.table.AbstractAttribute>
                        <default>
                          <annotations id="37">
                            <keyValueMap id="38"/>
                          </annotations>
                          <attributeDescription id="39">
                            <name>body</name>
                            <valueType>5</valueType>
                            <blockType>1</blockType>
                            <defaultValue>0.0</defaultValue>
                            <index>2</index>
                          </attributeDescription>
                          <constructionDescription>body</constructionDescription>
                          <statistics class="linked-list" id="40">
                            <NominalStatistics id="41">
                              <mode>-1</mode>
                              <maxCounter>0</maxCounter>
                            </NominalStatistics>
                            <UnknownStatistics id="42">
                              <unknownCounter>0</unknownCounter>
                            </UnknownStatistics>
                          </statistics>
                          <transformations id="43"/>
                        </default>
                      </com.rapidminer.example.table.AbstractAttribute>
                      <PolynominalAttribute>
                        <default>
                          <nominalMapping class="PolynominalMapping" id="44">
                            <symbolToIndexMap id="45">
                              <entry>
                                <string>I still fap to this movie sometimes. Hell i still would do her.</string>
                                <int>533</int>
                              </entry>
                              <entry>
                                <string>If you are not on a family plan there are a lot of good / better options outside the major carriers so I would highly advise against starting a contract if your family is not paying for it. </string>
                                <int>371</int>
                              </entry>
                              <entry>
                                <string>That guy&apos;s a fucking cunt.</string>
                                <int>106</int>
                              </entry>
                              <entry>
                                <string>I came.</string>
                                <int>543</int>
                              </entry>
                              <entry>
                                <string>boo fucking hoo</string>
                                <int>48</int>
                              </entry>
                              <entry>
                                <string>THANK YOU </string>
                                <int>2</int>
                              </entry>
                              <entry>
                                <string>Nebraska @ Baylor in...2001? It was awhile back. I was in the bathroom, and the urinal is essentially one giant tub that everyone pisses into so you&apos;re usually to close to whoever is next to you. A Nebraska fan is relieving himself and a Baylor fan walks up to him and yells Cornfucker Cornfucker!&quot; about 2 inches from his face. The Nebraska fan proceeds to turn and piss on the Baylor fan and the two start fighting. Highlight of the game.&quot;</string>
                                <int>188</int>
                              </entry>
                              <entry>
                                <string>Holy self-righteous masturbation batman. </string>
                                <int>175</int>
                              </entry>
                              <entry>
                                <string>That is what he said, nigger</string>
                                <int>116</int>
                              </entry>
                              <entry>
                                <string>If I was to pick a second team, i think it would have to be NYI as well, they were in my shortlist of who to pick and I like the idea of keeping the orange/blue theme going.  Also, happy cake day!</string>
                                <int>373</int>
                              </entry>
                              <entry>
                                <string>Eventually. I have some rats right now that I love and I don&apos;t want them to be in fear since I&apos;ll be moving into an apartment this weekend they wouldn&apos;t have separate rooms. My Gf still thinks snakes are evil but I&apos;m hoping to change her mind about it or at least let me have a snake that she doesn&apos;t personally have to hold.</string>
                                <int>431</int>
                              </entry>
                              <entry>
                                <string>shut up slut</string>
                                <int>22</int>
                              </entry>
                              <entry>
                                <string>HOLY SHIT WHAT A THROW I DIDN&apos;T KNOW NOVA COULD DO THAT</string>
                                <int>226</int>
                              </entry>
                              <entry>
                                <string>Most disappointing 16-0 playoff run ever.</string>
                                <int>468</int>
                              </entry>
                              <entry>
                                <string>Okay, what&apos;s wrong with the I-Cues? As someone who&apos;s only done lighting on a budget, those motherfuckers were useful as hell when we needed a little fill. </string>
                                <int>59</int>
                              </entry>
                              <entry>
                                <string>someone help that poor, retarded child. </string>
                                <int>464</int>
                              </entry>
                              <entry>
                                <string>*slashes both swords across the yetis stomach, turns and jogs toward you holding my side again* Tyler!</string>
                                <int>297</int>
                              </entry>
                              <entry>
                                <string>;_;7 RIP IN PEACE YOU MAGNIFICENT BASTARD</string>
                                <int>0</int>
                              </entry>
                              <entry>
                                <string>So pretty much nothing as usual..</string>
                                <int>434</int>
                              </entry>
                              <entry>
                                <string>Soooo.... Why is this so controversial that the news sites AND imgur are removing it?</string>
                                <int>574</int>
                              </entry>
                              <entry>
                                <string>No, I agree with you.  The landlord seems to be concerned with his property, which is his right. While he might be a bit nosy, I don&apos;t see it as invasive- he gave 24 hours notice of inspection.</string>
                                <int>372</int>
                              </entry>
                              <entry>
                                <string>Riiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiisp</string>
                                <int>212</int>
                              </entry>
                              <entry>
                                <string>come. fucking. on.</string>
                                <int>70</int>
                              </entry>
                              <entry>
                                <string>All you need to do is:  a) provide extremely thorough context for the statement which got you accused of being a racist,  b) not sound like you&apos;re reaching,  c) not accidentally say something else racist while doing so.</string>
                                <int>386</int>
                              </entry>
                              <entry>
                                <string>Wow I didn&apos;t know that fox show was based of this comic....That being said is the show any good? Because I&apos;ve just been reading these comics now for the past half hour and it&apos;s been great. </string>
                                <int>369</int>
                              </entry>
                              <entry>
                                <string>Hot glue motherfucker</string>
                                <int>83</int>
                              </entry>
                              <entry>
                                <string>**NPC**  ---  *The badger walks away, not caring about a silly human, but near by he finds three herbs*</string>
                                <int>299</int>
                              </entry>
                              <entry>
                                <string>I&apos;m shocked. Really. </string>
                                <int>384</int>
                              </entry>
                              <entry>
                                <string>No. Give me your wand.   OOC: Alright. No problem. :)</string>
                                <int>505</int>
                              </entry>
                              <entry>
                                <string>If there&apos;s one thing I learned while watching anime, it&apos;s this:  There are three different types of anime watchers/fans.  1. The weeaboo narutard. Essentially everything that makes you hate anime fans. Fuck these guys.  2. Hardcore anime otaku. The other end of the spectrum. Generally bearable, but they&apos;ll nerd out over anime stuff. Basically like any other nerd/geek, but with anime. If you are used to being around nerds/geeks, these guys are usually fine.  3. Regular viewers. Basically anyone who watches anime casually or doesn&apos;t fit into one of the first two categories. I know lots of people who didn&apos;t know much about anime and are now in this category. Normal people + anime.  I&apos;d say check out: Death Note, Mirai Nikki, Steins;Gate, and Time of Eve. This&apos;ll get you started if you really want to check it out. Not all of it is the negative-stereotype stuff.</string>
                                <int>264</int>
                              </entry>
                              <entry>
                                <string>Kia Rio or 2014 Kia Forte</string>
                                <int>401</int>
                              </entry>
                              <entry>
                                <string>I lived in KCMO for less than a year going on 9 years ago now. When I was there it was a complete shithole. We actually broke our lease to leave. Bittersweet, because we found our way back to Canada from there. I now see it has turned around significantly for the better. (not just a picture, but what I have been reading over the past few years). Would love to give er a go again. </string>
                                <int>458</int>
                              </entry>
                              <entry>
                                <string>Yeah those three in Western Canada are a bitch. I&apos;ve never managed to figure them out.</string>
                                <int>282</int>
                              </entry>
                              <entry>
                                <string>Yay, one less dirty nigger.</string>
                                <int>109</int>
                              </entry>
                              <entry>
                                <string>As a Canadian-American, you&apos;re thankcome.</string>
                                <int>469</int>
                              </entry>
                              <entry>
                                <string>You shouldn&apos;t do it because litterers are faggots.  </string>
                                <int>215</int>
                              </entry>
                              <entry>
                                <string>Aw, man.  I not only drop $20 on scallops, I drop it on ~frozen~ seafood. I need to move to the coast. </string>
                                <int>298</int>
                              </entry>
                              <entry>
                                <string>Who would want a devils threesome?</string>
                                <int>139</int>
                              </entry>
                              <entry>
                                <string>I tried to bang this chick in a wheelchair I met once but I got nowhere. That&apos;s the extent of my experience into banging the handicapped. </string>
                                <int>39</int>
                              </entry>
                              <entry>
                                <string>walter jr?</string>
                                <int>295</int>
                              </entry>
                              <entry>
                                <string>[](/rariwhy)I really, *really* hope that when you replay a mission, it includes cutscenes, too. (Completely skippable, of course.)  OR LET US PAUSE THE CUTSCENE, PLEASE. :O</string>
                                <int>360</int>
                              </entry>
                              <entry>
                                <string>you... you bastard.</string>
                                <int>72</int>
                              </entry>
                              <entry>
                                <string>Well, on one hand, I feel bad for your cockblock... but on the other hand, I sympathize with ditched guy. The girls didn&apos;t think that through, they needed a distraction girl. The fat girl that is gonna be a virgin until she moves away for college then becomes a total slut girl. I&apos;m sure ditched goody goody guy would have loved a blow job too - it sucks being the ditched guy. You have a half-cool memory, ditched guy just feels left out :(</string>
                                <int>187</int>
                              </entry>
                              <entry>
                                <string>I don&apos;t believe in Lucifer. </string>
                                <int>410</int>
                              </entry>
                              <entry>
                                <string>You american hating commie bastard</string>
                                <int>140</int>
                              </entry>
                              <entry>
                                <string>What if I told you, as a fat ass american, I lost 60lbs this year (225 &amp;gt;&amp;gt; 165) doing nothing but cutting out HFCS (fructose glucose for our UK friends signing in).  Gorge away, just be cognizant.  This meal sounds delicious.  I learned this simple shit over 2 weeks while in London,  No one is fat, no one drinks soda.  It&apos;s not irony.  It&apos;s lies.</string>
                                <int>266</int>
                              </entry>
                              <entry>
                                <string>It costs the bank incredibly little to store your money, certainly not anywhere near the realm of $15. Besides that, they are not paying almost any of it back to anyone. .5% interest, really banks? They are making tons of money off loans. The banks can afford not to jab the poor people, but they do, because they can.   Not to mention the copious amounts of money they have made because of deregulation and the FED giving them near 0% loans, to load out to people. </string>
                                <int>484</int>
                              </entry>
                              <entry>
                                <string>I&apos;ll steer the luxury cruise ship that is your shlong into the glorious harbor of masturbation.</string>
                                <int>291</int>
                              </entry>
                              <entry>
                                <string>Would that mean that Mike could still be used? </string>
                                <int>485</int>
                              </entry>
                              <entry>
                                <string>░░░░░░░░░▄░░░░░░░░░░░░░░▄ ░░░░░░░░▌▒█░░░░░░░░░░░▄▀▒▌ ░░░░░░░░▌▒▒█░░░░░░░░▄▀▒▒▒▐ ░░░░░░░▐▄▀▒▒▀▀▀▀▄▄▄▀▒▒▒▒▒▐ ░░░░░▄▄▀▒░▒▒▒▒▒▒▒▒▒█▒▒▄█▒▐ ░░░▄▀▒▒▒░░░▒▒▒░░░▒▒▒▀██▀▒▌ ░░▐▒▒▒▄▄▒▒▒▒░░░▒▒▒▒▒▒▒▀▄▒▒▌ ░░▌░░▌█▀▒▒▒▒▒▄▀█▄▒▒▒▒▒▒▒█▒▐ ░▐░░░▒▒▒▒▒▒▒▒▌██▀▒▒░░░▒▒▒▀▄▌ ░▌░▒▄██▄▒▒▒▒▒▒▒▒▒░░░░░░▒▒▒▒▌ ▀▒▀▐▄█▄█▌▄░▀▒▒░░░░░░░░░░▒▒▒▐ ▐▒▒▐▀▐▀▒░▄▄▒▄▒▒▒▒▒▒░▒░▒░▒▒▒▒▌ ▐▒▒▒▀▀▄▄▒▒▒▄▒▒▒▒▒▒▒▒░▒░▒░▒▒▐ ░▌▒▒▒▒▒▒▀▀▀▒▒▒▒▒▒░▒░▒░▒░▒▒▒▌ ░▐▒▒▒▒▒▒▒▒▒▒▒▒▒▒░▒░▒░▒▒▄▒▒▐ ░░▀▄▒▒▒▒▒▒▒▒▒▒▒░▒░▒░▒▄▒▒▒▒▌ ░░░░▀▄▒▒▒▒▒▒▒▒▒▒▄▄▄▀▒▒▒▒▄▀ ░░░░░░▀▄▄▄▄▄▄▀▀▀▒▒▒▒▒▄▄▀ ░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▀▀ wow so brutal</string>
                                <int>221</int>
                              </entry>
                              <entry>
                                <string>Shit title.</string>
                                <int>308</int>
                              </entry>
                              <entry>
                                <string>Dead, died, will die, guess I better pack my stuff up and start the job hunt...</string>
                                <int>561</int>
                              </entry>
                              <entry>
                                <string>Oh shut up you prude.</string>
                                <int>82</int>
                              </entry>
                              <entry>
                                <string>Trolololololol??</string>
                                <int>63</int>
                              </entry>
                              <entry>
                                <string>Niggers are the greatest enigma of planet earth.</string>
                                <int>200</int>
                              </entry>
                              <entry>
                                <string>nigger</string>
                                <int>239</int>
                              </entry>
                              <entry>
                                <string>&amp;gt- i.imgur.com/V4xESNu.gif  You, sir, are a faggot and a retard.</string>
                                <int>251</int>
                              </entry>
                              <entry>
                                <string>take me 2 ur leaduhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhollllllllllllllllllllllllll</string>
                                <int>281</int>
                              </entry>
                              <entry>
                                <string>BABOU!</string>
                                <int>243</int>
                              </entry>
                              <entry>
                                <string>Dude you completely misspelled it. It&apos;s spelled  DIIIEEEEYAAAAAHHHEEEEEEEIIIIIIAHAHAHAAAOOOAYAYAHAHAYAYEIEIEIEEEEEEEEE**</string>
                                <int>26</int>
                              </entry>
                              <entry>
                                <string>wimp or karma whore?</string>
                                <int>75</int>
                              </entry>
                              <entry>
                                <string>What did cookie monster ever do to you!?!</string>
                                <int>471</int>
                              </entry>
                              <entry>
                                <string>I don&apos;t necessarily think that the Liberals walk the walk as much as people think on this front.  That&apos;s why they didn&apos;t get my primary vote in the House, and my first Senate preference went to the LDP.  By &quot;smaller government&quot;, I mean a smaller scope, less regulation, and lower taxing and spending.  I believe that all of these contribute to prosperity, as individuals are given more power to decide how best to use their own resources.  There is an entire school of economic thoughtâ&#x80;&#x94;Austrianâ&#x80;&#x94;that subscribes to this view.  It isn&apos;t just about efficiency in running things.  It&apos;s about choosing to build those things in the first place.  I believe that if a strong business case can&apos;t be made for something, then it means it wouldn&apos;t provide enough value to people to warrant being undertaken.  Projects decreed by government fiat circumvent this market test.</string>
                                <int>575</int>
                              </entry>
                              <entry>
                                <string>NOPE.jpg  NOPE.avi  NOPE NOPE NOPE</string>
                                <int>135</int>
                              </entry>
                              <entry>
                                <string>IF MY CAT DID THAT, IT WOULD BE HIS LAST DAY ON EARTH</string>
                                <int>216</int>
                              </entry>
                              <entry>
                                <string>&amp;gt; - JUST S,TARTED DATING NEW GUY &amp;gt; - BE,TTER LE.T HIM KNOW YOU AREN&apos;T PREGNANT *^^These ^^captions ^^aren&apos;t ^^guaranteed ^^to ^^be ^^correct*</string>
                                <int>166</int>
                              </entry>
                              <entry>
                                <string>What exactly does my age have to do with facts? The people who do this are far more employable than you, me, or any average schmuck off the street. The tiger chick &quot;katzen&quot; charges $200+ an hour for her tattoo work, when&apos;s the last time *you* made $200 an hour? Just because you&apos;re a dumb cunt with a shitty opinion of people who&apos;ve done far more than you have, doesn&apos;t make you right. Fuckin retard, grow up.</string>
                                <int>176</int>
                              </entry>
                              <entry>
                                <string>You weren&apos;t &quot;friendzoned&quot;, you just ARE FRIENDS. Besides, you said - in front of her - that you don&apos;t want to date her, so I don&apos;t know what you expect? You: I don&apos;t want to date her. Her: Want to go on a date with me?   If you would like to date her, ask her out. It&apos;s really that simple.</string>
                                <int>415</int>
                              </entry>
                              <entry>
                                <string>I see. OOC, what rotation do you use for 40 durability items like mats?</string>
                                <int>547</int>
                              </entry>
                              <entry>
                                <string>What ratio corresponds to a faster bike?</string>
                                <int>467</int>
                              </entry>
                              <entry>
                                <string>THAT IS NOT FUNNY AT ALL ASSHOLE. </string>
                                <int>141</int>
                              </entry>
                              <entry>
                                <string>That&apos;s a great find. Yeah, you did very well with that one. Nice wheels on the Miata, too:</string>
                                <int>581</int>
                              </entry>
                              <entry>
                                <string>GOB&apos;S PROGRAM  PENUS  PENUS  PENUS</string>
                                <int>136</int>
                              </entry>
                              <entry>
                                <string>um  I dont have one? I was saying you son of a bitch&quot; as in &quot;you talented son of a bitch, it pains me how awesome that is&quot;&quot;</string>
                                <int>32</int>
                              </entry>
                              <entry>
                                <string>I think it means that the contrast between &quot;God speaking by the prophets&quot; and &quot;God speaking by his Son&quot; implies a radical disjunction between the two. Or at least, that God speaking by his Son is qualitatively superior to God speaking by the prophets.</string>
                                <int>403</int>
                              </entry>
                              <entry>
                                <string>I love these guys, but I don&apos;t expect them to make it out of Group stage :(</string>
                                <int>552</int>
                              </entry>
                              <entry>
                                <string>Niggers</string>
                                <int>259</int>
                              </entry>
                              <entry>
                                <string>You made a smart.</string>
                                <int>358</int>
                              </entry>
                              <entry>
                                <string>Skydoesminecraft fans.  DAE BUDDER!?!11!!  I AM NOW BUDDER MAN!!! XD XD </string>
                                <int>262</int>
                              </entry>
                              <entry>
                                <string>Find a friend on the team and have hin train you on stance, stance, more stance and finally stance.  Then you can move onto other basics. Hone your basics and I gurantee you&apos;ll do well</string>
                                <int>365</int>
                              </entry>
                              <entry>
                                <string>Plot twist: OP put it on himself and is shamelessly karmawhoring.</string>
                                <int>535</int>
                              </entry>
                              <entry>
                                <string>So the guy that I watched hack while I was playing by shooting guys in the head one by one through the double doors on Aztec was just &quot;Good&quot;.  Or the guy on TFC that I used to watch run around faster then anyone in the server while shooting everyone in the head was bad?  Nah bruv ur a cunt m8.</string>
                                <int>123</int>
                              </entry>
                              <entry>
                                <string>3 months and 11 days? you&apos;re a cunt </string>
                                <int>151</int>
                              </entry>
                              <entry>
                                <string>Understood! Thank you.</string>
                                <int>388</int>
                              </entry>
                              <entry>
                                <string>I want to play  </string>
                                <int>345</int>
                              </entry>
                              <entry>
                                <string>It is a law in some (read: too many) states. However, the thought of those sham clinics scares and enrages me.</string>
                                <int>309</int>
                              </entry>
                              <entry>
                                <string>Maybe he wants him to have a big dick</string>
                                <int>157</int>
                              </entry>
                              <entry>
                                <string>DAE APPLE LITERALLY HITLER? ANDROID 4 LIEF!</string>
                                <int>179</int>
                              </entry>
                              <entry>
                                <string>and [mint glazed baby carrots](http://www.food.com/recipe/minted-glazed-baby-carrots-506239), obviously!</string>
                                <int>300</int>
                              </entry>
                              <entry>
                                <string>fuck you, cunt. </string>
                                <int>64</int>
                              </entry>
                              <entry>
                                <string>Agreed so much. I think that the further removed these things are, it becomes something &apos;okay&apos; to joke about. And it&apos;s not okay.   I&apos;m older than the average redditor, and I will never forget the all consuming fury I felt (still feel) at the movie Titanic.  How dare Hollywood take this horrible, horrible tragedy to make money out of a love story... They could have made the same damn story and called it something else. Fuck those guys! There were still Titanic survivors alive at that time, how do you think they felt at their trauma turned into a blockbuster?   Same fury at Hitler jokes, and I call them out when I hear them.  Thankfully not often.  Just because the people directly involved are mostly dead now does NOT make it okay to turn their pain and anguish into your goddamn punch line!  How will those people feel when they are old and hear 9/11 jokes, I wonder? </string>
                                <int>284</int>
                              </entry>
                              <entry>
                                <string>Nigger racist troll is a dick</string>
                                <int>122</int>
                              </entry>
                              <entry>
                                <string>stupid fucking nigger, lol</string>
                                <int>102</int>
                              </entry>
                              <entry>
                                <string>HEY TOLEDO I HOPE WHATEVER YOU DO WITH THE $800,000 WE GAVE YOU SUCKS. </string>
                                <int>260</int>
                              </entry>
                              <entry>
                                <string>Spoken from a true nigger. </string>
                                <int>111</int>
                              </entry>
                              <entry>
                                <string>you look like a fat whore. </string>
                                <int>110</int>
                              </entry>
                              <entry>
                                <string>Thank you!</string>
                                <int>294</int>
                              </entry>
                              <entry>
                                <string>I lost it at &quot;Good for you lad.&quot;   This was fun to read! I hope if you go again that you do another.</string>
                                <int>302</int>
                              </entry>
                              <entry>
                                <string>You BASTARD.</string>
                                <int>21</int>
                              </entry>
                              <entry>
                                <string>that&apos;s cool, that&apos;s cool...but no conkers bad fur day</string>
                                <int>506</int>
                              </entry>
                              <entry>
                                <string>Hell, I&apos;ve been doing that to my body for a lot longer. </string>
                                <int>516</int>
                              </entry>
                              <entry>
                                <string>Much better, now this will keep things organized and simple.</string>
                                <int>525</int>
                              </entry>
                              <entry>
                                <string>#IDONTKNOWWHATWEREHASHTAGGINGABOUT   #LOUDNOISES</string>
                                <int>203</int>
                              </entry>
                              <entry>
                                <string>**LE GEM****^LE ^GEM****^^LE ^^GEM****^^^LE ^^^GEM****^^^^LE ^^^^GEM****^^^^^LE ^^^^^GEM****^^^^^^LE ^^^^^^GEM****^^^^^^^LE ^^^^^^^GEM****LE GEM****^LE ^GEM****^^LE ^^GEM****^^^LE ^^^GEM****^^^^LE ^^^^GEM****^^^^^LE ^^^^^GEM****^^^^^^LE ^^^^^^GEM****^^^^^^^LE ^^^^^^^GEM** **LE GEM****^LE ^GEM****^^LE ^^GEM****^^^LE ^^^GEM****^^^^LE ^^^^GEM****^^^^^LE ^^^^^GEM****^^^^^^LE ^^^^^^GEM****^^^^^^^LE ^^^^^^^GEM** **LE GEM****^LE ^GEM****^^LE ^^GEM****^^^LE ^^^GEM****^^^^LE ^^^^GEM****^^^^^LE ^^^^^GEM****^^^^^^LE ^^^^^^GEM****^^^^^^^LE ^^^^^^^GEM** **LE GEM****^LE ^GEM****^^LE ^^GEM****^^^LE ^^^GEM****^^^^LE ^^^^GEM****^^^^^LE ^^^^^GEM****^^^^^^LE ^^^^^^GEM****^^^^^^^LE ^^^^^^^GEM** **LE GEM****^LE ^GEM****^^LE ^^GEM****^^^LE ^^^GEM****^^^^LE ^^^^GEM****^^^^^LE ^^^^^GEM****^^^^^^LE ^^^^^^GEM****^^^^^^^LE ^^^^^^^GEM** **LE GEM****^LE ^GEM****^^LE ^^GEM****^^^LE ^^^GEM****^^^^LE ^^^^GEM****^^^^^LE ^^^^^GEM****^^^^^^LE ^^^^^^GEM****^^^^^^^LE ^^^^^^^GEM** **LE GEM****^LE ^GEM****^^LE ^^GEM****^^^LE ^^^GEM****^^^^LE ^^^^GEM****^^^^^LE ^^^^^GEM****^^^^^^LE ^^^^^^GEM****^^^^^^^LE ^^^^^^^GEM**    **^^^^^^^LE ^^^^^^^GEM****^^^^^^LE ^^^^^^GEM****^^^^^LE ^^^^^GEM****^^^^LE ^^^^GEM****^^^LE ^^^GEM****^^LE ^^GEM****^LE ^GEM****LE GEM****^^^^^^^LE ^^^^^^^GEM****^^^^^^LE ^^^^^^GEM****^^^^^LE ^^^^^GEM****^^^^LE ^^^^GEM****^^^LE ^^^GEM****^^LE ^^GEM****^LE ^GEM****LE GEM****^^^^^^^LE ^^^^^^^GEM****^^^^^^LE ^^^^^^GEM****^^^^^LE ^^^^^GEM****^^^^LE ^^^^GEM****^^^LE ^^^GEM****^^LE ^^GEM****^LE ^GEM****LE GEM****^^^^^^^LE ^^^^^^^GEM****^^^^^^LE ^^^^^^GEM****^^^^^LE ^^^^^GEM****^^^^LE ^^^^GEM****^^^LE ^^^GEM****^^LE ^^GEM****^LE ^GEM****LE GEM****^^^^^^^LE ^^^^^^^GEM****^^^^^^LE ^^^^^^GEM****^^^^^LE ^^^^^GEM****^^^^LE ^^^^GEM****^^^LE ^^^GEM****^^LE ^^GEM****^LE ^GEM****LE GEM****^^^^^^^LE ^^^^^^^GEM****^^^^^^LE ^^^^^^GEM****^^^^^LE ^^^^^GEM****^^^^LE ^^^^GEM****^^^LE ^^^GEM****^^LE ^^GEM****^LE ^GEM****LE GEM****^^^^^^^LE ^^^^^^^GEM****^^^^^^LE ^^^^^^GEM****^^^^^LE ^^^^^GEM****^^^^LE ^^^^GEM****^^^LE ^^^GEM****^^LE ^^GEM****^LE ^GEM****LE GEM**</string>
                                <int>76</int>
                              </entry>
                              <entry>
                                <string>Oh pick me pick me I&apos;ll help</string>
                                <int>412</int>
                              </entry>
                              <entry>
                                <string>downvote me because I&apos;m right</string>
                                <int>119</int>
                              </entry>
                              <entry>
                                <string>&amp;gt; I have NO idea about what I can do. Alternatively, I&apos;m doing&quot; everything I perceive, but I don&apos;t know how. A mystery that can be misinterpretated as concrete reality is emanating sponteanously out of a mystery that can&apos;t be interpreted at all. And, I am that mystery.  You&apos;ve dodged my question.  &amp;gt</string>
                                <int>170</int>
                              </entry>
                              <entry>
                                <string>Only 2 fighters that come to mind when I hear the word &quot;Shook&quot; , Muhammad Ali and Danny Garcia, both for totally different reasons.</string>
                                <int>326</int>
                              </entry>
                              <entry>
                                <string>Needs red tears.</string>
                                <int>344</int>
                              </entry>
                              <entry>
                                <string>If a person does that, it&apos;s a quick way of letting me know they are an idiot. </string>
                                <int>558</int>
                              </entry>
                              <entry>
                                <string>YOUR TIGER IS HAVING A BAD TRIP 24/7. I DON&apos;T THINK YOU CAN TALK ABOUT WHAT A LAME ASS MASCOT WE HAVE, WHEN YOUR&apos;S IS JUST AS LIKELY TO TEAR OUT HIS FINGER NAILS AS TO LEAD YOU IN A ROUSING RENDITION OF THE TIGER RAG? </string>
                                <int>85</int>
                              </entry>
                              <entry>
                                <string>I&apos;D SAY THAT IS A PRETTY MAJOR SCORE - A MAJOR WIN - FOR ALL PARTIES</string>
                                <int>253</int>
                              </entry>
                              <entry>
                                <string>whats in the box</string>
                                <int>347</int>
                              </entry>
                              <entry>
                                <string>http://i.imgur.com/mfzjSBe.gif</string>
                                <int>421</int>
                              </entry>
                              <entry>
                                <string>OMG THE AWWWWWWWWWWWWWWWWWWWWWWS</string>
                                <int>130</int>
                              </entry>
                              <entry>
                                <string>Congratulations you have become the worlds biggest faggot OP to claim your prize follow the link http://imgur.com/kAmDTqz</string>
                                <int>27</int>
                              </entry>
                              <entry>
                                <string>Fuck well there goes my attempt :P i started after him</string>
                                <int>224</int>
                              </entry>
                              <entry>
                                <string>Why don&apos;t you go back to your home on whore island</string>
                                <int>497</int>
                              </entry>
                              <entry>
                                <string>Commence upvote? Sorry, all I can do is commence downvote. Fuck you, OP.</string>
                                <int>263</int>
                              </entry>
                              <entry>
                                <string>do you happen to have a gold or secret rare thunder king?</string>
                                <int>519</int>
                              </entry>
                              <entry>
                                <string>I&apos;d put money on it not being a body. Any takers? 10$?</string>
                                <int>511</int>
                              </entry>
                              <entry>
                                <string>HOW TO MAKE CHEAP AND EASY KARMA:  Step 1: GIFify a popular front page youtube vid post. Step 2: Post it.</string>
                                <int>8</int>
                              </entry>
                              <entry>
                                <string>A good lion face always makes me smile</string>
                                <int>454</int>
                              </entry>
                              <entry>
                                <string>OMG ALMOST10 WRECKS?  AND YOUR SOME KIND OF CAREER DRIVER?  WOW.  no.  move over. </string>
                                <int>274</int>
                              </entry>
                              <entry>
                                <string>I honestly disagree, I think around 15/16 a good majority of teens are mature enough to be considered adults.</string>
                                <int>306</int>
                              </entry>
                              <entry>
                                <string>Oh damn---well, look, you made this lady-boner, so it&apos;s only fair that now you come take care of it...  kinda odd saying this to a stranger, but your cock is gorgeous,  not veiny and lumpy, but straight, fat, glossy, perfect cockhead. Aesthetically pleasing to the eye, physically pleasing everywhere else,  I just know. =p</string>
                                <int>430</int>
                              </entry>
                              <entry>
                                <string>I think it would be better if he cried stony-faced, than his weird facial scrunch.</string>
                                <int>567</int>
                              </entry>
                              <entry>
                                <string>No . . . the entire world revolves around ALLAH.</string>
                                <int>488</int>
                              </entry>
                              <entry>
                                <string>5k budget = COCAINE.  10k budget = MORE COCAINE.</string>
                                <int>197</int>
                              </entry>
                              <entry>
                                <string>This doesn&apos;t technically pertain to the right criteria, however, if you laugh while you are criticizing me or indirectly insulting me, fuck you. My mom always does this and it hurts my feelings really bad. She always scoffs when she is calling me lazy and shit. No matter how lighthearted she tries to make vindictive bullshit sound, the scoff just hits me hard. </string>
                                <int>152</int>
                              </entry>
                              <entry>
                                <string>Socrates, Zidane, Baggio? Maybe though op asked for best players ever.</string>
                                <int>544</int>
                              </entry>
                              <entry>
                                <string>FUCK FUCKIN FUCK FUCK FUCKING FUCK</string>
                                <int>138</int>
                              </entry>
                              <entry>
                                <string>HEY TITS LETS SEE WHICH QB CAN OVERTHROW HIS RECEIVER WORSE</string>
                                <int>235</int>
                              </entry>
                              <entry>
                                <string>I&apos;d enslave marry for sure.</string>
                                <int>406</int>
                              </entry>
                              <entry>
                                <string>I didn&apos;t know until now.</string>
                                <int>395</int>
                              </entry>
                              <entry>
                                <string>From another of my comments:   &amp;gt-&quot;blogspam&quot; is &quot;an article which copies a majority of its text from another article, with or without attribution, and usually with very little added commentary.&quot; The other sources you mention are not usually that way, in the same fashion that articles from the CATO Institute are rarely (in their case, never) blogspam.   Note that this differentiates from &quot;spam.&quot;   Also note that the liberal site Upworthy was banned from /r/politics soon after it took off because of the same blogspam issue.</string>
                                <int>509</int>
                              </entry>
                              <entry>
                                <string>THEY&apos;LL GET BACK TO IT SOON I&apos;M SURE, DON&apos;T WORRY. DIDN&apos;T YOU HEAR WE ALL WORSHIP SATAN IN STATE COLLEGE?  ALSO, YOUR LACK OF FLAIR CONFUSES ME. FLAIR UP, BROTHER.</string>
                                <int>67</int>
                              </entry>
                              <entry>
                                <string>But hey, he was a fucking ventriloquist. That&apos;s a talent. Binkie is just some bullshit ass nigger ass whore that bullies everyone.  </string>
                                <int>36</int>
                              </entry>
                              <entry>
                                <string>while masturbating on the subway</string>
                                <int>429</int>
                              </entry>
                              <entry>
                                <string>I found out his first name is Richard.  Dick Riddick. That name has two Dick&quot;s.   it&apos;s a re-dick.  ^sorry&quot;</string>
                                <int>12</int>
                              </entry>
                              <entry>
                                <string>My fingerprints? lol</string>
                                <int>376</int>
                              </entry>
                              <entry>
                                <string>What if I told you that this Rick repelled tigers?</string>
                                <int>498</int>
                              </entry>
                              <entry>
                                <string>sure if you still have one.</string>
                                <int>407</int>
                              </entry>
                              <entry>
                                <string>ALRIGHT YOU FUCKING COCK SUCKING TRY-HARDS, WE&apos;VE GOT SOME NEW RULES AROUND HERE. THE PEOPLE UPSTAIRS HAVE DECIDED THAT WE&apos;VE KICKED SO MUCH ASS WITH REG GUNS OVER THE YEARS, IT&apos;S TIME TO FUCK PEOPLE UP IN STYLE! WE DON&apos;T GIVE A SHIT ABOUT HOW GOOD YOU ARE WITH A RED DOT M16 ANYMORE, THAT SHIT IS LAMER THAN YOUR DADS WILLY ON THE NIGHT HE MADE YOU SORRY CUM STAINS!    THIS IS THE NEW WEAPON OF CHOICE, THE CHEYTAC INTERVENTION. THIRTY ONE MOTHER FUCKING POUNDS OF WHOOP ASS! YES THAT&apos;S RIGHT, THIRTY ONE POUNDS! IF YOU FAT ASSES CAN LUG AROUND YOUR BEER GUTS ALL DAY, YOU SHOULD BE ABLE TO CARRY THIS BEAUTY INTO WAR! BUT I DON&apos;T JUST WANT YOU TO CARRY IT, I WANT YOU TO LOVE IT! THIS RIFLE IS YOUR LIFE! THIS RIFLE IS THE KEY TO GETTING THE 1337 KILLS THAT WILL MAKE YOU A YOUTUBE LEGEND! THIS IS HOW YOU CAN GET THAT MACHINIMA CONTRACT THAT YOU HAD WET DREAMS ABOUT WHEN YOU WERE 12 AND STILL THOUGH PUSSY WAS GROSS, HELL MOST OF YOU PROBABLY STILL DO!   NOW THAT YOU&apos;VE BECOME ACQUAINTED WITH YOUR NEW GIRLFRIEND, IT&apos;S TIME TO LEARN HOW TO MAKE LOVE TO HER! BY WHICH, I MEAN KILL FUCKERS! THIS RIFLE IS THE NUMBER ONE CHOICE OF THE WORLDS FINEST QUICKSCOPERS AND TRICK SHOTTERS, WHICH ONE DAY, IF YOU DON&apos;T  HARDSCOPE YOUR WAY INTO THE FUCKING FRENCH ARMY, YOU WILL BE! THIS RIFLE IS FOR AGGRESSIVE SNIPING, AND THE MOST IMPORTANT ABILITY OF ANY SNIPER IN 2013 IS TO 360! NOW JUST IN CASE YOU SORRY SACKS OF SHIT FAILED GEOMETRY, THAT&apos;S A FULL CIRCLE! REMEMBER THAT, A FULL GOD DAMN CIRCLE, IF I SEE ANY OF YOU PULL A 180 OR A 240, I WILL PERSONALLY SHOVE THIS .408 ROUND UP YOUR FAT ASS!  NOW THESE ARE THE RULES OF 360&apos;N  RULE NUMBER 1. YOU START FROM AN ELEVATED POSITION AND JUMP, ALWAYS! DO YOU THINK ANYONE WANTS TO SEE YOU SPIN ON THE GROUND LIKE A FUCKING BALLERINA? YOUR DAD STILL CRIES AT NIGHT FROM THE TIME HE CAUGHT YOU CROSS DRESSING IN YOUR SISTERS PANTIES WHEN YOU WERE 8! DON&apos;T MAKE HIM GET A DAMN VASECTOMY!   RULE NUMBER 2: SCOPES ARE FOR FUCKING QUEERS WHO SIT IN BUSHES! THE ONLY USE YOU FAIRIES HAVE FOR THE SCOPE ON THIS RIFLE IS ANAL STIMULATION IN CASE YOU GET LONELY FOR YOUR BOYFRIEND BACK HOME! OTHERWISE, I DON&apos;T WANT YOU TO EVEN THINK ABOUT USING IT! IT&apos;S **NO SCOPE OR GET THE FUCK OUT** OF MY CORP!   RULE NUMBER THREE: THAT&apos;S THE NUMBER AFTER TWO, BY THE WAY! IF I EVER, *EVER*, CATCH YOU USING STEADY AIM, I WILL FUCKING DE-RANK YOUR ASS BACK TO PRIVATE! SO IF YOU WANNA KEEP THOSE PURDY BADGES ON YOUR UNIFORM, AND ALL THE SICK NASTY CAMOs AVAILABLE FOR YOUR NEW GIRLFRIEND, YOU WON&apos;T BE A GOD DAMN NANCY AND USE THAT FUCKING PERK!    </string>
                                <int>99</int>
                              </entry>
                              <entry>
                                <string>It&apos;s quite similar to bird law, simple stuff if you know who to ask. </string>
                                <int>541</int>
                              </entry>
                              <entry>
                                <string>DO IT, OR I&apos;LL POUR IT ON THE FLOOR AND BLAME IT ON YOU!</string>
                                <int>229</int>
                              </entry>
                              <entry>
                                <string>How&apos;s that cultural relativism working out for you?   </string>
                                <int>513</int>
                              </entry>
                              <entry>
                                <string>There is always the 360.</string>
                                <int>396</int>
                              </entry>
                              <entry>
                                <string>Still not sure Watt exactly is going on here...</string>
                                <int>486</int>
                              </entry>
                              <entry>
                                <string>Nothing I&apos;ve seen has beaten [Yu-Gi-Oh! Zexal](http://www.google.com/imgres?imgurl=http://images4.wikia.nocookie.net/__cb20120905022614/yugiohenespanol/es/images/6/6b/Yugioh_zexal_20.jpg&amp;amp-imgrefurl=http://es.yugioh.wikia.com/wiki/Yu-Gi-Oh!_ZEXAL&amp;amp-h=1024&amp;amp-w=1280&amp;amp-sz=647&amp;amp-tbnid=yEH0HX_T3Gue5M:&amp;amp-tbnh=90&amp;amp-tbnw=113&amp;amp-zoom=1&amp;amp-usg=__4aDn5aE2DONiuhK3LiFUbr2NIM0=&amp;amp-docid=PoM-SKH4e_nbyM&amp;amp-sa=X&amp;amp-ei=TA0xUsndEYLvqwGfhYGQDQ&amp;amp-ved=0CGkQ9QEwCA&amp;amp-dur=4967) yet.</string>
                                <int>489</int>
                              </entry>
                              <entry>
                                <string>you... BASTARD.</string>
                                <int>52</int>
                              </entry>
                              <entry>
                                <string>Small World. I remember you!</string>
                                <int>409</int>
                              </entry>
                              <entry>
                                <string>i feel like i agree with every break up reason except for cheating. if she was a bitch, or mean, or crazy, or whatever just say things didnt work out. otherwise you look bad when you just trash talk an ex. besides everything is opinion based and theres always 2 sides to a story, so it didnt work out is usually the best way to go.  but with cheating its such a clear cut factual thing that happened, and its easy to just say she cheated, its over.</string>
                                <int>189</int>
                              </entry>
                              <entry>
                                <string>Wenger... Wenger... Wenger... WENGEEEEEEEEERRRRRRRR!!!!  What?  DANGER ZONE!!!</string>
                                <int>269</int>
                              </entry>
                              <entry>
                                <string>Bastard.</string>
                                <int>273</int>
                              </entry>
                              <entry>
                                <string>Awww :(  Keep in mind that maybe you&apos;re messaging people who aren&apos;t subscribers.  So they can&apos;t even see who messaged them and maybe they don&apos;t want to pay to read it!</string>
                                <int>356</int>
                              </entry>
                              <entry>
                                <string>Where Whores Go</string>
                                <int>54</int>
                              </entry>
                              <entry>
                                <string>Upvote for vascularity...and that huge dick. </string>
                                <int>190</int>
                              </entry>
                              <entry>
                                <string>GUYS THESE TIPS ARE COMPLETELY LEGIT  AFTER MY 3RD SEXUAL HARASSMENT CONVICTION I FINALLY SCORED WITH A DRUNK SLUT USING THIS ADVICE  THANK YOU ASKMEN.COM!</string>
                                <int>58</int>
                              </entry>
                              <entry>
                                <string>you dont need a credit card, you&apos;ve already fucked up once.</string>
                                <int>234</int>
                              </entry>
                              <entry>
                                <string>TYPICAL SYDNEY, THROWS MILLIONS AT A PLAYER AND DOESN&apos;T CARE TO LEARN HOW TO SPELL HIS NAME.</string>
                                <int>288</int>
                              </entry>
                              <entry>
                                <string>Nice to see you -)</string>
                                <int>361</int>
                              </entry>
                              <entry>
                                <string>529</string>
                                <int>417</int>
                              </entry>
                              <entry>
                                <string>Oh yeah definitely. I used to drink a shitload of black tea, and it was incredibly detrimental and I couldn&apos;t believe the effect it had on me after I stopped. So I watch out for that now. Thanks though really.</string>
                                <int>382</int>
                              </entry>
                              <entry>
                                <string>After seeing [this TED talk about stress](http://www.ted.com/talks/kelly_mcgonigal_how_to_make_stress_your_friend.html?utm_content=awesm-publisher&amp;amp-utm_medium=on.ted.com-static&amp;amp-utm_campaign=&amp;amp-utm_source=t.co&amp;amp-awesm=on.ted.com_UpsideOfStress), everything else I read/hear about stress seems kind of ... secondary.</string>
                                <int>432</int>
                              </entry>
                              <entry>
                                <string>LIKE FUCK HUMANS GUYZ</string>
                                <int>78</int>
                              </entry>
                              <entry>
                                <string>Classic American behavior, actually.</string>
                                <int>444</int>
                              </entry>
                              <entry>
                                <string>Uh that would stop all human progress.... forever.</string>
                                <int>499</int>
                              </entry>
                              <entry>
                                <string>WWWHHHHEEEEAAAATTTTTOOONNN!!! ...and felicia day!!!!!!</string>
                                <int>222</int>
                              </entry>
                              <entry>
                                <string>The spice must flow.</string>
                                <int>378</int>
                              </entry>
                              <entry>
                                <string>Agree</string>
                                <int>495</int>
                              </entry>
                              <entry>
                                <string>Cheetos in Cherry Garcia, I got called &quot;the holy spirit of snacking&quot; for it.</string>
                                <int>565</int>
                              </entry>
                              <entry>
                                <string>Good ol Anal cunt!</string>
                                <int>68</int>
                              </entry>
                              <entry>
                                <string>WHAT AN INTERCEPTION!!!   DERRON SMITH!!!!   MY MAN!</string>
                                <int>213</int>
                              </entry>
                              <entry>
                                <string>NIGGERS</string>
                                <int>258</int>
                              </entry>
                              <entry>
                                <string>Shut up bitch ass slut</string>
                                <int>86</int>
                              </entry>
                              <entry>
                                <string>TIME TO SCORE, SOXBROS! </string>
                                <int>94</int>
                              </entry>
                              <entry>
                                <string>BIRD!</string>
                                <int>494</int>
                              </entry>
                              <entry>
                                <string>KEEP THEM COMING CHEMISTRY PONY!</string>
                                <int>128</int>
                              </entry>
                              <entry>
                                <string>No, but your cunt does. </string>
                                <int>95</int>
                              </entry>
                              <entry>
                                <string>SPECULATED PORNOGRAPHY OF CHOICE: Pornbot films. </string>
                                <int>217</int>
                              </entry>
                              <entry>
                                <string>Bots crawl this subreddit for steam keys. Instantly steal them before humans can. Clever, yet fucked up.</string>
                                <int>301</int>
                              </entry>
                              <entry>
                                <string>No. Because I desire to fuck attractive female humans. </string>
                                <int>228</int>
                              </entry>
                              <entry>
                                <string>Do it!</string>
                                <int>523</int>
                              </entry>
                              <entry>
                                <string>ACC! ACC! Acc! acc. acc... acc?</string>
                                <int>125</int>
                              </entry>
                              <entry>
                                <string>That&apos;s enough for me. Gotta wait for the BluRay release now.</string>
                                <int>527</int>
                              </entry>
                              <entry>
                                <string>Crochet ALL the hobo mittens! I will knit them. It will be a team effort.</string>
                                <int>549</int>
                              </entry>
                              <entry>
                                <string>Look at the fucking url you idiots. Information Clearing House&quot; is where you get your news? Jesus Christ.&quot;</string>
                                <int>13</int>
                              </entry>
                              <entry>
                                <string>EEEEEXXXXXXPPPPPPPPPLLLLLLLLOOOOOOOOOOOOOOSSSSSIIIIOOOONNNNNNNNNNSSSSSSSSSSS!!!! F**K YEAH!!!!</string>
                                <int>290</int>
                              </entry>
                              <entry>
                                <string>Gus mentions &quot;the lawyer&quot; once or twice.</string>
                                <int>476</int>
                              </entry>
                              <entry>
                                <string>&amp;gt;He never responded but I texted him at least 100 messages over two weeks. Calling him a liar, an alcoholic, a monkey. Calling his new girlfriend a monkey and a slutty rake with a loose vagina. Calling him a traitor. Asking him repeatedly if he was ashamed of himself.  You&apos;re a nut job.  I have a horror story though. My first love when I was 17-19, moved in together when we were 19 in a summer resort where we both worked. Found out he cheated on me with a stripper at his friends 21st, we broke up and I kicked him out but my poor little teenage heart was broken for the first time so I started NC which was hard because we saw each other every day at work. Broke NC when we texted me obsessively (I thought it was cute at the time, dafuq?) and asked for me back. Took him back and he stayed the night, while he was in the shower the next morning he got a phone call from Vanessa&quot; which I answered and apparently they&apos;d been dating ever since he cheated on me with her, including him taking her to my favourite restaurant. After we hung up, she text him saying it was over and I broke up with him(again) which was apparently too much for him as he then proceeded to strangle me to unconsciousness. Quit my job the next day out of shame and moved back into Mums.   2 years later he was sentenced to (mandatory) 15 years for murder when his girlfriend at the time tried to break up with him. Could&apos;ve been me.&quot;</string>
                                <int>167</int>
                              </entry>
                              <entry>
                                <string>DING DING DING DING DING DING DING DING</string>
                                <int>161</int>
                              </entry>
                              <entry>
                                <string>The other girl is cute </string>
                                <int>392</int>
                              </entry>
                              <entry>
                                <string>Denver, but I&apos;d look for a Defense with a better match up. This may not be a low scoring game. </string>
                                <int>589</int>
                              </entry>
                              <entry>
                                <string>I made it all the way through. Mostly to confirm all of it was terrible. At the end they ask, &quot;Do you think you could write a list for us?&quot; Well, not after you set that bar so high.</string>
                                <int>367</int>
                              </entry>
                              <entry>
                                <string>Sigh...niggers</string>
                                <int>41</int>
                              </entry>
                              <entry>
                                <string>that is impressive. </string>
                                <int>377</int>
                              </entry>
                              <entry>
                                <string>sorry for my ignorance, what game is this for?</string>
                                <int>482</int>
                              </entry>
                              <entry>
                                <string>^^^^FUCK ^^^YOU ^^NIGGER ^WIDE EYES BRIGHT CUNT</string>
                                <int>194</int>
                              </entry>
                              <entry>
                                <string>i&apos;ll look at this too while i&apos;m there :3</string>
                                <int>466</int>
                              </entry>
                              <entry>
                                <string>I don&apos;t get the hate of the keyboard. I type at about 60 wpm, and have never had an issue.</string>
                                <int>582</int>
                              </entry>
                              <entry>
                                <string>Remember the last meet up? It was in sf and i guess this one is too because of the picture (golden gate bridge). It&apos;s on September 25. Which is my birthday too :D</string>
                                <int>350</int>
                              </entry>
                              <entry>
                                <string>They&apos;re just innocent spoilers that build hype! Besides, it&apos;s not like you&apos;ll remember all of them when you&apos;re playing! :P</string>
                                <int>316</int>
                              </entry>
                              <entry>
                                <string>he doesn&apos;t need to justify it to the people before it happens... that&apos;s the whole point of a republic, it would be impossible to get anything done if the president had to get popular approval for every action.</string>
                                <int>383</int>
                              </entry>
                              <entry>
                                <string>The Spurs</string>
                                <int>579</int>
                              </entry>
                              <entry>
                                <string>http://i.imgur.com/BLdfAHo.jpg</string>
                                <int>420</int>
                              </entry>
                              <entry>
                                <string>you&apos;re  sister is fucking  insane!</string>
                                <int>146</int>
                              </entry>
                              <entry>
                                <string>But you can keep the cross around your neck. That was never the cause of violence.</string>
                                <int>568</int>
                              </entry>
                              <entry>
                                <string>It has some good episodes, especially in the second season. But it&apos;s also got some really stupid episodes. Very hit or miss IMHO.</string>
                                <int>320</int>
                              </entry>
                              <entry>
                                <string>#NIGGER FAGGOT JEW SHITHEAD. ALL FAGGOTS ARE SINFUL FUCKTARDS WHO SUCK DICK</string>
                                <int>265</int>
                              </entry>
                              <entry>
                                <string>Man I feel you, when I was little I loved Barry Bonds - I mean he was the home run hitting king! But then as I got older I realised it was only because he was a cheating bastard. Really ruined the way I looked at baseball for a while - still my all time favourite sport though!</string>
                                <int>113</int>
                              </entry>
                              <entry>
                                <string>Just know that healthy relationships are out there. Know your boundaries and stick to it. Keep a distance from him and when you do move out you can tell him to go fuck himself. I did this and I still visit with my grandmother with out seeing old man.  You make your own happiness so don&apos;t let this toxic man poison you. </string>
                                <int>134</int>
                              </entry>
                              <entry>
                                <string>I AM AN AMERICAN HEAR ME ROAR WITH IGNORANCE!!! RAAAAAHHHHH!!!!</string>
                                <int>247</int>
                              </entry>
                              <entry>
                                <string>Don&apos;t snatch!  -Vinny, Snatch</string>
                                <int>441</int>
                              </entry>
                              <entry>
                                <string>As I mentioned, I don&apos;t think it&apos;s universal.  Plus, some women&apos;s view of other people as potential sex partners change as they get older and have been in a relationship longer.  You might find yourself viewing others differently in the future, and it might not be that you only view your significant other that way.  </string>
                                <int>426</int>
                              </entry>
                              <entry>
                                <string>Yeah that&apos;s a pretty nice trade!</string>
                                <int>427</int>
                              </entry>
                              <entry>
                                <string>&amp;gt;You have never said a pro sucks dick when they fuck up?  Maybe I have, maybe I haven&apos;t, I am not a professional player so it&apos;s irrelevant.   &amp;gt; It was in the privacy of his own home and he wasn&apos;t trying to start shit. Talk about grasping at straws.  It was not. It was in a gaming house with at least three cameras on Regi, probably up to 8 cameras in the room total. If it was in private none of us would be talking about it because we wouldn&apos;t know. I&apos;m not grasping at straws, Regi talked shit and I can&apos;t wait to see him get slammed at worlds.</string>
                                <int>165</int>
                              </entry>
                              <entry>
                                <string>[Here&apos;s the original post.](http://www.reddit.com/r/whatsthisplant/comments/1lhe5a/this_plant_with_long_thin_leaves_is_growing_near/)</string>
                                <int>323</int>
                              </entry>
                              <entry>
                                <string>#[](/backwardsk)^Aoki K^SanFran </string>
                                <int>428</int>
                              </entry>
                              <entry>
                                <string>They live in a safe place. Swat Valley</string>
                                <int>456</int>
                              </entry>
                              <entry>
                                <string>Call me a shithead or a hipster or whatever but I think it&apos;s cool. I actually bought Carcass&apos; new cassette just to experience it. I remember buying Symphonies of Sickness on tape back in the day. It sounded like shit and it was metal as fuck! I like vinyl an CD&apos;s but i can&apos;t wait to hear this. Also I like giving bands money when they offer something I want. </string>
                                <int>446</int>
                              </entry>
                              <entry>
                                <string>Whore!</string>
                                <int>240</int>
                              </entry>
                              <entry>
                                <string>cunt</string>
                                <int>164</int>
                              </entry>
                              <entry>
                                <string>why does the horse at the top of the page have an antennae, is she ill? wait is it rude to talk about her deformity my bad</string>
                                <int>28</int>
                              </entry>
                              <entry>
                                <string>I DON&apos;T SEE YOU HERE, SCAREDY-CUNT! Ã¢â&#x84;¢Â¥</string>
                                <int>181</int>
                              </entry>
                              <entry>
                                <string>Go suck Cristiano Ronaldo&apos;s dick you sad faggot.</string>
                                <int>201</int>
                              </entry>
                              <entry>
                                <string>Yea i&apos;d probably take it down a bit to get it to a more uniform length.</string>
                                <int>545</int>
                              </entry>
                              <entry>
                                <string>HAH GAYYYYYYYY</string>
                                <int>43</int>
                              </entry>
                              <entry>
                                <string>I feel like the German bootcamp would be more effective -)</string>
                                <int>521</int>
                              </entry>
                              <entry>
                                <string>this is totally gonna happen in humble bundle 10 </string>
                                <int>490</int>
                              </entry>
                              <entry>
                                <string>Ok, that one freaked me the hell out man.</string>
                                <int>470</int>
                              </entry>
                              <entry>
                                <string>I don&apos;t want to.</string>
                                <int>346</int>
                              </entry>
                              <entry>
                                <string>Uncritical support for Israel (which spent a lot of time occupying Lebanon a couple of decades back).  This was actually one of Bin Laden&apos;s chief grievances with the US.</string>
                                <int>357</int>
                              </entry>
                              <entry>
                                <string>Well I think more highly of you. Fuck those sand niggers</string>
                                <int>230</int>
                              </entry>
                              <entry>
                                <string>Miller. I get the feeling SD will be playing from behind and passing a lot. </string>
                                <int>555</int>
                              </entry>
                              <entry>
                                <string>But..popping them feels so good</string>
                                <int>424</int>
                              </entry>
                              <entry>
                                <string>TRAIN BY DAY JOE ROGAN PODCAST BY NIGHT ALL DAY!!!! Talkin shit with eddie ifft. Dan Carlins hardcore history is dope if you wanna do some learning.  Startalk with Neil deGrasse Tyson is awesome </string>
                                <int>73</int>
                              </entry>
                              <entry>
                                <string>THERE&apos;S NO MORE HYPE ANY MORE CAN I HAVE A BETA KEY PLEASE FUCK YOU NO REALLY PLEAES?</string>
                                <int>279</int>
                              </entry>
                              <entry>
                                <string>NO NO NO NO  NO SPORKS TODAY</string>
                                <int>114</int>
                              </entry>
                              <entry>
                                <string>plant something, like a tree. </string>
                                <int>419</int>
                              </entry>
                              <entry>
                                <string>^(S^u)^(p)e^(r^s)^(c)r^(i^p)^(t) i^s ^(^f)^(u)n</string>
                                <int>193</int>
                              </entry>
                              <entry>
                                <string>ARGOfuckyourself&quot;&quot;</string>
                                <int>88</int>
                              </entry>
                              <entry>
                                <string>EvErYoNe NeEdS tO cAlM tHe FuCk DoWn.</string>
                                <int>155</int>
                              </entry>
                              <entry>
                                <string>So nice to come home from work and have dinner waiting for you...</string>
                                <int>537</int>
                              </entry>
                              <entry>
                                <string>Looks like he&apos;s wearing 33?</string>
                                <int>405</int>
                              </entry>
                              <entry>
                                <string>SUPER THUNDER POUT **ACTIVATE!**</string>
                                <int>129</int>
                              </entry>
                              <entry>
                                <string>Rayman will always be the best helicopter animal in my book </string>
                                <int>526</int>
                              </entry>
                              <entry>
                                <string>YOU SHUT YOUR WHORE MOUTH!</string>
                                <int>103</int>
                              </entry>
                              <entry>
                                <string>Because when all else fails.... Crowbar that fucker. </string>
                                <int>220</int>
                              </entry>
                              <entry>
                                <string>OHHHHHH MY GOD WHAT THE FUUUCK! YOU STUPID CUNTS FAIL AT THESE HEISTS. 420YOLOSWAG!</string>
                                <int>275</int>
                              </entry>
                              <entry>
                                <string>Just apply the cream directly to the anus for 5-6 days to help with the inflammation.</string>
                                <int>573</int>
                              </entry>
                              <entry>
                                <string>If your penis, or a penis that you know, is suffering from depression, don&apos;t just walk away. have it seek treatment.</string>
                                <int>17</int>
                              </entry>
                              <entry>
                                <string>But he&apos;s still a cunt. And good at being a cunt. </string>
                                <int>204</int>
                              </entry>
                              <entry>
                                <string>is there a subreddit for this kinda of awesomeness?</string>
                                <int>502</int>
                              </entry>
                              <entry>
                                <string>DIDN&apos;T THEY DO SOMETHING TO EARN OUR HATE WITH SEC AND DOLLAR SIGNS OR SOMETHING? I THOUGHT WE HATED RICE FOR THAT. </string>
                                <int>15</int>
                              </entry>
                              <entry>
                                <string>Wally West got screwed pretty hard.</string>
                                <int>147</int>
                              </entry>
                              <entry>
                                <string>Give me a source or shut your dirty whore mouth.</string>
                                <int>199</int>
                              </entry>
                              <entry>
                                <string>Can I get more info?  Got a $300 infraction and broke as fuck to pay it. </string>
                                <int>550</int>
                              </entry>
                              <entry>
                                <string>Heh, ur stoopid.</string>
                                <int>65</int>
                              </entry>
                              <entry>
                                <string>Yeah, I want something hanging off the bottom of my my phone /s. </string>
                                <int>536</int>
                              </entry>
                              <entry>
                                <string>&apos;straya cunt</string>
                                <int>18</int>
                              </entry>
                              <entry>
                                <string>Yes. Though I wouldn&apos;t be in favor of like cameras in beds, however removal of it from TVs, ads, public support and the like would be ideal.</string>
                                <int>331</int>
                              </entry>
                              <entry>
                                <string>YOU SHUT YOUR WHORE MOUTH.</string>
                                <int>104</int>
                              </entry>
                              <entry>
                                <string>Thanks my nigger. We really out here. Shut up ^x15^strawberry Milk</string>
                                <int>250</int>
                              </entry>
                              <entry>
                                <string>Until the leaks come out. I am going to use all willpower to avoid watching streams</string>
                                <int>570</int>
                              </entry>
                              <entry>
                                <string>sand niggers</string>
                                <int>23</int>
                              </entry>
                              <entry>
                                <string>You aren&apos;t alone. </string>
                                <int>362</int>
                              </entry>
                              <entry>
                                <string>lololo!! LOL!!!!! i bet!</string>
                                <int>93</int>
                              </entry>
                              <entry>
                                <string>Raven needs to give this clowns lessons on how to play a psychic</string>
                                <int>248</int>
                              </entry>
                              <entry>
                                <string>He may not a license either.</string>
                                <int>411</int>
                              </entry>
                              <entry>
                                <string>Yeah pops really isn&apos;t cynical at all. I was just fuckin around with words</string>
                                <int>551</int>
                              </entry>
                              <entry>
                                <string>Yeaaaaahhh!!! Magnets, bitch!</string>
                                <int>120</int>
                              </entry>
                              <entry>
                                <string>FROLIC SLUT</string>
                                <int>11</int>
                              </entry>
                              <entry>
                                <string>Do not get Lancer unless it&apos;s the evo</string>
                                <int>450</int>
                              </entry>
                              <entry>
                                <string>Yeah. Cunt. </string>
                                <int>20</int>
                              </entry>
                              <entry>
                                <string>I had to take mine out from my Sabertooth, those 40 mm&apos;s were the only noise-makers in my entire case- they even went through the sound-dampening from my Corsair 550D.</string>
                                <int>352</int>
                              </entry>
                              <entry>
                                <string>ALL MALE ATTRACTION IS RAPE!  *ALL MALE ATTRACTION IS RAPE!*  **ALL MALE ATTRACTION IS RAPE**  ***ALL. MALE. ATTRACTION. IS. RAPE!***  Kaka khan.</string>
                                <int>44</int>
                              </entry>
                              <entry>
                                <string>and you get a downvote from me nigger.</string>
                                <int>159</int>
                              </entry>
                              <entry>
                                <string>Good ole S&apos;Port</string>
                                <int>338</int>
                              </entry>
                              <entry>
                                <string>There&apos;s an MMO in beta stages right now called Wildstar. The company plasters your username all over the whole entire screen yet the game is still playable. Also, Blizzard has a watermark system that whenever you take a screenshot (using their screenshot software) your name is watermarked yet it&apos;s invisible. It&apos;s some weird stuff...</string>
                                <int>435</int>
                              </entry>
                              <entry>
                                <string>Because your an asshole, and you don&apos;t like dogs.  Also, whats wrong with you.</string>
                                <int>271</int>
                              </entry>
                              <entry>
                                <string>The cross-hatching behind Washington is wrong. It stood out to me immediately.</string>
                                <int>559</int>
                              </entry>
                              <entry>
                                <string>Just looking at it I&apos;m baked...</string>
                                <int>423</int>
                              </entry>
                              <entry>
                                <string>Could you give me the original of this so i can use it? Cuz I love this wallpaper.</string>
                                <int>566</int>
                              </entry>
                              <entry>
                                <string>OMG ANIMAL MASKS HOTLINE MIAMI INVENTED THAT GET SUED BRO</string>
                                <int>231</int>
                              </entry>
                              <entry>
                                <string>yup. leave it to all you fat fucks in fitness to upvote yet another pointless post. youre all ugly as fuck niggers stop contributing the to cancer pls</string>
                                <int>56</int>
                              </entry>
                              <entry>
                                <string>I took on above an [overpass](http://i.imgur.com/zKRWt5O.jpg), there was at least 3 other cars stopped just for taking the photo there too.</string>
                                <int>328</int>
                              </entry>
                              <entry>
                                <string>SHUT UP FOREVER AND STOP RUINING EZRIA.</string>
                                <int>160</int>
                              </entry>
                              <entry>
                                <string>&amp;gt-Here&apos;s my question, after all this typing: should I be mad at our mutual friends? Should I cause a scene about this? Should I even be friends with these people if they are friends of the rapist? Some of them are like, &quot;yeah, I&apos;m trying to remain impartial because I don&apos;t really know what happened,&quot; but others are like, &quot;she&apos;s lying.&quot;  No, don&apos;t remain friends with them. Don&apos;t go to their parties, don&apos;t remain their friends on facebook, cut them out completely. People like this are not people you want in your life, even casually.   Report the sexual assault to the real police. More often than not it seems that the role of campus police is to cover up the fact that any crime took place so that the university&apos;s reputation isn&apos;t damaged.</string>
                                <int>554</int>
                              </entry>
                              <entry>
                                <string>You... Bastard...  You guys get all the hot sorority girls! Give us the bicurious ones at least!</string>
                                <int>292</int>
                              </entry>
                              <entry>
                                <string>Waffle? Don&apos;t you mean carrots? HAHAHAHA</string>
                                <int>465</int>
                              </entry>
                              <entry>
                                <string>YOUNG N GETTIN IT YOUNG N GETTIN IT YOUNG N GETTIN IT YOUNG N GETTIN IT YOUNG N GETTIN IT YOUNG N GETTIN IT YOUNG N GETTIN IT </string>
                                <int>29</int>
                              </entry>
                              <entry>
                                <string>Cunt</string>
                                <int>163</int>
                              </entry>
                              <entry>
                                <string>He&apos;s a judas cunt and a glorious piece of shit.</string>
                                <int>196</int>
                              </entry>
                              <entry>
                                <string>Perhaps</string>
                                <int>542</int>
                              </entry>
                              <entry>
                                <string>I&apos;d kill in defense of myself. I&apos;m thinking that&apos;s more human nature than anything. &quot;Flight or fight&quot;. Are there any other religions that have that type of clause?</string>
                                <int>355</int>
                              </entry>
                              <entry>
                                <string>But really I haven&apos;t seen Nod in a couple weeks at least.</string>
                                <int>518</int>
                              </entry>
                              <entry>
                                <string>KILL BEFORE WE LOOK WEAK!  KILL NOW!!</string>
                                <int>156</int>
                              </entry>
                              <entry>
                                <string>Sure, you could make the argument that we wouldn&apos;t have gone to Iraq either considering the UN was a big factor in that.  I don&apos;t buy this at all. I mean, the US seemed especially willing to go into Syria before the Russian diplomatic effort to utilize the UN to clear chemical weapons from Syria. US intelligence reports differed from UN reports and President Obama didn&apos;t have a willingness to send US intelligence to the UN so that they would have a say in the US military intervention into Syria.  It&apos;s pretty obvious that the US would have gone into Syria, without any approval from the UN. The diplomatic effort vis-a-vis Russia was introduced in a forum outside of the UN and it&apos;s not as if their security counsel actually diplomatically negotiated anything with the US.  Furthermore, I don&apos;t think that the US Congress would have voted to go into Syria and if they had, it wouldn&apos;t have been a legitimate representation of the American public&apos;s will. If anything the UN could have been used to justify Syrian intervention had the circumstance existed in which their intelligence could have been used as justification for military intervention, like it did in Iraq.  Obviously, &quot;might&quot; is the operative word in the title of this article. I just don&apos;t think its a logical argument. I think it&apos;s just some liberal slander against Dr. Paul, libertarianism and non-interventionism.</string>
                                <int>327</int>
                              </entry>
                              <entry>
                                <string>il dek u in da gabba m8 I swer on me mum ur a propa cunt bruv im the sikkest bloke ul cum acros u shit eating wanka </string>
                                <int>16</int>
                              </entry>
                              <entry>
                                <string>Yeah...      I wouldn&apos;t know, my swimming is to the pool bar normally so...</string>
                                <int>553</int>
                              </entry>
                              <entry>
                                <string>Noah nigger.</string>
                                <int>19</int>
                              </entry>
                              <entry>
                                <string>Very much a repost.</string>
                                <int>370</int>
                              </entry>
                              <entry>
                                <string>tl-dr YOLO</string>
                                <int>1</int>
                              </entry>
                              <entry>
                                <string>I concur</string>
                                <int>563</int>
                              </entry>
                              <entry>
                                <string>Uhhh did you hear what the provincial government of Quebec just proposed? It is called the Quebec Charter of Values and it pretty much is about as ignorant and shocking as what this guy is suggesting, but it is in French.  Turns out we have some pretty crazy wing nuts up in Canada too :(</string>
                                <int>413</int>
                              </entry>
                              <entry>
                                <string>YOU SAID YOUD GET ME A DRINK AND YOU LIIIIIIED!!!</string>
                                <int>205</int>
                              </entry>
                              <entry>
                                <string>why have i NEVER HEARD OF SUCH A BEAUTIFUL THING?  </string>
                                <int>210</int>
                              </entry>
                              <entry>
                                <string>RAJESH KUMAR GIRI A PROFESSIONAL APPLIED MATHEMATICS TEACHER FROM DELHI WILLINGS TO MAKE YOU ALL A WINNER. CALL- +91-8527307169</string>
                                <int>30</int>
                              </entry>
                              <entry>
                                <string>I actually though Limsa Lominsa is the 2nd easiest to navigate (next to gridania). U&apos;ldah is just the worse. I remember starting in that area and I spent about 3 hours trying to navigate to a city quest. </string>
                                <int>381</int>
                              </entry>
                              <entry>
                                <string>&amp;gt- ... it&apos;s clearly in public interests those of the victim that other victims come forward if they exist.  &amp;gt- ... it&apos;s incredibly important to protect the innocent ...  Then the solution is to find a way to persuade victims to come forward in the first place, rather than hoping in vain to fix the public and the news media&apos;s false take.  </string>
                                <int>439</int>
                              </entry>
                              <entry>
                                <string>[Definitely this.](www.youtube.com/watch?v=cs7CW6xDbL8#t=1m0s)</string>
                                <int>529</int>
                              </entry>
                              <entry>
                                <string>Fuck niggers</string>
                                <int>24</int>
                              </entry>
                              <entry>
                                <string>Waaaaait, do you have a urinal at your house?</string>
                                <int>479</int>
                              </entry>
                              <entry>
                                <string>ALL THESE WHISPERQUEEF COMMENTS MAKIN MY PANTSGAT RIGID, NOW YOU PUT UP A PIC OF THIS SWEET SHOTGUN?  ARE U TRYIN TO GET A BITCH RAPED UP IN HERE?</string>
                                <int>45</int>
                              </entry>
                              <entry>
                                <string>I think it&apos;s busy being a daddy-long bitch to consider that thought.</string>
                                <int>254</int>
                              </entry>
                              <entry>
                                <string>YOLO SWAG BITCH</string>
                                <int>50</int>
                              </entry>
                              <entry>
                                <string>&amp;gt; The USA is the only country who demands taxes from their &apos;citizens&apos; that haven&apos;t even been in their country  So tell them you&apos;re unemployed. How the hell are they going to find out your foreign wages? Domestic companies have to report income to the IRS, but do those outside the U.S.? I&apos;d be surprised. If I were, say, a Paris-based company and the IRS came to me saying You need to report income to us&quot;, I&apos;d tell them to fuck off.&quot;</string>
                                <int>168</int>
                              </entry>
                              <entry>
                                <string>Same here! </string>
                                <int>307</int>
                              </entry>
                              <entry>
                                <string>I first read the thing the girl says as &quot;Yes, I win.&quot; My next thought was that it was the boy who should be saying that if she said yes.</string>
                                <int>330</int>
                              </entry>
                              <entry>
                                <string>You might use a switch statement:  switch (race) { case 1: cout &amp;lt-&amp;lt- &quot;You are Sir &quot; &amp;lt-&amp;lt- fname &amp;lt-&amp;lt-  &quot; &quot; &amp;lt-&amp;lt- lname &amp;lt-&amp;lt- &quot; The Elf!&quot; &amp;lt-&amp;lt- endl-  case 2: etc. etc...</string>
                                <int>374</int>
                              </entry>
                              <entry>
                                <string>The frequent use of the word feminist in this article kills me. Feminism supports women&apos;s choices, and if a woman wants to stay at home and raise children, feminists support that life style- in fact, they work for a world in which women (and men) will not be judged or discriminated for making that choice. They are perpetuating such a harmful stereotype saying that &quot;bad&quot; feminism forces &quot;good&quot; women out of the home and into the workplace.</string>
                                <int>478</int>
                              </entry>
                              <entry>
                                <string>Actually, all of the damage was from me fucking your fat whore of a mother on top of it</string>
                                <int>283</int>
                              </entry>
                              <entry>
                                <string>What the fuck? Bayern got their ratings fucked in the ass.</string>
                                <int>233</int>
                              </entry>
                              <entry>
                                <string>It&apos;s supposed to be a solid green light with no blinking whatsoever.  A blinking light indicates a problem, and my first instinct here is that it&apos;s a warranty problem.  Try plugging it into a different power outlet in your house (go outside your room to test).  Try the car adapter that came with the PA v2 as well.  This well help determine exactly where the problem lies, at which step in the chain between the wall and the MFLB.</string>
                                <int>474</int>
                              </entry>
                              <entry>
                                <string>Thank you! :D</string>
                                <int>321</int>
                              </entry>
                              <entry>
                                <string>Oooohhh let&apos;s see shall we?  1. The friend that was supposed to be gay she had sex with. 2. Fucked some guy drunk at a party while I was watching our baby. 3. Left me for my best friend of 7 years...(worst year of my life) 4. got engaged to him after 4 months of dating...jesus 5. Find new girlfriend and peace...ex stirs things up and says I hit her before. (I didn&apos;t)</string>
                                <int>153</int>
                              </entry>
                              <entry>
                                <string>yo estaba en casa porque era el ddia del maestro y no habia clases, y lo vi en las noticias pero no entendi mucho</string>
                                <int>313</int>
                              </entry>
                              <entry>
                                <string>He&apos;s a goofy bastard with a hint of seriousness and don&apos;t give a crap what people think when he does stupid stuff. Which makes me the complete opposite being serious with a bit of goofy, very self conscious, tall and broad shouldered while he is only like 5&apos;8&apos;&apos; and slouches.</string>
                                <int>112</int>
                              </entry>
                              <entry>
                                <string>But not his career...really dropped the ball there. </string>
                                <int>503</int>
                              </entry>
                              <entry>
                                <string>Op is a faggot</string>
                                <int>42</int>
                              </entry>
                              <entry>
                                <string>I WOULD UPVOTE YOU FOR CORRECTNESS, BUT THE BUTTON DOESN&apos;T SEEM TO BE WORKING.  NO IDEA WHY.</string>
                                <int>287</int>
                              </entry>
                              <entry>
                                <string>I think Jesse was too emotionally unstable to start cooking in the first place. He&apos;s just not cut out to be a criminal, and he would like to think he&apos;s a better person than he actually is. He can&apos;t seem to grasp the concept that Walter can kill people and ruin lives with no remorse. When your life is in danger, do you set all your moral values aside and do what&apos;s necessary, or do you die?</string>
                                <int>461</int>
                              </entry>
                              <entry>
                                <string>YO THAT SPENCER GRAPPLE WHILE HE WAS CRUMPLED HRNGGH</string>
                                <int>211</int>
                              </entry>
                              <entry>
                                <string>Rengar.. FUUUUUUUUUUUUUUUUUUUU-</string>
                                <int>126</int>
                              </entry>
                              <entry>
                                <string>Yep. Most LCDs are 60hz, the best are 144hz, but even mediocre CRTs are like 160hz.</string>
                                <int>571</int>
                              </entry>
                              <entry>
                                <string>thanks for your concern for my social status.   I hope your teenage bitch sarcasm wears off one day, bro</string>
                                <int>5</int>
                              </entry>
                              <entry>
                                <string>Fucking niggers</string>
                                <int>53</int>
                              </entry>
                              <entry>
                                <string>You&apos;re a cunt. </string>
                                <int>51</int>
                              </entry>
                              <entry>
                                <string>I find bringing a bottle of disinfecting spray to the toilet weirder then putting a layer of TP on the seat.</string>
                                <int>305</int>
                              </entry>
                              <entry>
                                <string>YOU SHUT YOUR WHORE MOUTH I WANT IT NOW. Jk, in all honesty I&apos;d accept that if it was a guarantee that it would be mind blowing.</string>
                                <int>31</int>
                              </entry>
                              <entry>
                                <string>anyone on xbox wants to play hit me up</string>
                                <int>452</int>
                              </entry>
                              <entry>
                                <string>AFAIK the size/shape of our currency is partly designed for the blind.  The edge ridges on a $1 coin, the small but solid weight of the $2 (not the light flat 5c) and the huge 50c.  Our currency has character and is durable as ****.  If notes are way to mangled its as easy as ironing them under a towel and presto they are brand new. </string>
                                <int>436</int>
                              </entry>
                              <entry>
                                <string>Damn sexy.</string>
                                <int>296</int>
                              </entry>
                              <entry>
                                <string>..........FUUUUCK YOU BIWEEKLY CHECKS!!!   FUCK!!!!   WHY!!!!!!!!   FUUUUCK  I AM GOING TO THROW A BF ALL OVER MY WHOLE HOUSE  FUCK ME</string>
                                <int>37</int>
                              </entry>
                              <entry>
                                <string>NAH NAH FUCK YOU OFWGKTADGAFLSLLBB TRIPLE 6 NIGGA DGAF NIGGA DANNY BROWN IS NOTHING COMPARED TO ANYONE THATS BEEN IN A TYLER SONG #GOLFWANGFORLIFE </string>
                                <int>46</int>
                              </entry>
                              <entry>
                                <string>Seen them live about 7-8 times over the past 4 years. Solid set. I mean if you like pendulum you will most likely hear some of your favorites.  I am still salty about the break up of pendulum though, I have heard it was Rob Swire idea.  Tl-dr: it will be OK. Have fun kid.</string>
                                <int>408</int>
                              </entry>
                              <entry>
                                <string>you shit eating cunt i upvoted you anyway</string>
                                <int>174</int>
                              </entry>
                              <entry>
                                <string>My Jack Russell gets so excited when I come home, she leaps up at me to catch her, and then proceeds to scramble up onto my shoulders and stands there, licking my ear.</string>
                                <int>354</int>
                              </entry>
                              <entry>
                                <string>I can&apos;t chew on stogies... shit o get finnicky when I get too much spit on them...</string>
                                <int>569</int>
                              </entry>
                              <entry>
                                <string>Awww! You&apos;re so funny, you gigantic cunt. </string>
                                <int>177</int>
                              </entry>
                              <entry>
                                <string>Thanks for the math! this helps tremendously</string>
                                <int>475</int>
                              </entry>
                              <entry>
                                <string>I&apos;m sure you&apos;re right. It was probably just a hologram airplane and some cheap special effects.</string>
                                <int>588</int>
                              </entry>
                              <entry>
                                <string>Also in the Time Machine episode why doesn&apos;t Fry get out of the time machine when they get to the year 2000</string>
                                <int>304</int>
                              </entry>
                              <entry>
                                <string>I met Ibra when ah fuck it I&apos;ve never met Ibra  :(</string>
                                <int>500</int>
                              </entry>
                              <entry>
                                <string>they will learn the valuable lesson of nothing at all  [](/furious)</string>
                                <int>539</int>
                              </entry>
                              <entry>
                                <string>I like. any more oldies?</string>
                                <int>397</int>
                              </entry>
                              <entry>
                                <string>Over 40 people + a $500 bonus pot in singles.</string>
                                <int>480</int>
                              </entry>
                              <entry>
                                <string>I meet everything but the income.  Once I&apos;m done with college I should have that rectified. </string>
                                <int>585</int>
                              </entry>
                              <entry>
                                <string>A^A^A^A^A^A^A^A^A^A^A^A^A^A^A^A^A^A^A^A^A^A^A^A</string>
                                <int>195</int>
                              </entry>
                              <entry>
                                <string>Okay, awesome, going to eat this every day. But not today, because the name made me fetch in a way I haven&apos;t since two girls one cup.</string>
                                <int>324</int>
                              </entry>
                              <entry>
                                <string>Made to look like cocaine</string>
                                <int>398</int>
                              </entry>
                              <entry>
                                <string>You&apos;re just a jealous cunt and you&apos;re pathetic. </string>
                                <int>198</int>
                              </entry>
                              <entry>
                                <string>lilstevie wrote:   It&apos;s funny because niggers steal.    You&apos;re all ugly cunts, btw.    -------  **Deleted**</string>
                                <int>9</int>
                              </entry>
                              <entry>
                                <string>You&apos;re really bashing on a ten year old&apos;s name? </string>
                                <int>487</int>
                              </entry>
                              <entry>
                                <string>Eh... I&apos;m in the UK and remembrance Sunday isn&apos;t really a big deal unless you are part of a uniformed organisation. My SJA division pressured me to join a march, but beyond that, no-one gave a shit.</string>
                                <int>375</int>
                              </entry>
                              <entry>
                                <string>I should also mention that at the end of each semester, ask what they do with lost and found. At my school we throw it out because in order to donate it somewhere it needs to be sorted, washed, and driven to said location with an assortment of paperwork. I now own several name brand hoodies (Columbia, Under Armour, Victoria Secret Pink etc) and I found this really awesome Doctor Who water bottle. Boil and sterilize everything and its brand new.   I make decent money, but I will never throw away my money at businesses like these, but free is always good. I&apos;ll also sell to consignment shops like Plato&apos;s Closet and get some cash in hand.   Frugal mother fuckers.</string>
                                <int>252</int>
                              </entry>
                              <entry>
                                <string>He has nice buns.</string>
                                <int>359</int>
                              </entry>
                              <entry>
                                <string>I&apos;M TOO HUNGRY FOR THAT BULLSHIT</string>
                                <int>131</int>
                              </entry>
                              <entry>
                                <string>OH I&apos;M MORE INTERESTING THAN A WET PUSSY CAT</string>
                                <int>184</int>
                              </entry>
                              <entry>
                                <string>COD, halo, mass effect, minecraft, and assassin creed. U?</string>
                                <int>517</int>
                              </entry>
                              <entry>
                                <string>niggers are racist</string>
                                <int>69</int>
                              </entry>
                              <entry>
                                <string>OUR NAME IS THE REDSKINS. I don&apos;t give a shit if you think it&apos;s racist, Bill, you don&apos;t get to decide what we&apos;re referred to just because you&apos;re uncomfortable.  Fuck Grantland and their faux-PC bullshit.   </string>
                                <int>77</int>
                              </entry>
                              <entry>
                                <string>I&apos;ve heard people say they&apos;re more unsure of their opinions after education and maybe this has to do with it. The world is complex and they feel helpless.</string>
                                <int>341</int>
                              </entry>
                              <entry>
                                <string>Walk outside. Right now. Find 10 people. Ask them if they like 3D movies. If you find more than 3 that say yes, I would be surprised. Fuck your bear.</string>
                                <int>47</int>
                              </entry>
                              <entry>
                                <string>#HAPPY MOTHER FUCKIN CAKE DAY NIGGER#</string>
                                <int>154</int>
                              </entry>
                              <entry>
                                <string>This is a brilliant idea Ogel. It will force cities to limit how many factories they have, causing overpowered cities to turn into Industrial Revolution London.</string>
                                <int>348</int>
                              </entry>
                              <entry>
                                <string>probably not</string>
                                <int>314</int>
                              </entry>
                              <entry>
                                <string>The only way in which this story makes sense is if your boyfriend and P are actually fucking each other.</string>
                                <int>6</int>
                              </entry>
                              <entry>
                                <string>kk. Hopefully it works, I suspect it was just the guy, but we&apos;ll try it anyways</string>
                                <int>560</int>
                              </entry>
                              <entry>
                                <string>No.</string>
                                <int>418</int>
                              </entry>
                              <entry>
                                <string>The fluency question is bit different depending on country.  In some places (Asia in particular), it is actually preferred that you don&apos;t know the language.  In others, although English fluency is the primary requirement, it is also kind of expected that you know the local language.  This seems to be the case in many Spanish speaking countries, especially since Spanish is a pretty widely spoken language.  Once again, generally not required per se, but often expected.  I think your level of Spanish would probably be just fine, and you would pick it up quickly once you were there.  Usually the boyfriend is an issue.  If he has a job and can come, great.  If he can find another job, great.  But as for teaching, that can be difficult.  There are some programs that will place couples together on teaching assigments, but a lot won&apos;t do it--they don&apos;t want to risk losing 2 teachers at once if the relationship goes south.  So boyfriend coming along can be complicated.    As to living situations, as the other poster mentioned, it really depends.  If oyu go through a program, yes, they do usually help find you a place to live.  If you just apply to a school directly, you are often on your own.  Depends on which route you take.    Good luck, and I hope it all works out for you!</string>
                                <int>319</int>
                              </entry>
                              <entry>
                                <string>Viva this account, you motherfucker.  You almost have a million karmas.</string>
                                <int>261</int>
                              </entry>
                              <entry>
                                <string>Anything by Wham or George Michael. I love that shit.</string>
                                <int>504</int>
                              </entry>
                              <entry>
                                <string>My only account left in NA is lvl 14 /: I transferred recently to play with friends (and with better ping).</string>
                                <int>303</int>
                              </entry>
                              <entry>
                                <string>wow                                                     flying sauce              alein tok my sister                            wow                               muldre an skulli              wow        wow                        such trauma                                                          many doge probe              i bring u luv</string>
                                <int>143</int>
                              </entry>
                              <entry>
                                <string>OMFG I KIN MAK AL DE KEDZ GIT REKT NW THT I NO HW 2 PEE VEE PEE</string>
                                <int>246</int>
                              </entry>
                              <entry>
                                <string>[](/trixchargin) Oh that is such bull shit. Welcome to the south. Where every one in politics is a religious bigot that wants to keep people from being fucking happy. Denying Gays and Lesbians the right to marry is denying them civil rights and it is BULL SHIT and unconstitutional. Separation of god damn church and state and...and...ARGHH!!!! FUCK THE SOUTH. ^^^I ^^^hate ^^^it ^^^here...</string>
                                <int>162</int>
                              </entry>
                              <entry>
                                <string>HUH. I DIDN&apos;T KNOW HIRDY WAS USING THE MEDIA AS A LEGAL DEFENCE. TIL.</string>
                                <int>255</int>
                              </entry>
                              <entry>
                                <string>So the seed is 13w36b?</string>
                                <int>389</int>
                              </entry>
                              <entry>
                                <string>LIVIN IN A LONELY WOOOOOORLD</string>
                                <int>115</int>
                              </entry>
                              <entry>
                                <string>Now THAT&apos;S a daddy long bitch if I&apos;ve ever seen one. I pray those things aren&apos;t in Australia, we&apos;ve got enough around here we don&apos;t need fucking *baboon* spiders</string>
                                <int>66</int>
                              </entry>
                              <entry>
                                <string>My small company&apos;s servers don&apos;t run antivirus software. My home computers do not. They run Windows Server 2012 and 7, respectively.  Tell me, why should a server have AV software if it is not a file server? Why should every single byte read or written be scanned by a process, effectively making the performance tank and completely negate the performance of any SSDs?  Antivirus software IS NOT a replacement for proper security measures:   - Never run any software on any server that has not been verified.   - Monitor all connections in and out of any servers   - Lock down all processes, ports, services that are not required.   - If possible, explicitly block every IP except the ones needing access.   - Never run any non-verified software.   - The web browser on a server is nothing more than a glorified file download app.  For a file server (and preferably backup storage as well), you need to have antivirus software, regardless of the operating system of the server or any clients accessing it (even if you&apos;re 100% Linux users - you never know when you&apos;ll need to plug a Windows laptop into the network). For any other purpose, antivirus software on a properly managed server is outright detrimental to its intended usage.</string>
                                <int>317</int>
                              </entry>
                              <entry>
                                <string>they are open now... did you add me?</string>
                                <int>445</int>
                              </entry>
                              <entry>
                                <string>Whenever TBM&apos;s advocate for teetotalism I like to throw this quote at them:   &quot;It is a mistake to think that Christians ought all to be tee-totallers- Mohammedanism, not Christianity, is the teetotal religion. Of course it may be the duty of a particular Christian, or of any Christian, at a particular time, to abstain from strong drink, either because he is the sort of man who cannot drink at all without drinking too much, or because he is with people who are inclined to drunkenness and must not encourage them by drinking himself. But *the whole point is that he is abstaining, for a good reason, from something which he does not condemn and which he likes to see other people enjoying.* One of the marks of a certain type of bad man is that he cannot give up a thing himself without wanting every one else to give it up.&quot;  -C.S. Lewis (my italics added for emphasis)</string>
                                <int>576</int>
                              </entry>
                              <entry>
                                <string>Y U NO DOCTOR</string>
                                <int>33</int>
                              </entry>
                              <entry>
                                <string>FUCKING MAGNETS, HOW DO THEY WORK?</string>
                                <int>137</int>
                              </entry>
                              <entry>
                                <string>Very normal. If I want to get my lady in the mood I&apos;ll put in a movie that has one of her favorite male actors in it. I know that she probably thinks about them when we&apos;re getting it on, kinda turns me on to be a proxy for her kinks.    That&apos;s just me. I&apos;m kinda&apos; overly secure about myself though. </string>
                                <int>416</int>
                              </entry>
                              <entry>
                                <string>You are a cunt. An egocentric stupid cunt. You like to imagine 15 year old girls stripping for you? Cuntish behavior. I am a cuntiologist and study many and all cunts. You sir a special kind of cunt- an new species of cunt infact!  The autism levels are skyrocketing. Fuck you op incase you didn&apos;t the the message kill yourself you&apos;re a worthless shitbag.</string>
                                <int>149</int>
                              </entry>
                              <entry>
                                <string>Joique Bell, hands down. Even if he doesn&apos;t get a lot of carries, he gets a lot of passes thrown his way. Especially if you are in a ppr league. He&apos;ll get like 30 yds rushing, 60 yds receiving and maybe a TD. His upside is even higher if Bush goes out. </string>
                                <int>402</int>
                              </entry>
                              <entry>
                                <string>Sorry, Baby Cunt Puncher, you are right. This forum is not the place. I will take my downvotes and potential ban.</string>
                                <int>14</int>
                              </entry>
                              <entry>
                                <string>**GREAT NOW A SINGLE TIES IT. FUCK**</string>
                                <int>150</int>
                              </entry>
                              <entry>
                                <string>Sorry for the late reply, I just got Alice in another trade, so I&apos;ll have to pass on this one.</string>
                                <int>587</int>
                              </entry>
                              <entry>
                                <string>Yep. It hurts pretty fucking bad to get hit in your crotch.</string>
                                <int>237</int>
                              </entry>
                              <entry>
                                <string>I&apos;m a girl lol and putting food in me seems like a bad idea ?</string>
                                <int>528</int>
                              </entry>
                              <entry>
                                <string>Mmmprobably not. *sticks out tongue playfully*</string>
                                <int>481</int>
                              </entry>
                              <entry>
                                <string>I really can&apos;t see this going far. Seems like Fluker took the cash separate from the university, and the NCAA is kind of in a tough spot anyway.</string>
                                <int>335</int>
                              </entry>
                              <entry>
                                <string>Happy dirty thirty!!!</string>
                                <int>385</int>
                              </entry>
                              <entry>
                                <string>Directions unclear, key stuck in head.</string>
                                <int>453</int>
                              </entry>
                              <entry>
                                <string>Thank you, that was the most though I put into writing in a while. :)</string>
                                <int>540</int>
                              </entry>
                              <entry>
                                <string>Yeah, but when does that ever happen? Once every thousand years? The man needs help.</string>
                                <int>572</int>
                              </entry>
                              <entry>
                                <string>Whore</string>
                                <int>207</int>
                              </entry>
                              <entry>
                                <string>You have been banned from /r/TrueChristian</string>
                                <int>473</int>
                              </entry>
                              <entry>
                                <string>ILLEGAL IMMA GO LAWFUL GOOD ON YO ASS  /s</string>
                                <int>173</int>
                              </entry>
                              <entry>
                                <string>There are a lot of asshole redditors who like to downvote for stupid reasons. Maybe someone thought you were being pretentious or something.  I don&apos;t, though; I&apos;m happy to be edified. Allow me to console you with an upvote for your timely knowledge.</string>
                                <int>62</int>
                              </entry>
                              <entry>
                                <string>Really that&apos;s be an effective law IMHO  You rape someone, once your convicted we burn your ass alive. We throw gasoline on you and give you a light and watch you burn.  Hell no what we DIP YOU INTO GASOLINE and then light you on fire. And we ain&apos;t got any water.</string>
                                <int>107</int>
                              </entry>
                              <entry>
                                <string>Talk about not even doing the barest of due diligence.</string>
                                <int>510</int>
                              </entry>
                              <entry>
                                <string>Of course a guy with a fedora is in the group.</string>
                                <int>483</int>
                              </entry>
                              <entry>
                                <string>The &quot;K-ONS BACK NIGGERS&quot; alt text?</string>
                                <int>158</int>
                              </entry>
                              <entry>
                                <string>This is from the 9/11 Memorial Museum near ground zero. I went in 2009, and the atmosphere was very somber. No one said a word, even the kids were quiet. They had a wall set up where you could listen to the emergency response team communications, and other plaques like this one with the voice recording that went with it. Just a glimpse of what those people endured, and the way it was remembered, is very powerful. I felt deeply sad by the time we&apos;d made our way through the whole place...</string>
                                <int>493</int>
                              </entry>
                              <entry>
                                <string>I couldn&apos;t agree more, thankfully the dungeons in this are fun as hell all the way through.</string>
                                <int>584</int>
                              </entry>
                              <entry>
                                <string>could you briefly describe the process of how 1 usd is created?</string>
                                <int>530</int>
                              </entry>
                              <entry>
                                <string>Actually, decades are an arbitrary way to categorize culture. Everything bleeds into everything </string>
                                <int>590</int>
                              </entry>
                              <entry>
                                <string>*Hey, faggot, quit grabbin&apos; my dick yo, hey err&apos;one, this motherfucker is grabbin&apos; on my cock yo! What a fucking faggot!*  ...it&apos;s not hard to imagine someone much less classy handling this in a much less classy way.</string>
                                <int>84</int>
                              </entry>
                              <entry>
                                <string>Thank you for this. I was raised by a single mother, but I spent time with my father a lot as well. If I hadn&apos;t, that would probably hold true for me too. Children require both parents in a healthy marriage to turn out normal and healthy. A child is not a tool that you use to gain society pity points, keep your man, or live vicariously through.</string>
                                <int>440</int>
                              </entry>
                              <entry>
                                <string>Fuck you whats your point? I built my own business and now I get to have some fun. Your no better then some nigger telling me I need to check my privilege.  </string>
                                <int>61</int>
                              </entry>
                              <entry>
                                <string>Please do go somewhere else faggot.</string>
                                <int>145</int>
                              </entry>
                              <entry>
                                <string>[I love experiences!](http://i.imgur.com/OrWal7B.jpg)</string>
                                <int>508</int>
                              </entry>
                              <entry>
                                <string>thanks for an answer instead of a downvote. </string>
                                <int>477</int>
                              </entry>
                              <entry>
                                <string>Would work, but we know him well.</string>
                                <int>433</int>
                              </entry>
                              <entry>
                                <string>HOLY FUCKING SHIT! PUT HER IN GYMNASTICS SEE HIW SHE DOES XD</string>
                                <int>244</int>
                              </entry>
                              <entry>
                                <string>I&apos;m a new fan and Planet of Ice was the album recommended to me first so that was the one I listened to first. Talking about an album as a whole with people about that album specifically is perfectly fine even if the person doesn&apos;t know any other albums by that band. I just wanted to talk about the album I recently listened to by a band I just started getting into. Complaining that I haven&apos;t listened to another album by this band is a little unfair. Ill be sure to pick up Menos el Oso whenever I can though as it will be the next album I&apos;ll listen to by this band. </string>
                                <int>520</int>
                              </entry>
                              <entry>
                                <string>Very interesting. I never even realized the ash statues were able to be spread around the island via smuggling. </string>
                                <int>312</int>
                              </entry>
                              <entry>
                                <string>You shut your whore mouth!</string>
                                <int>105</int>
                              </entry>
                              <entry>
                                <string>Bitch!</string>
                                <int>242</int>
                              </entry>
                              <entry>
                                <string>people do that in real life? dang, I am glad I haven&apos;t been hired at any of those kind of places. everywhere I have worked I have never had to worry about anything like that, and neither did my coworkers.</string>
                                <int>380</int>
                              </entry>
                              <entry>
                                <string>P A R E N T I N G  D O N E  R I G H T!!!</string>
                                <int>171</int>
                              </entry>
                              <entry>
                                <string>Faggot</string>
                                <int>241</int>
                              </entry>
                              <entry>
                                <string>Pants. Pants pants pants pant. Pants pants pants pants. </string>
                                <int>514</int>
                              </entry>
                              <entry>
                                <string>Darker is a little better. I&apos;m a hairstylist, so that&apos;s why I mentioned your hair colour specifically.  I think that due to your prominent nose (nothing&apos;s wrong with it), how you do your bangs are important.  The colour in this pic is good, but the bangs are a little too straight/flat.  Everyone keeps mentioning pic 5. Of course it&apos;s sexy, but the hair also has a little volume and the bangs have volume with a slight bend and go off to the side a little bit, instead of coming straight down.  I think that pic looks the best.  Your glasses are fine.  I just think that you might appear prettier  if you stay away from super straight, blunt cuts and flat hair.  I would go with more razored stylings and try to keep a little wave or volume in your hair.  I&apos;m similar, I love to have pin straight, super razord/jagged edges, but I look best with kind of wavy, sweet &quot;new girl&quot; hair.</string>
                                <int>577</int>
                              </entry>
                              <entry>
                                <string>I would find out why she was seeking attention from another man. I hate to say it, but when a woman emotionally checks out from a relationship, there has to be a reason. Unless she&apos;s a bitch. If she&apos;s a bitch, tell her to hit the bricks. </string>
                                <int>92</int>
                              </entry>
                              <entry>
                                <string>1.  did you fuck her? 2. have you fucked anyone since?</string>
                                <int>225</int>
                              </entry>
                              <entry>
                                <string>THIS MOTHERFUCKER STOLE AN AK AND IS TRYING TO DISGUISE IT. NIGGER THAT AINT A BIKE </string>
                                <int>276</int>
                              </entry>
                              <entry>
                                <string>Don&apos;t forget that tongue!</string>
                                <int>400</int>
                              </entry>
                              <entry>
                                <string>The police here are pretty chill. Our street has a speed limit of 30, my roommate always goes at least 45, and she&apos;s never been pulled over in her life.  Plus, I can&apos;t be bothered to do math in my head.</string>
                                <int>379</int>
                              </entry>
                              <entry>
                                <string>hahahaha HaHaHaHa HAHAHAHA  HA!HA!HA!HA! Down vote for making me laugh so hard</string>
                                <int>270</int>
                              </entry>
                              <entry>
                                <string>This lineup seems to be hitting fine without Harper in it. But today...</string>
                                <int>546</int>
                              </entry>
                              <entry>
                                <string>Then you provide a source that says otherwise. If you bothered to read the article http://en.wikipedia.org/wiki/Marker_degradation it spells out exactly how production is done for test. Not that it would matter since you clearly have no reading comprehension, your only evidence was citing a pheromone called androstenone synthesized from horse urine and claiming it was an AAS.</string>
                                <int>451</int>
                              </entry>
                              <entry>
                                <string>Hahaha. God look at his flair too. What a total cunt. (the troll, not NuKeX)</string>
                                <int>267</int>
                              </entry>
                              <entry>
                                <string>I&apos;ve no experience with it. It probably won&apos;t get much love in this sub-reddit. Check out [/r/zeos](http://www.reddit.com/r/zeos)  Start with a 2.0 or 2.1 system.</string>
                                <int>351</int>
                              </entry>
                              <entry>
                                <string>Does the laptop have a discrete graphics card or just integrated?   Also since it&apos;s a laptop nothing is going to be easy to replace, especially since opening it up voids your warranty. </string>
                                <int>366</int>
                              </entry>
                              <entry>
                                <string>Umm, you&apos;re the fucking asshole.</string>
                                <int>133</int>
                              </entry>
                              <entry>
                                <string>Yikes, this is all wrong. I&apos;d recommend something more like.. 1. Finish your L50 Job Quest. 2. Finish your Story by doing Castrum Meridianum and the Praetorium. 3. Unlock Amdapor Keep and Ifrit Hard. 4. If your weapon is particularly bad (ie. sub-level 40), get the highest level GC weapon you can (42? 44? I forget!) 5. Wanderer&apos;s Palace! Lots of ilvl 55 gear here, including Darklight weapons, and 50 Philosophy stones a run. A speedrun strategy can get this to sub-20 minutes even with fresh 50 groups. 6. Once you&apos;re comfortable, powerbomb Ifrit Hard for a weapon. 7. Unlock Garuda Hard from this while you&apos;re at it. 8. Amdapor Keep! There are a lot of words already said about this place - just keep your head together and keep at it. It gets easier as you gear up. 9. Progress your Relic Quest up to HM Primals once you&apos;re mostly fully geared from Amdapor. Hydra don&apos;t play around, not to mention Garuda and Titan. 10. Garuda Hard. Yay! 11. Titan Hard. Also yay. 12. Relic! Coil! More yay! Honestly if you can beat Titan you don&apos;t even need this kind of advice anymore, what are you reading this for.</string>
                                <int>310</int>
                              </entry>
                              <entry>
                                <string>I hate those, they make me feel awkward and uncomfortable and I never know what&apos;s going on.</string>
                                <int>583</int>
                              </entry>
                              <entry>
                                <string>Abused women aren&apos;t foolish have. You&apos;re a terribly cruel person :(</string>
                                <int>538</int>
                              </entry>
                              <entry>
                                <string>My dog is so dumb but lovable. She&apos;s walked into walls, trees, cars etc because she&apos;s looking behind her to check if we&apos;re following her. She also keeps trying to eat the hedgehog in our garden. Good luck with that one.</string>
                                <int>387</int>
                              </entry>
                              <entry>
                                <string>WHAT A CROCK UP</string>
                                <int>49</int>
                              </entry>
                              <entry>
                                <string>THERE SEEMS TO BE A SAND NIGGER IN THE ROOM</string>
                                <int>178</int>
                              </entry>
                              <entry>
                                <string>you can see out the penthouse windows</string>
                                <int>449</int>
                              </entry>
                              <entry>
                                <string>I was 13 when it happened. It&apos;s never really bothered me, but my parents definitely cared. Now, I look at events like 9/11 and laugh. Don&apos;t ask me why, maybe its how I cope with tragedy, I don&apos;t know. I&apos;m not convinced that 9/11 wasn&apos;t an inside job, but its much easier to believe that it was just a cruel act of a few insane men. Believe what you want I guess, but once you start digging, you really cant go back. </string>
                                <int>472</int>
                              </entry>
                              <entry>
                                <string>Do you prefer cunt or whore? </string>
                                <int>121</int>
                              </entry>
                              <entry>
                                <string>Nigger</string>
                                <int>238</int>
                              </entry>
                              <entry>
                                <string>This one is by far my favorite ever.  http://www.youtube.com/watch?v=fpYrQplZ0VQ</string>
                                <int>564</int>
                              </entry>
                              <entry>
                                <string>this amazes me. you&apos;re actually asking what to talk about with your friends when you take drugs? you must be the most boring person (people) on the planet.</string>
                                <int>343</int>
                              </entry>
                              <entry>
                                <string>Fuck nationals.</string>
                                <int>55</int>
                              </entry>
                              <entry>
                                <string>&amp;gt-Silly child that wasn&apos;t Santa that was Satan. Easy mistake.  Stalin  FTFY</string>
                                <int>557</int>
                              </entry>
                              <entry>
                                <string>DayZ doesn&apos;t take place in the 80&apos;s you twit. The little story it has implies it takes place a few years from now. Aside from that, I&apos;m not going to bother arguing with you until you formulate real sentences instead of typing like a 14 year old on facebook.   That said I do agree with some of the things you wrote, though your presentation is horrid, makes you come off like a class A asshole. </string>
                                <int>462</int>
                              </entry>
                              <entry>
                                <string>VW tech here. All services up to 30k are free. For now</string>
                                <int>512</int>
                              </entry>
                              <entry>
                                <string>I would pay more to spite a crook.</string>
                                <int>438</int>
                              </entry>
                              <entry>
                                <string>I too, have hunted all over the place for one =( </string>
                                <int>492</int>
                              </entry>
                              <entry>
                                <string>Yeah why can&apos;t.i see your art work:( </string>
                                <int>448</int>
                              </entry>
                              <entry>
                                <string>&amp;gt- are heading to the online world at a rapid pace, and bandwidth to the home needs to increase to deal with it.  This is a really important thing that I think a lot of people are forgetting when it comes to internet speed. It&apos;s not like computers where it&apos;s really simple to upgrade hardware when we need it (e.g. adding a new hard drive). When you&apos;re talking about large infrastructure as this, we *need* to plan it for the future, and in IT, that means making it for usage that may seem ridiculous now.</string>
                                <int>501</int>
                              </entry>
                              <entry>
                                <string>&amp;gt- Can you imagine? A for-profit company imagining new ways to pay people they don&apos;t need?  Having worked in for-profit companies before, I can promise you- there are *tons* of jobs where the people do next to nothing. Hell, *I&apos;ve* had one of those jobs before. By the time I got another job, I seriously wondered why I was paid at all at my last one.</string>
                                <int>442</int>
                              </entry>
                              <entry>
                                <string>YOU DON&apos;T FUCKING SAY!</string>
                                <int>89</int>
                              </entry>
                              <entry>
                                <string>HEY GUYZ LUKE ME LEIK PERKIMANZ U WUV MYE NAW RIT?</string>
                                <int>209</int>
                              </entry>
                              <entry>
                                <string>I wanna hold your ass</string>
                                <int>81</int>
                              </entry>
                              <entry>
                                <string>Gays &amp;gt- Douches, faggot!</string>
                                <int>101</int>
                              </entry>
                              <entry>
                                <string>SPORK MOTHAFUCKA! CHECK YOSELF</string>
                                <int>124</int>
                              </entry>
                              <entry>
                                <string>I had a job like that i quit. I burnt out on 12 hour days and the last straw was when i found out the bitch was making a grand more a month than me to basically shove her work off on me and call in all the time and HR took her side because she&apos;s a single mom.&quot; When i wasnt around to do her work for her they found out how incompetent she was and got fired, but too late for me. The excuse for me always picking up her slack from management was &quot;she has a hard life, be more understanding.&quot; Im like, how more understanding can i be when i am working 7am to 9pm with a 90 minute commute each way? It was a law office and wouldnt you know, her kids always, like magic, got sick the day before major court cases or filing deadlines. What crazy coincidences!   Now i have a job where everyone gets to call in, kids or not.  Bossman just wants shit to get done and if you call in to go to little Timmy&apos;s soccer game then that is what vpn at ten pm is for. The end result is we all bust our asses. It took me until i was almost 40 to work in a job where parents actually work. &quot;</string>
                                <int>10</int>
                              </entry>
                              <entry>
                                <string>Fat sluts  ^/s</string>
                                <int>40</int>
                              </entry>
                              <entry>
                                <string>now we need someone to make it say something like, deal with it or, fuck the police.</string>
                                <int>277</int>
                              </entry>
                              <entry>
                                <string>Have you been to Wykikamoocau yet?</string>
                                <int>437</int>
                              </entry>
                              <entry>
                                <string>You&apos;re a stupid cunt.</string>
                                <int>79</int>
                              </entry>
                              <entry>
                                <string>Idiot... </string>
                                <int>286</int>
                              </entry>
                              <entry>
                                <string>Why do faggots like you make fun of CoD for no reason</string>
                                <int>218</int>
                              </entry>
                              <entry>
                                <string>You can&apos;t explain that.</string>
                                <int>393</int>
                              </entry>
                              <entry>
                                <string>GOAT</string>
                                <int>463</int>
                              </entry>
                              <entry>
                                <string>Tesla&apos;s work, no doubt. </string>
                                <int>394</int>
                              </entry>
                              <entry>
                                <string>If you think about it the dude contributes literally NOTHING to society.</string>
                                <int>548</int>
                              </entry>
                              <entry>
                                <string>ur such a faggot u got carried by tryndamere stfu lol</string>
                                <int>219</int>
                              </entry>
                              <entry>
                                <string>All my juice has gone to shit because of our wacky weather in New England. I had three 15ml bottles from a local B&amp;amp-M and they lost their flavor first after three weeks. I&apos;ve got a flavor called Desert, not a clue who it&apos;s made by, almost done with the 30ml bottle and I&apos;ve had it since I started vaping...so five or six weeks for that one. Still has its taste. I went through a 15ml bottle of Impearmint from Seduce Juice in a two weeks, neglecting all the rest of my flavors. Currently my daily vape is Snickerdoodle from Seduce Juice and after two weeks it still has its flavor. </string>
                                <int>522</int>
                              </entry>
                              <entry>
                                <string>This line always hits me in the gut, I don&apos;t know why...</string>
                                <int>515</int>
                              </entry>
                              <entry>
                                <string>lolololololololololololololololololololololololoololololololoolololololoolololoololololololoololololololololololololololololololololololololololololololoololololololoolololololoolololoololololololoololololololololololololololololololololololololololololololoololololololoolololololoolololoololololololoololololololololololololololololololololololololololololololoololololololoolololololoolololoololololololoololololololololololololololololololololololololololololololoololololololoolololololoolololoololololololoololololololololololololololololololololololololololololololoololololololoolololololoolololoololololololoololololololololololololololololololololololololololololololoololololololoolololololoolololoololololololoololololololo......sorry obama, cameron can&apos;t come out and play.</string>
                                <int>272</int>
                              </entry>
                              <entry>
                                <string>What&apos;s with people breaking up and acting like teenagers over it? Are you 14, OP? Because that&apos;s the only way this makes sense, that or you&apos;re an asshole and got dumped for a reason.</string>
                                <int>71</int>
                              </entry>
                              <entry>
                                <string>Nope. &quot;Character actor&quot; is used in a couple of different ways. It can mean a &quot;character&quot; type, as in not a leading man/woman, as the above casting director stated. It can also mean an actor who aims to transform into something quite different from their normal persona instead of the role being written to the persona. Harrison Ford will never be a character actor- he is always playing Harrison Ford, because that&apos;s what people want.   &quot;Method&quot; describes a particular school of acting that got a lot of press from the late 50s through mid-70s (though it was founded in the 30s and got its first Broadway exposure then). This ultimately had more to do with the press savvy of its primary guru, Lee Strasberg. Though it was most associated with Marlon Brando&apos;s particular brand of raw, animal talent, Brando did not ever acknowledge learning anything from Strasberg. (In fact, he claimed his training was all from Stella Adler, who consciously split from Strasberg&apos;s &quot;Method&quot; by 1934.)  Strasberg&apos;s work has largely fallen out of favor, and even those who teach in his name mostly teach an amalgam of his and other people&apos;s work. The &quot;Method&quot; term has become an oft-overused descriptor of any type of acting process that someone thinks overwrought or self-indulgent. Even though the term is misapplied in 90% of cases, it is true that these were some qualities of Strasberg&apos;s training that his peers and students came to distrust and disagree with.  Source: Ph.D. Candidate in Theatre specializing in Acting theory and cognitive science.</string>
                                <int>342</int>
                              </entry>
                              <entry>
                                <string>PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC •PC﻿ • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC •</string>
                                <int>206</int>
                              </entry>
                              <entry>
                                <string>After we got on the same page we&apos;ve been fighting side by side for a long time. God bless America and God bless Great Britain.</string>
                                <int>318</int>
                              </entry>
                              <entry>
                                <string>Fuck you, fundie slut</string>
                                <int>80</int>
                              </entry>
                              <entry>
                                <string>most have a better chance of beating superman 64.   but if a person is a passionate gamer, CRAZY hard-worker, humble, intelligent, etc., then Riot really appreciates that. They are hiring a lot of people right now, so I recommend anyone who fits the aforementioned bill to check out the careers page.</string>
                                <int>422</int>
                              </entry>
                              <entry>
                                <string>Keep at it.  I was in the same boat about a year ago. Gains after 17mph take more and more effort. I am good for 19-20mph for 60+ rides now.   Add distance to the efforts and the shorter ones will get faster as well.   https://www.strava.com/activities/81498963 </string>
                                <int>404</int>
                              </entry>
                              <entry>
                                <string>I just snorted</string>
                                <int>329</int>
                              </entry>
                              <entry>
                                <string>OP is being intentionally thick here. He&apos;s either an idiot or a troll</string>
                                <int>257</int>
                              </entry>
                              <entry>
                                <string>LIKE, OMIGAWD, I&apos;M OUT OF LIKE, PRINGLES. THIS IS LIKE, AFRICA #nofood #hunger #nobodyunderstands</string>
                                <int>293</int>
                              </entry>
                              <entry>
                                <string>We get a lot of hobo spiders here in upstate NY, along the Hudson river. I don&apos;t really care whether or not they consume other unwanted bugs. These fuckers are 2-3 inches long. I&apos;m not gonna sit there and tolerate them crawling all over my fucking apartment.</string>
                                <int>100</int>
                              </entry>
                              <entry>
                                <string>SORRY, BUT YOU DON&apos;T DESERVE TO PLAY FINALS!</string>
                                <int>185</int>
                              </entry>
                              <entry>
                                <string>MINUS? WHAT THE SHIT KIND OF NIGGER ARE YOU?</string>
                                <int>186</int>
                              </entry>
                              <entry>
                                <string>##THIS IS &quot;MURICA YOU CUM GUZZLIN FAGGOT  THE ONLY GOOD COMMIE LIKE YOU IS A DEAD [COMMIE](https://lh3.googleusercontent.com/-OvPhtjQOsTo/UU-62ds6SxI/AAAAAAAAAD4/afnyvIiYqcA/s1000-fcrop64=1,120233fcedfccc02/Meanwhile%252Bin%252BMurica%252B_062f93860376eea9654d7c1a8db8eb13.jpg)##</string>
                                <int>117</int>
                              </entry>
                              <entry>
                                <string>Could be the same guy, I have no idea! I&apos;ve never met him myself- she was just telling me this the other day. Glad your uncle is alive and well</string>
                                <int>334</int>
                              </entry>
                              <entry>
                                <string>Go fuck yourself nigger.</string>
                                <int>96</int>
                              </entry>
                              <entry>
                                <string>WHAT?! NIGEL FARAGE?! BUT ISN&apos;T HE THE BAD GUY!? OH GOD DOWNVOTE! LABOUR TOLD ME HE&apos;S BAD SO HE MUST BE HITLER HURRRRR.  How can anyone possibly downvote this? Even if, for some *insane* reason, you&apos;re against UKIP, how can you be pro Syrian intervention? Excuse my French, but how fucking stupid can you possibly be you absolute fucking piece of shit.</string>
                                <int>148</int>
                              </entry>
                              <entry>
                                <string>You are supposed to put that after the name of Allah (SWT) which stands for: Subhanahu wa ta&apos;ala. That translates into &quot;May He be Glorified and Exalted&quot;. After the Prophet Muhammed (SAW) you put SAW which stands for: á¹£all AllÄ&#x81;hu Ê¿alay-hi wa-sallam. It translates into &quot;May Allah honor him and grant him peace&quot; or more simply &quot;peace be upon him&quot;.</string>
                                <int>443</int>
                              </entry>
                              <entry>
                                <string>Ah, true love. The kind of lover you can tell your significant other that you want to fucking murder them. I hope my relationship is so open one day.</string>
                                <int>337</int>
                              </entry>
                              <entry>
                                <string>I GETS UP. AND THEN I PACK A BLUNT AND SMOKE IT AND DEN I HAVE MY WOMAN COOKS ME BREAKFAST AND DEN I MIGHT FUCKY WITH HER AND DEN I PROBABLY OR MOST LIKELY WILL GO AND ROB A NIGGER OR TWO AND RIDE ON OVER TO THE PAWN SHOP AND GETS WHAT I CAN GETS...  MY DAY VARIES</string>
                                <int>108</int>
                              </entry>
                              <entry>
                                <string>IT DEPENDS ON THE PERSON. POP RADIO OR NPR IS PROBABLY SAFEST</string>
                                <int>245</int>
                              </entry>
                              <entry>
                                <string>SHE TOOK THE MIDNIGHT TRAIN GOIN AAAA NEEEEE WHEEEERE </string>
                                <int>223</int>
                              </entry>
                              <entry>
                                <string>As a long term smoker this is not unusual. I try to take regular breaks from smoking (once a month for at least a week) to clear my system and get my head straight. Sometimes I take 18 month breaks to get my head straight, it&apos;s just about being in tune with your mind. If you feel fucked up and in a bad place every time you smoke then my advise is just go with it and don&apos;t smoke -)</string>
                                <int>459</int>
                              </entry>
                              <entry>
                                <string>This is such a dick move stop doing this please </string>
                                <int>202</int>
                              </entry>
                              <entry>
                                <string>&apos;*ELLO LUV! I ON ME BLOODY RAG, JESS TOSS IT EN ME DUMPUH*</string>
                                <int>232</int>
                              </entry>
                              <entry>
                                <string>Wrong subreddit faggot</string>
                                <int>87</int>
                              </entry>
                              <entry>
                                <string>Ooey.</string>
                                <int>496</int>
                              </entry>
                              <entry>
                                <string>pls respond LOLOLO SO ORIGINAL RITE GUISE?!</string>
                                <int>180</int>
                              </entry>
                              <entry>
                                <string>SEE, THIS IS WHY ROBOTS CANT TAKE OUR JOBS, THEY ARENT VERY GOOD AT SOME THINGS SOMETIMES. Unlike us....</string>
                                <int>4</int>
                              </entry>
                              <entry>
                                <string>I WISH WE COULD PLAY THE BILLS 8TH STRING DEFENSE EVERY WEEK. ALSO LEINART LOLOLOLOLOL</string>
                                <int>280</int>
                              </entry>
                              <entry>
                                <string>thanks.  i am trying to decide if I should try to get insanely fit or just decide to let it go heh</string>
                                <int>592</int>
                              </entry>
                              <entry>
                                <string>AYO DIS SERUS AS SH!T NIQQA YOU KNOW DEM PATRITS SENT DAT SECSHUN 8 N2 AFEKT SO I GOT DAT FREE HOWSE #FREE4LYF #LILB #BASEDGOD #SWAG</string>
                                <int>35</int>
                              </entry>
                              <entry>
                                <string>i swear to fucking satan you fucking link me to a fucking nigger song again and make my fucking youtube recommendations all fucking nigger trash sounds ill fucking kill you, once again. dont fucking tag nigger shit without making the tag NIGGERSHITSOUNDS you fucking filthy faggot hood rat</string>
                                <int>118</int>
                              </entry>
                              <entry>
                                <string>My classic army LWRC M6a2</string>
                                <int>399</int>
                              </entry>
                              <entry>
                                <string>What a fucking cunt!</string>
                                <int>74</int>
                              </entry>
                              <entry>
                                <string>Those are some powerful bright lights. </string>
                                <int>460</int>
                              </entry>
                              <entry>
                                <string>Can of Coke- worked out to seven or eight dollars in Bora Bora.</string>
                                <int>531</int>
                              </entry>
                              <entry>
                                <string>Why?  What possible benefit could this have that would outweigh the risk to the employees lives?</string>
                                <int>591</int>
                              </entry>
                              <entry>
                                <string>**EMPTY IT ALL INTO A SINGLE RUBBERMAID BIN.**</string>
                                <int>191</int>
                              </entry>
                              <entry>
                                <string>Bitch.&quot;&quot;</string>
                                <int>25</int>
                              </entry>
                              <entry>
                                <string>Thanks mine is an e-type ots.</string>
                                <int>414</int>
                              </entry>
                              <entry>
                                <string>u want di.ck in the ass ;)</string>
                                <int>98</int>
                              </entry>
                              <entry>
                                <string>Does this polypropylene rope look okay to use? http://www.homedepot.com/p/Everbilt-1-4-in-x-100-ft-Diamond-Braid-Poly-Rope-in-Yellow-Black-17984/202048183#.UjEEy8bkt8E</string>
                                <int>353</int>
                              </entry>
                              <entry>
                                <string>Agreed</string>
                                <int>524</int>
                              </entry>
                              <entry>
                                <string>s-o-c-k-s</string>
                                <int>580</int>
                              </entry>
                              <entry>
                                <string>Works beautifully. I use the side button to Toggle eraser on/off</string>
                                <int>534</int>
                              </entry>
                              <entry>
                                <string>I&apos;m old old enough that I don&apos;t give a shit about civility.  I may have had a couple when I replied.  But truth be told at my age, I&apos;m sick of this bullshit that we as men have to keep our emotions bottled up inside.  It&apos;s not fucking healthy.  And it doesn&apos;t make us weak to express how we feel.  All it does is make us one step to blowing our brains out.  So fucking what, we express our emotions.  If you really believe that a partner is going to disrespect you for that, then you shouldn&apos;t be with her to begin with (assuming you&apos;re a straight male).  It&apos;s time as men that we claim that we are emotion beings as much as any one and deal with the fallout.  The only thing stoicism will give you is ulcers and cold sores.  Fuck off and get over your bad self.</string>
                                <int>268</int>
                              </entry>
                              <entry>
                                <string>I attended a summer music program this year (I&apos;m a pretty damn decent percussionist) and as an orchestra we focused a lot on contemporary composers. Hearing a summerful of atonal music changed my views of what i put and and what i dont. Don&apos;t follow rules because you feel you have to. Create whatever YOU want to.</string>
                                <int>425</int>
                              </entry>
                              <entry>
                                <string>For me it was all the kids bullying me at school.</string>
                                <int>491</int>
                              </entry>
                              <entry>
                                <string>That picture is older than my niece. And no my niece was not born today. ALL HAIL OP, KING OF FAGGOTVILLE</string>
                                <int>7</int>
                              </entry>
                              <entry>
                                <string>No homo.</string>
                                <int>562</int>
                              </entry>
                              <entry>
                                <string>Ask your doctor and stop medicating yourself, you fucking dumbass</string>
                                <int>249</int>
                              </entry>
                              <entry>
                                <string>Sort of like this: 01001101 01101111 01110010 01100101 00100000 01101100 01101001 01101011 01100101 00100000 01110111 01101111 01110010 01101100 01100100 00100000 01110111 01100001 01110010 00100000 01000110 01010101 01001110 00101100 00100000 01100001 01101101 00100000 01001001 00100000 01110010 01101001 01100111 01101000 01110100 00111111</string>
                                <int>142</int>
                              </entry>
                              <entry>
                                <string>He&apos;s a scrappy dude, I&apos;ll give him that, but it was absolutely terrifying having him on the field trying to cover guys.  He was a decent tackler, but he could not stick with anybody.</string>
                                <int>364</int>
                              </entry>
                              <entry>
                                <string>You&apos;re a TERRIBLE LIAR OKAY, BECAUSE OVER 100 METERS YOU HAVE TO HEADSHOT, or else your bow won&apos;t get the kill, and no way guy that&apos;s just straight balonga!</string>
                                <int>60</int>
                              </entry>
                              <entry>
                                <string>AND THEN THE ULTRA RICH WILL LIVE ON THE MOON AND WE WILL ALL BUILD A FUCK YOU ROCKET AND POINT IT AT THE MOON AND THEN THE MOON PEOPLE WILL SEND THEIR SHIPS AND ALL WE WILL HAVE ARE TANKS THAT SHOOT DIRECTLY UP.  Space Invaders.</string>
                                <int>90</int>
                              </entry>
                              <entry>
                                <string>IT&apos;S JIM CUNTING WHITE. HERE WE GO.</string>
                                <int>144</int>
                              </entry>
                              <entry>
                                <string>Yellow sac spiders can suck. They can possibly cause that same necrotic condition than Recluses can, just not as often because they aren&apos;t nearly as poisonous.   Still. They&apos;re freaky little bastards. If they or their web is disturbed they drop to whatever surface is under them and take off, either to attack or escape. It makes them tough to catch (if you&apos;re like me and hate killing spiders. I catch them and toss them outside)</string>
                                <int>183</int>
                              </entry>
                              <entry>
                                <string>What up tittays</string>
                                <int>339</int>
                              </entry>
                              <entry>
                                <string>It sounds like Gamestop is giving out an extra gold pack a week as it&apos;s promotion ASIDE from the EA Season Ticket. So it might be 2. </string>
                                <int>325</int>
                              </entry>
                              <entry>
                                <string>But is it tax deductible? I think not.</string>
                                <int>455</int>
                              </entry>
                              <entry>
                                <string>GOOOOOAAAAAAALLLLLLLLLL</string>
                                <int>91</int>
                              </entry>
                              <entry>
                                <string>YOU SHUT YOUR WHORE MOUTH! WE WILL SIGN AND ANNOUNCE PLAYERS AT A REASONABLE TI- Ah I couldn&apos;t get myself to finish that sentence. </string>
                                <int>34</int>
                              </entry>
                              <entry>
                                <string>CAN&apos;T ANYONE SEE THAT TIME IS AGAINST THIS MAN?! GIVE HIM HIS FUCKING CROISSANTS, STAT! </string>
                                <int>285</int>
                              </entry>
                              <entry>
                                <string>Which one did you get?</string>
                                <int>390</int>
                              </entry>
                              <entry>
                                <string>Wow, the 1990s called, they want their joke back.  How is that &quot;INSANE WOLFFFFF&quot; material?  How about: No means yes and yes means a 1 man 1 jar.</string>
                                <int>336</int>
                              </entry>
                              <entry>
                                <string>Enjoy the karma you Crack whore.</string>
                                <int>132</int>
                              </entry>
                              <entry>
                                <string>[yes?](http://i.imgur.com/TKH6v8Q.gif)</string>
                                <int>457</int>
                              </entry>
                              <entry>
                                <string>Shut up faggot Breaking Bad can do no wrong</string>
                                <int>182</int>
                              </entry>
                              <entry>
                                <string>Patrol car or head of a shoulder-fired rocket weapon.</string>
                                <int>507</int>
                              </entry>
                              <entry>
                                <string>I&apos;m envious that you get to enjoy it all for the first time right now, haha! But regardless - have fun, you are in for a hell of a ride, and prepare for an addiction like no other.</string>
                                <int>363</int>
                              </entry>
                              <entry>
                                <string>The scale is a lying bitch. Just saying.</string>
                                <int>172</int>
                              </entry>
                              <entry>
                                <string>Unless Season 8 does something jaw-dropping in the last two eps, Season 8 will 100% go down as the worst season, no doubt.</string>
                                <int>315</int>
                              </entry>
                              <entry>
                                <string>BRANDON FREAKIN&apos; MOSS!  CARGO GOES ON THE DL.  NO PROBLEM. LET&apos;S SEE WHAT&apos;S ON THE WIRE?  5HR - 11 RBI - 8R OVER THE PAST 7 GAMES?  YEAH, LIFE IS GOOD.</string>
                                <int>57</int>
                              </entry>
                              <entry>
                                <string>Who doesn&apos;t? </string>
                                <int>322</int>
                              </entry>
                              <entry>
                                <string>Thanks.  </string>
                                <int>578</int>
                              </entry>
                              <entry>
                                <string>Rats won&apos;t get nearly this high because meth and money aren&apos;t worth as much as weapons and gold, which make up a good portion of the bags in firestarter.</string>
                                <int>340</int>
                              </entry>
                              <entry>
                                <string>amazing meaty pussy, yay</string>
                                <int>97</int>
                              </entry>
                              <entry>
                                <string>Why can&apos;t we post Arencibia&apos;s stats? This is the blue jays sub, his stats and anybody elses on the jays can go here no matter how good or bad.</string>
                                <int>332</int>
                              </entry>
                              <entry>
                                <string>I GUESS THEY DON&apos;T TEACH YOU HOW TO USE YOUR MOUTH AT THE SAME TIME AS YOUR HANDS IN CHICAGO.</string>
                                <int>289</int>
                              </entry>
                              <entry>
                                <string>I was just in the area when I took the screenshot haha. I was heading north. </string>
                                <int>556</int>
                              </entry>
                              <entry>
                                <string>My DICK is good at analyzing bad advice.  BAM!</string>
                                <int>192</int>
                              </entry>
                              <entry>
                                <string>CALM THE FUCK DOWN YOU SILLY CUNT AND DON&apos;T READ IT THEN. with love x</string>
                                <int>256</int>
                              </entry>
                              <entry>
                                <string>I know exactly how you feel. The meds I&apos;m on make me really tired so I find it easy to sleep my days away and hide from my problems. Getting up in the morning feels like climbing Everest and staying up feels like climbing again. </string>
                                <int>391</int>
                              </entry>
                              <entry>
                                <string>Muslims over the world have been crying this for ages now, and the only reason any hatred that exists in Muslim world is because of it.  No one hates USA itself.</string>
                                <int>349</int>
                              </entry>
                              <entry>
                                <string>You back in the days when you were a cunt?  Yeah, probably.</string>
                                <int>236</int>
                              </entry>
                              <entry>
                                <string>Oh, in that case. Wasn&apos;t thinking about Irish Gaelic for some reason. In that case, I agree it would probably not be the easiest to pronounce.</string>
                                <int>333</int>
                              </entry>
                              <entry>
                                <string>Thats awesome I&apos;m glad you showed that punk whats up.  Its supposed be a gentleman&apos;s game.  And fuck him for talking smack about your pre shot routine.  The range is for practice&quot; so yes, weat your golf shoes and practice your routine to hit targets.  Dont just go out and pound the ball aimlessly.  Well done sir&quot;</string>
                                <int>127</int>
                              </entry>
                              <entry>
                                <string>SUP IUW. I KNOW YOU&apos;LL BE A CONFERENCE GAME NEXT YEAR. OR IS THAT THIS YEAR? FUCK IF I REMEMBER, THAT&apos;S HOW INCONSEQUENTIAL YOU ARE TO ME.</string>
                                <int>38</int>
                              </entry>
                              <entry>
                                <string>LAMAO</string>
                                <int>208</int>
                              </entry>
                              <entry>
                                <string>Malkeru noticed the odd look and followed the girl thinking, &apos;God, this will be so much fun.&apos;</string>
                                <int>586</int>
                              </entry>
                              <entry>
                                <string>Yo mama is such a ghetto slut, she has blown more black guys than hurricane Katrina. </string>
                                <int>278</int>
                              </entry>
                              <entry>
                                <string>OI FUCK YOU ARSENAL CUNT. GO AWAY YOU GOONING WANKER</string>
                                <int>214</int>
                              </entry>
                              <entry>
                                <string>That is the only cupcake/cookie I tried from them. Quite tasty.</string>
                                <int>532</int>
                              </entry>
                              <entry>
                                <string>Ways Clerks related to other films in Kevin Smith&apos;s New Jersey Trilogy&quot; (actually 6 films)  - Willem Black, the guy who got snowballed in Clerks is supposedly the same character in  Mallrats that can&apos;t see the schooner in the Magic Eye picture.  - Dante and Randal ditch work to go to Julie Dwyer&apos;s funeral. Julie Dwyer died while swimming laps to lose weight for the game show that Brandi Svenning&apos;s dad created in Mallrats.   - In the scene where Heather Jones and Rick Derris are telling Dante how terrible and out of shape he is, Heather Jones is the younger sister of Alyssa &quot;Fingercuffs&quot; Jones from Chasing Amy. Rick Derris is the one who fucked Gwen Turner (also played by Joey Lauren Adams) on a pool table in Mallrats. Also, the bus that Bartleby and Loki are traveling on in Dogma is the Derris Bus Lines.  - Brian O&apos; Halloran plays characters that are &quot;cousins&quot; of each other. Dante Hicks in Clerks, Gil Hicks in Mallrats, reporter Grant Hicks in Dogma, and TV executive Jim Hicks in Chasing Amy.&quot;</string>
                                <int>3</int>
                              </entry>
                              <entry>
                                <string>[archetypal projection](http://upload.wikimedia.org/wikipedia/commons/thumb/7/72/OHP-sch.JPG/300px-OHP-sch.JPG)</string>
                                <int>311</int>
                              </entry>
                              <entry>
                                <string>I wish he brushes his teeth regularly. Filthy scumbag. </string>
                                <int>227</int>
                              </entry>
                              <entry>
                                <string>As a healer, yea they suck unless there is adds on the boss.  2k HP over Pallys, yet they get ripped 3x quicker and have no interupts.  Hopefully over time some changes will happen though.</string>
                                <int>368</int>
                              </entry>
                              <entry>
                                <string>&amp;gt; While changing one&apos;s weight is not an easy task and may have medical issues that prevent traditional weight loss methods from working, it is still possible.  No, you are forcing someone to change who they are in order to fit in to society so they aren&apos;t harassed with slurs like lard-ass&quot;.  If I don&apos;t want to be called a &quot;lard-ass&quot; I should lose weight.  But if I tell someone that if they don&apos;t want to be called &quot;faggot&quot; they should act like a straight person, you&apos;d probably find that offensive.  Go fuck yourself.  I know exactly what it feels like to be a second-class citizen.  &quot;</string>
                                <int>169</int>
                              </entry>
                              <entry>
                                <string>Ol&apos; Staff Sarn&apos; gonna get that ass...</string>
                                <int>447</int>
                              </entry>
                            </symbolToIndexMap>
                            <indexToSymbolMap id="46">
                              <string>;_;7 RIP IN PEACE YOU MAGNIFICENT BASTARD</string>
                              <string>tl-dr YOLO</string>
                              <string>THANK YOU </string>
                              <string>Ways Clerks related to other films in Kevin Smith&apos;s New Jersey Trilogy&quot; (actually 6 films)  - Willem Black, the guy who got snowballed in Clerks is supposedly the same character in  Mallrats that can&apos;t see the schooner in the Magic Eye picture.  - Dante and Randal ditch work to go to Julie Dwyer&apos;s funeral. Julie Dwyer died while swimming laps to lose weight for the game show that Brandi Svenning&apos;s dad created in Mallrats.   - In the scene where Heather Jones and Rick Derris are telling Dante how terrible and out of shape he is, Heather Jones is the younger sister of Alyssa &quot;Fingercuffs&quot; Jones from Chasing Amy. Rick Derris is the one who fucked Gwen Turner (also played by Joey Lauren Adams) on a pool table in Mallrats. Also, the bus that Bartleby and Loki are traveling on in Dogma is the Derris Bus Lines.  - Brian O&apos; Halloran plays characters that are &quot;cousins&quot; of each other. Dante Hicks in Clerks, Gil Hicks in Mallrats, reporter Grant Hicks in Dogma, and TV executive Jim Hicks in Chasing Amy.&quot;</string>
                              <string>SEE, THIS IS WHY ROBOTS CANT TAKE OUR JOBS, THEY ARENT VERY GOOD AT SOME THINGS SOMETIMES. Unlike us....</string>
                              <string>thanks for your concern for my social status.   I hope your teenage bitch sarcasm wears off one day, bro</string>
                              <string>The only way in which this story makes sense is if your boyfriend and P are actually fucking each other.</string>
                              <string>That picture is older than my niece. And no my niece was not born today. ALL HAIL OP, KING OF FAGGOTVILLE</string>
                              <string>HOW TO MAKE CHEAP AND EASY KARMA:  Step 1: GIFify a popular front page youtube vid post. Step 2: Post it.</string>
                              <string>lilstevie wrote:   It&apos;s funny because niggers steal.    You&apos;re all ugly cunts, btw.    -------  **Deleted**</string>
                              <string>I had a job like that i quit. I burnt out on 12 hour days and the last straw was when i found out the bitch was making a grand more a month than me to basically shove her work off on me and call in all the time and HR took her side because she&apos;s a single mom.&quot; When i wasnt around to do her work for her they found out how incompetent she was and got fired, but too late for me. The excuse for me always picking up her slack from management was &quot;she has a hard life, be more understanding.&quot; Im like, how more understanding can i be when i am working 7am to 9pm with a 90 minute commute each way? It was a law office and wouldnt you know, her kids always, like magic, got sick the day before major court cases or filing deadlines. What crazy coincidences!   Now i have a job where everyone gets to call in, kids or not.  Bossman just wants shit to get done and if you call in to go to little Timmy&apos;s soccer game then that is what vpn at ten pm is for. The end result is we all bust our asses. It took me until i was almost 40 to work in a job where parents actually work. &quot;</string>
                              <string>FROLIC SLUT</string>
                              <string>I found out his first name is Richard.  Dick Riddick. That name has two Dick&quot;s.   it&apos;s a re-dick.  ^sorry&quot;</string>
                              <string>Look at the fucking url you idiots. Information Clearing House&quot; is where you get your news? Jesus Christ.&quot;</string>
                              <string>Sorry, Baby Cunt Puncher, you are right. This forum is not the place. I will take my downvotes and potential ban.</string>
                              <string>DIDN&apos;T THEY DO SOMETHING TO EARN OUR HATE WITH SEC AND DOLLAR SIGNS OR SOMETHING? I THOUGHT WE HATED RICE FOR THAT. </string>
                              <string>il dek u in da gabba m8 I swer on me mum ur a propa cunt bruv im the sikkest bloke ul cum acros u shit eating wanka </string>
                              <string>If your penis, or a penis that you know, is suffering from depression, don&apos;t just walk away. have it seek treatment.</string>
                              <string>&apos;straya cunt</string>
                              <string>Noah nigger.</string>
                              <string>Yeah. Cunt. </string>
                              <string>You BASTARD.</string>
                              <string>shut up slut</string>
                              <string>sand niggers</string>
                              <string>Fuck niggers</string>
                              <string>Bitch.&quot;&quot;</string>
                              <string>Dude you completely misspelled it. It&apos;s spelled  DIIIEEEEYAAAAAHHHEEEEEEEIIIIIIAHAHAHAAAOOOAYAYAHAHAYAYEIEIEIEEEEEEEEE**</string>
                              <string>Congratulations you have become the worlds biggest faggot OP to claim your prize follow the link http://imgur.com/kAmDTqz</string>
                              <string>why does the horse at the top of the page have an antennae, is she ill? wait is it rude to talk about her deformity my bad</string>
                              <string>YOUNG N GETTIN IT YOUNG N GETTIN IT YOUNG N GETTIN IT YOUNG N GETTIN IT YOUNG N GETTIN IT YOUNG N GETTIN IT YOUNG N GETTIN IT </string>
                              <string>RAJESH KUMAR GIRI A PROFESSIONAL APPLIED MATHEMATICS TEACHER FROM DELHI WILLINGS TO MAKE YOU ALL A WINNER. CALL- +91-8527307169</string>
                              <string>YOU SHUT YOUR WHORE MOUTH I WANT IT NOW. Jk, in all honesty I&apos;d accept that if it was a guarantee that it would be mind blowing.</string>
                              <string>um  I dont have one? I was saying you son of a bitch&quot; as in &quot;you talented son of a bitch, it pains me how awesome that is&quot;&quot;</string>
                              <string>Y U NO DOCTOR</string>
                              <string>YOU SHUT YOUR WHORE MOUTH! WE WILL SIGN AND ANNOUNCE PLAYERS AT A REASONABLE TI- Ah I couldn&apos;t get myself to finish that sentence. </string>
                              <string>AYO DIS SERUS AS SH!T NIQQA YOU KNOW DEM PATRITS SENT DAT SECSHUN 8 N2 AFEKT SO I GOT DAT FREE HOWSE #FREE4LYF #LILB #BASEDGOD #SWAG</string>
                              <string>But hey, he was a fucking ventriloquist. That&apos;s a talent. Binkie is just some bullshit ass nigger ass whore that bullies everyone.  </string>
                              <string>..........FUUUUCK YOU BIWEEKLY CHECKS!!!   FUCK!!!!   WHY!!!!!!!!   FUUUUCK  I AM GOING TO THROW A BF ALL OVER MY WHOLE HOUSE  FUCK ME</string>
                              <string>SUP IUW. I KNOW YOU&apos;LL BE A CONFERENCE GAME NEXT YEAR. OR IS THAT THIS YEAR? FUCK IF I REMEMBER, THAT&apos;S HOW INCONSEQUENTIAL YOU ARE TO ME.</string>
                              <string>I tried to bang this chick in a wheelchair I met once but I got nowhere. That&apos;s the extent of my experience into banging the handicapped. </string>
                              <string>Fat sluts  ^/s</string>
                              <string>Sigh...niggers</string>
                              <string>Op is a faggot</string>
                              <string>HAH GAYYYYYYYY</string>
                              <string>ALL MALE ATTRACTION IS RAPE!  *ALL MALE ATTRACTION IS RAPE!*  **ALL MALE ATTRACTION IS RAPE**  ***ALL. MALE. ATTRACTION. IS. RAPE!***  Kaka khan.</string>
                              <string>ALL THESE WHISPERQUEEF COMMENTS MAKIN MY PANTSGAT RIGID, NOW YOU PUT UP A PIC OF THIS SWEET SHOTGUN?  ARE U TRYIN TO GET A BITCH RAPED UP IN HERE?</string>
                              <string>NAH NAH FUCK YOU OFWGKTADGAFLSLLBB TRIPLE 6 NIGGA DGAF NIGGA DANNY BROWN IS NOTHING COMPARED TO ANYONE THATS BEEN IN A TYLER SONG #GOLFWANGFORLIFE </string>
                              <string>Walk outside. Right now. Find 10 people. Ask them if they like 3D movies. If you find more than 3 that say yes, I would be surprised. Fuck your bear.</string>
                              <string>boo fucking hoo</string>
                              <string>WHAT A CROCK UP</string>
                              <string>YOLO SWAG BITCH</string>
                              <string>You&apos;re a cunt. </string>
                              <string>you... BASTARD.</string>
                              <string>Fucking niggers</string>
                              <string>Where Whores Go</string>
                              <string>Fuck nationals.</string>
                              <string>yup. leave it to all you fat fucks in fitness to upvote yet another pointless post. youre all ugly as fuck niggers stop contributing the to cancer pls</string>
                              <string>BRANDON FREAKIN&apos; MOSS!  CARGO GOES ON THE DL.  NO PROBLEM. LET&apos;S SEE WHAT&apos;S ON THE WIRE?  5HR - 11 RBI - 8R OVER THE PAST 7 GAMES?  YEAH, LIFE IS GOOD.</string>
                              <string>GUYS THESE TIPS ARE COMPLETELY LEGIT  AFTER MY 3RD SEXUAL HARASSMENT CONVICTION I FINALLY SCORED WITH A DRUNK SLUT USING THIS ADVICE  THANK YOU ASKMEN.COM!</string>
                              <string>Okay, what&apos;s wrong with the I-Cues? As someone who&apos;s only done lighting on a budget, those motherfuckers were useful as hell when we needed a little fill. </string>
                              <string>You&apos;re a TERRIBLE LIAR OKAY, BECAUSE OVER 100 METERS YOU HAVE TO HEADSHOT, or else your bow won&apos;t get the kill, and no way guy that&apos;s just straight balonga!</string>
                              <string>Fuck you whats your point? I built my own business and now I get to have some fun. Your no better then some nigger telling me I need to check my privilege.  </string>
                              <string>There are a lot of asshole redditors who like to downvote for stupid reasons. Maybe someone thought you were being pretentious or something.  I don&apos;t, though; I&apos;m happy to be edified. Allow me to console you with an upvote for your timely knowledge.</string>
                              <string>Trolololololol??</string>
                              <string>fuck you, cunt. </string>
                              <string>Heh, ur stoopid.</string>
                              <string>Now THAT&apos;S a daddy long bitch if I&apos;ve ever seen one. I pray those things aren&apos;t in Australia, we&apos;ve got enough around here we don&apos;t need fucking *baboon* spiders</string>
                              <string>THEY&apos;LL GET BACK TO IT SOON I&apos;M SURE, DON&apos;T WORRY. DIDN&apos;T YOU HEAR WE ALL WORSHIP SATAN IN STATE COLLEGE?  ALSO, YOUR LACK OF FLAIR CONFUSES ME. FLAIR UP, BROTHER.</string>
                              <string>Good ol Anal cunt!</string>
                              <string>niggers are racist</string>
                              <string>come. fucking. on.</string>
                              <string>What&apos;s with people breaking up and acting like teenagers over it? Are you 14, OP? Because that&apos;s the only way this makes sense, that or you&apos;re an asshole and got dumped for a reason.</string>
                              <string>you... you bastard.</string>
                              <string>TRAIN BY DAY JOE ROGAN PODCAST BY NIGHT ALL DAY!!!! Talkin shit with eddie ifft. Dan Carlins hardcore history is dope if you wanna do some learning.  Startalk with Neil deGrasse Tyson is awesome </string>
                              <string>What a fucking cunt!</string>
                              <string>wimp or karma whore?</string>
                              <string>**LE GEM****^LE ^GEM****^^LE ^^GEM****^^^LE ^^^GEM****^^^^LE ^^^^GEM****^^^^^LE ^^^^^GEM****^^^^^^LE ^^^^^^GEM****^^^^^^^LE ^^^^^^^GEM****LE GEM****^LE ^GEM****^^LE ^^GEM****^^^LE ^^^GEM****^^^^LE ^^^^GEM****^^^^^LE ^^^^^GEM****^^^^^^LE ^^^^^^GEM****^^^^^^^LE ^^^^^^^GEM** **LE GEM****^LE ^GEM****^^LE ^^GEM****^^^LE ^^^GEM****^^^^LE ^^^^GEM****^^^^^LE ^^^^^GEM****^^^^^^LE ^^^^^^GEM****^^^^^^^LE ^^^^^^^GEM** **LE GEM****^LE ^GEM****^^LE ^^GEM****^^^LE ^^^GEM****^^^^LE ^^^^GEM****^^^^^LE ^^^^^GEM****^^^^^^LE ^^^^^^GEM****^^^^^^^LE ^^^^^^^GEM** **LE GEM****^LE ^GEM****^^LE ^^GEM****^^^LE ^^^GEM****^^^^LE ^^^^GEM****^^^^^LE ^^^^^GEM****^^^^^^LE ^^^^^^GEM****^^^^^^^LE ^^^^^^^GEM** **LE GEM****^LE ^GEM****^^LE ^^GEM****^^^LE ^^^GEM****^^^^LE ^^^^GEM****^^^^^LE ^^^^^GEM****^^^^^^LE ^^^^^^GEM****^^^^^^^LE ^^^^^^^GEM** **LE GEM****^LE ^GEM****^^LE ^^GEM****^^^LE ^^^GEM****^^^^LE ^^^^GEM****^^^^^LE ^^^^^GEM****^^^^^^LE ^^^^^^GEM****^^^^^^^LE ^^^^^^^GEM** **LE GEM****^LE ^GEM****^^LE ^^GEM****^^^LE ^^^GEM****^^^^LE ^^^^GEM****^^^^^LE ^^^^^GEM****^^^^^^LE ^^^^^^GEM****^^^^^^^LE ^^^^^^^GEM**    **^^^^^^^LE ^^^^^^^GEM****^^^^^^LE ^^^^^^GEM****^^^^^LE ^^^^^GEM****^^^^LE ^^^^GEM****^^^LE ^^^GEM****^^LE ^^GEM****^LE ^GEM****LE GEM****^^^^^^^LE ^^^^^^^GEM****^^^^^^LE ^^^^^^GEM****^^^^^LE ^^^^^GEM****^^^^LE ^^^^GEM****^^^LE ^^^GEM****^^LE ^^GEM****^LE ^GEM****LE GEM****^^^^^^^LE ^^^^^^^GEM****^^^^^^LE ^^^^^^GEM****^^^^^LE ^^^^^GEM****^^^^LE ^^^^GEM****^^^LE ^^^GEM****^^LE ^^GEM****^LE ^GEM****LE GEM****^^^^^^^LE ^^^^^^^GEM****^^^^^^LE ^^^^^^GEM****^^^^^LE ^^^^^GEM****^^^^LE ^^^^GEM****^^^LE ^^^GEM****^^LE ^^GEM****^LE ^GEM****LE GEM****^^^^^^^LE ^^^^^^^GEM****^^^^^^LE ^^^^^^GEM****^^^^^LE ^^^^^GEM****^^^^LE ^^^^GEM****^^^LE ^^^GEM****^^LE ^^GEM****^LE ^GEM****LE GEM****^^^^^^^LE ^^^^^^^GEM****^^^^^^LE ^^^^^^GEM****^^^^^LE ^^^^^GEM****^^^^LE ^^^^GEM****^^^LE ^^^GEM****^^LE ^^GEM****^LE ^GEM****LE GEM****^^^^^^^LE ^^^^^^^GEM****^^^^^^LE ^^^^^^GEM****^^^^^LE ^^^^^GEM****^^^^LE ^^^^GEM****^^^LE ^^^GEM****^^LE ^^GEM****^LE ^GEM****LE GEM**</string>
                              <string>OUR NAME IS THE REDSKINS. I don&apos;t give a shit if you think it&apos;s racist, Bill, you don&apos;t get to decide what we&apos;re referred to just because you&apos;re uncomfortable.  Fuck Grantland and their faux-PC bullshit.   </string>
                              <string>LIKE FUCK HUMANS GUYZ</string>
                              <string>You&apos;re a stupid cunt.</string>
                              <string>Fuck you, fundie slut</string>
                              <string>I wanna hold your ass</string>
                              <string>Oh shut up you prude.</string>
                              <string>Hot glue motherfucker</string>
                              <string>*Hey, faggot, quit grabbin&apos; my dick yo, hey err&apos;one, this motherfucker is grabbin&apos; on my cock yo! What a fucking faggot!*  ...it&apos;s not hard to imagine someone much less classy handling this in a much less classy way.</string>
                              <string>YOUR TIGER IS HAVING A BAD TRIP 24/7. I DON&apos;T THINK YOU CAN TALK ABOUT WHAT A LAME ASS MASCOT WE HAVE, WHEN YOUR&apos;S IS JUST AS LIKELY TO TEAR OUT HIS FINGER NAILS AS TO LEAD YOU IN A ROUSING RENDITION OF THE TIGER RAG? </string>
                              <string>Shut up bitch ass slut</string>
                              <string>Wrong subreddit faggot</string>
                              <string>ARGOfuckyourself&quot;&quot;</string>
                              <string>YOU DON&apos;T FUCKING SAY!</string>
                              <string>AND THEN THE ULTRA RICH WILL LIVE ON THE MOON AND WE WILL ALL BUILD A FUCK YOU ROCKET AND POINT IT AT THE MOON AND THEN THE MOON PEOPLE WILL SEND THEIR SHIPS AND ALL WE WILL HAVE ARE TANKS THAT SHOOT DIRECTLY UP.  Space Invaders.</string>
                              <string>GOOOOOAAAAAAALLLLLLLLLL</string>
                              <string>I would find out why she was seeking attention from another man. I hate to say it, but when a woman emotionally checks out from a relationship, there has to be a reason. Unless she&apos;s a bitch. If she&apos;s a bitch, tell her to hit the bricks. </string>
                              <string>lololo!! LOL!!!!! i bet!</string>
                              <string>TIME TO SCORE, SOXBROS! </string>
                              <string>No, but your cunt does. </string>
                              <string>Go fuck yourself nigger.</string>
                              <string>amazing meaty pussy, yay</string>
                              <string>u want di.ck in the ass ;)</string>
                              <string>ALRIGHT YOU FUCKING COCK SUCKING TRY-HARDS, WE&apos;VE GOT SOME NEW RULES AROUND HERE. THE PEOPLE UPSTAIRS HAVE DECIDED THAT WE&apos;VE KICKED SO MUCH ASS WITH REG GUNS OVER THE YEARS, IT&apos;S TIME TO FUCK PEOPLE UP IN STYLE! WE DON&apos;T GIVE A SHIT ABOUT HOW GOOD YOU ARE WITH A RED DOT M16 ANYMORE, THAT SHIT IS LAMER THAN YOUR DADS WILLY ON THE NIGHT HE MADE YOU SORRY CUM STAINS!    THIS IS THE NEW WEAPON OF CHOICE, THE CHEYTAC INTERVENTION. THIRTY ONE MOTHER FUCKING POUNDS OF WHOOP ASS! YES THAT&apos;S RIGHT, THIRTY ONE POUNDS! IF YOU FAT ASSES CAN LUG AROUND YOUR BEER GUTS ALL DAY, YOU SHOULD BE ABLE TO CARRY THIS BEAUTY INTO WAR! BUT I DON&apos;T JUST WANT YOU TO CARRY IT, I WANT YOU TO LOVE IT! THIS RIFLE IS YOUR LIFE! THIS RIFLE IS THE KEY TO GETTING THE 1337 KILLS THAT WILL MAKE YOU A YOUTUBE LEGEND! THIS IS HOW YOU CAN GET THAT MACHINIMA CONTRACT THAT YOU HAD WET DREAMS ABOUT WHEN YOU WERE 12 AND STILL THOUGH PUSSY WAS GROSS, HELL MOST OF YOU PROBABLY STILL DO!   NOW THAT YOU&apos;VE BECOME ACQUAINTED WITH YOUR NEW GIRLFRIEND, IT&apos;S TIME TO LEARN HOW TO MAKE LOVE TO HER! BY WHICH, I MEAN KILL FUCKERS! THIS RIFLE IS THE NUMBER ONE CHOICE OF THE WORLDS FINEST QUICKSCOPERS AND TRICK SHOTTERS, WHICH ONE DAY, IF YOU DON&apos;T  HARDSCOPE YOUR WAY INTO THE FUCKING FRENCH ARMY, YOU WILL BE! THIS RIFLE IS FOR AGGRESSIVE SNIPING, AND THE MOST IMPORTANT ABILITY OF ANY SNIPER IN 2013 IS TO 360! NOW JUST IN CASE YOU SORRY SACKS OF SHIT FAILED GEOMETRY, THAT&apos;S A FULL CIRCLE! REMEMBER THAT, A FULL GOD DAMN CIRCLE, IF I SEE ANY OF YOU PULL A 180 OR A 240, I WILL PERSONALLY SHOVE THIS .408 ROUND UP YOUR FAT ASS!  NOW THESE ARE THE RULES OF 360&apos;N  RULE NUMBER 1. YOU START FROM AN ELEVATED POSITION AND JUMP, ALWAYS! DO YOU THINK ANYONE WANTS TO SEE YOU SPIN ON THE GROUND LIKE A FUCKING BALLERINA? YOUR DAD STILL CRIES AT NIGHT FROM THE TIME HE CAUGHT YOU CROSS DRESSING IN YOUR SISTERS PANTIES WHEN YOU WERE 8! DON&apos;T MAKE HIM GET A DAMN VASECTOMY!   RULE NUMBER 2: SCOPES ARE FOR FUCKING QUEERS WHO SIT IN BUSHES! THE ONLY USE YOU FAIRIES HAVE FOR THE SCOPE ON THIS RIFLE IS ANAL STIMULATION IN CASE YOU GET LONELY FOR YOUR BOYFRIEND BACK HOME! OTHERWISE, I DON&apos;T WANT YOU TO EVEN THINK ABOUT USING IT! IT&apos;S **NO SCOPE OR GET THE FUCK OUT** OF MY CORP!   RULE NUMBER THREE: THAT&apos;S THE NUMBER AFTER TWO, BY THE WAY! IF I EVER, *EVER*, CATCH YOU USING STEADY AIM, I WILL FUCKING DE-RANK YOUR ASS BACK TO PRIVATE! SO IF YOU WANNA KEEP THOSE PURDY BADGES ON YOUR UNIFORM, AND ALL THE SICK NASTY CAMOs AVAILABLE FOR YOUR NEW GIRLFRIEND, YOU WON&apos;T BE A GOD DAMN NANCY AND USE THAT FUCKING PERK!    </string>
                              <string>We get a lot of hobo spiders here in upstate NY, along the Hudson river. I don&apos;t really care whether or not they consume other unwanted bugs. These fuckers are 2-3 inches long. I&apos;m not gonna sit there and tolerate them crawling all over my fucking apartment.</string>
                              <string>Gays &amp;gt- Douches, faggot!</string>
                              <string>stupid fucking nigger, lol</string>
                              <string>YOU SHUT YOUR WHORE MOUTH!</string>
                              <string>YOU SHUT YOUR WHORE MOUTH.</string>
                              <string>You shut your whore mouth!</string>
                              <string>That guy&apos;s a fucking cunt.</string>
                              <string>Really that&apos;s be an effective law IMHO  You rape someone, once your convicted we burn your ass alive. We throw gasoline on you and give you a light and watch you burn.  Hell no what we DIP YOU INTO GASOLINE and then light you on fire. And we ain&apos;t got any water.</string>
                              <string>I GETS UP. AND THEN I PACK A BLUNT AND SMOKE IT AND DEN I HAVE MY WOMAN COOKS ME BREAKFAST AND DEN I MIGHT FUCKY WITH HER AND DEN I PROBABLY OR MOST LIKELY WILL GO AND ROB A NIGGER OR TWO AND RIDE ON OVER TO THE PAWN SHOP AND GETS WHAT I CAN GETS...  MY DAY VARIES</string>
                              <string>Yay, one less dirty nigger.</string>
                              <string>you look like a fat whore. </string>
                              <string>Spoken from a true nigger. </string>
                              <string>He&apos;s a goofy bastard with a hint of seriousness and don&apos;t give a crap what people think when he does stupid stuff. Which makes me the complete opposite being serious with a bit of goofy, very self conscious, tall and broad shouldered while he is only like 5&apos;8&apos;&apos; and slouches.</string>
                              <string>Man I feel you, when I was little I loved Barry Bonds - I mean he was the home run hitting king! But then as I got older I realised it was only because he was a cheating bastard. Really ruined the way I looked at baseball for a while - still my all time favourite sport though!</string>
                              <string>NO NO NO NO  NO SPORKS TODAY</string>
                              <string>LIVIN IN A LONELY WOOOOOORLD</string>
                              <string>That is what he said, nigger</string>
                              <string>##THIS IS &quot;MURICA YOU CUM GUZZLIN FAGGOT  THE ONLY GOOD COMMIE LIKE YOU IS A DEAD [COMMIE](https://lh3.googleusercontent.com/-OvPhtjQOsTo/UU-62ds6SxI/AAAAAAAAAD4/afnyvIiYqcA/s1000-fcrop64=1,120233fcedfccc02/Meanwhile%252Bin%252BMurica%252B_062f93860376eea9654d7c1a8db8eb13.jpg)##</string>
                              <string>i swear to fucking satan you fucking link me to a fucking nigger song again and make my fucking youtube recommendations all fucking nigger trash sounds ill fucking kill you, once again. dont fucking tag nigger shit without making the tag NIGGERSHITSOUNDS you fucking filthy faggot hood rat</string>
                              <string>downvote me because I&apos;m right</string>
                              <string>Yeaaaaahhh!!! Magnets, bitch!</string>
                              <string>Do you prefer cunt or whore? </string>
                              <string>Nigger racist troll is a dick</string>
                              <string>So the guy that I watched hack while I was playing by shooting guys in the head one by one through the double doors on Aztec was just &quot;Good&quot;.  Or the guy on TFC that I used to watch run around faster then anyone in the server while shooting everyone in the head was bad?  Nah bruv ur a cunt m8.</string>
                              <string>SPORK MOTHAFUCKA! CHECK YOSELF</string>
                              <string>ACC! ACC! Acc! acc. acc... acc?</string>
                              <string>Rengar.. FUUUUUUUUUUUUUUUUUUUU-</string>
                              <string>Thats awesome I&apos;m glad you showed that punk whats up.  Its supposed be a gentleman&apos;s game.  And fuck him for talking smack about your pre shot routine.  The range is for practice&quot; so yes, weat your golf shoes and practice your routine to hit targets.  Dont just go out and pound the ball aimlessly.  Well done sir&quot;</string>
                              <string>KEEP THEM COMING CHEMISTRY PONY!</string>
                              <string>SUPER THUNDER POUT **ACTIVATE!**</string>
                              <string>OMG THE AWWWWWWWWWWWWWWWWWWWWWWS</string>
                              <string>I&apos;M TOO HUNGRY FOR THAT BULLSHIT</string>
                              <string>Enjoy the karma you Crack whore.</string>
                              <string>Umm, you&apos;re the fucking asshole.</string>
                              <string>Just know that healthy relationships are out there. Know your boundaries and stick to it. Keep a distance from him and when you do move out you can tell him to go fuck himself. I did this and I still visit with my grandmother with out seeing old man.  You make your own happiness so don&apos;t let this toxic man poison you. </string>
                              <string>NOPE.jpg  NOPE.avi  NOPE NOPE NOPE</string>
                              <string>GOB&apos;S PROGRAM  PENUS  PENUS  PENUS</string>
                              <string>FUCKING MAGNETS, HOW DO THEY WORK?</string>
                              <string>FUCK FUCKIN FUCK FUCK FUCKING FUCK</string>
                              <string>Who would want a devils threesome?</string>
                              <string>You american hating commie bastard</string>
                              <string>THAT IS NOT FUNNY AT ALL ASSHOLE. </string>
                              <string>Sort of like this: 01001101 01101111 01110010 01100101 00100000 01101100 01101001 01101011 01100101 00100000 01110111 01101111 01110010 01101100 01100100 00100000 01110111 01100001 01110010 00100000 01000110 01010101 01001110 00101100 00100000 01100001 01101101 00100000 01001001 00100000 01110010 01101001 01100111 01101000 01110100 00111111</string>
                              <string>wow                                                     flying sauce              alein tok my sister                            wow                               muldre an skulli              wow        wow                        such trauma                                                          many doge probe              i bring u luv</string>
                              <string>IT&apos;S JIM CUNTING WHITE. HERE WE GO.</string>
                              <string>Please do go somewhere else faggot.</string>
                              <string>you&apos;re  sister is fucking  insane!</string>
                              <string>Wally West got screwed pretty hard.</string>
                              <string>WHAT?! NIGEL FARAGE?! BUT ISN&apos;T HE THE BAD GUY!? OH GOD DOWNVOTE! LABOUR TOLD ME HE&apos;S BAD SO HE MUST BE HITLER HURRRRR.  How can anyone possibly downvote this? Even if, for some *insane* reason, you&apos;re against UKIP, how can you be pro Syrian intervention? Excuse my French, but how fucking stupid can you possibly be you absolute fucking piece of shit.</string>
                              <string>You are a cunt. An egocentric stupid cunt. You like to imagine 15 year old girls stripping for you? Cuntish behavior. I am a cuntiologist and study many and all cunts. You sir a special kind of cunt- an new species of cunt infact!  The autism levels are skyrocketing. Fuck you op incase you didn&apos;t the the message kill yourself you&apos;re a worthless shitbag.</string>
                              <string>**GREAT NOW A SINGLE TIES IT. FUCK**</string>
                              <string>3 months and 11 days? you&apos;re a cunt </string>
                              <string>This doesn&apos;t technically pertain to the right criteria, however, if you laugh while you are criticizing me or indirectly insulting me, fuck you. My mom always does this and it hurts my feelings really bad. She always scoffs when she is calling me lazy and shit. No matter how lighthearted she tries to make vindictive bullshit sound, the scoff just hits me hard. </string>
                              <string>Oooohhh let&apos;s see shall we?  1. The friend that was supposed to be gay she had sex with. 2. Fucked some guy drunk at a party while I was watching our baby. 3. Left me for my best friend of 7 years...(worst year of my life) 4. got engaged to him after 4 months of dating...jesus 5. Find new girlfriend and peace...ex stirs things up and says I hit her before. (I didn&apos;t)</string>
                              <string>#HAPPY MOTHER FUCKIN CAKE DAY NIGGER#</string>
                              <string>EvErYoNe NeEdS tO cAlM tHe FuCk DoWn.</string>
                              <string>KILL BEFORE WE LOOK WEAK!  KILL NOW!!</string>
                              <string>Maybe he wants him to have a big dick</string>
                              <string>The &quot;K-ONS BACK NIGGERS&quot; alt text?</string>
                              <string>and you get a downvote from me nigger.</string>
                              <string>SHUT UP FOREVER AND STOP RUINING EZRIA.</string>
                              <string>DING DING DING DING DING DING DING DING</string>
                              <string>[](/trixchargin) Oh that is such bull shit. Welcome to the south. Where every one in politics is a religious bigot that wants to keep people from being fucking happy. Denying Gays and Lesbians the right to marry is denying them civil rights and it is BULL SHIT and unconstitutional. Separation of god damn church and state and...and...ARGHH!!!! FUCK THE SOUTH. ^^^I ^^^hate ^^^it ^^^here...</string>
                              <string>Cunt</string>
                              <string>cunt</string>
                              <string>&amp;gt;You have never said a pro sucks dick when they fuck up?  Maybe I have, maybe I haven&apos;t, I am not a professional player so it&apos;s irrelevant.   &amp;gt; It was in the privacy of his own home and he wasn&apos;t trying to start shit. Talk about grasping at straws.  It was not. It was in a gaming house with at least three cameras on Regi, probably up to 8 cameras in the room total. If it was in private none of us would be talking about it because we wouldn&apos;t know. I&apos;m not grasping at straws, Regi talked shit and I can&apos;t wait to see him get slammed at worlds.</string>
                              <string>&amp;gt; - JUST S,TARTED DATING NEW GUY &amp;gt; - BE,TTER LE.T HIM KNOW YOU AREN&apos;T PREGNANT *^^These ^^captions ^^aren&apos;t ^^guaranteed ^^to ^^be ^^correct*</string>
                              <string>&amp;gt;He never responded but I texted him at least 100 messages over two weeks. Calling him a liar, an alcoholic, a monkey. Calling his new girlfriend a monkey and a slutty rake with a loose vagina. Calling him a traitor. Asking him repeatedly if he was ashamed of himself.  You&apos;re a nut job.  I have a horror story though. My first love when I was 17-19, moved in together when we were 19 in a summer resort where we both worked. Found out he cheated on me with a stripper at his friends 21st, we broke up and I kicked him out but my poor little teenage heart was broken for the first time so I started NC which was hard because we saw each other every day at work. Broke NC when we texted me obsessively (I thought it was cute at the time, dafuq?) and asked for me back. Took him back and he stayed the night, while he was in the shower the next morning he got a phone call from Vanessa&quot; which I answered and apparently they&apos;d been dating ever since he cheated on me with her, including him taking her to my favourite restaurant. After we hung up, she text him saying it was over and I broke up with him(again) which was apparently too much for him as he then proceeded to strangle me to unconsciousness. Quit my job the next day out of shame and moved back into Mums.   2 years later he was sentenced to (mandatory) 15 years for murder when his girlfriend at the time tried to break up with him. Could&apos;ve been me.&quot;</string>
                              <string>&amp;gt; The USA is the only country who demands taxes from their &apos;citizens&apos; that haven&apos;t even been in their country  So tell them you&apos;re unemployed. How the hell are they going to find out your foreign wages? Domestic companies have to report income to the IRS, but do those outside the U.S.? I&apos;d be surprised. If I were, say, a Paris-based company and the IRS came to me saying You need to report income to us&quot;, I&apos;d tell them to fuck off.&quot;</string>
                              <string>&amp;gt; While changing one&apos;s weight is not an easy task and may have medical issues that prevent traditional weight loss methods from working, it is still possible.  No, you are forcing someone to change who they are in order to fit in to society so they aren&apos;t harassed with slurs like lard-ass&quot;.  If I don&apos;t want to be called a &quot;lard-ass&quot; I should lose weight.  But if I tell someone that if they don&apos;t want to be called &quot;faggot&quot; they should act like a straight person, you&apos;d probably find that offensive.  Go fuck yourself.  I know exactly what it feels like to be a second-class citizen.  &quot;</string>
                              <string>&amp;gt; I have NO idea about what I can do. Alternatively, I&apos;m doing&quot; everything I perceive, but I don&apos;t know how. A mystery that can be misinterpretated as concrete reality is emanating sponteanously out of a mystery that can&apos;t be interpreted at all. And, I am that mystery.  You&apos;ve dodged my question.  &amp;gt</string>
                              <string>P A R E N T I N G  D O N E  R I G H T!!!</string>
                              <string>The scale is a lying bitch. Just saying.</string>
                              <string>ILLEGAL IMMA GO LAWFUL GOOD ON YO ASS  /s</string>
                              <string>you shit eating cunt i upvoted you anyway</string>
                              <string>Holy self-righteous masturbation batman. </string>
                              <string>What exactly does my age have to do with facts? The people who do this are far more employable than you, me, or any average schmuck off the street. The tiger chick &quot;katzen&quot; charges $200+ an hour for her tattoo work, when&apos;s the last time *you* made $200 an hour? Just because you&apos;re a dumb cunt with a shitty opinion of people who&apos;ve done far more than you have, doesn&apos;t make you right. Fuckin retard, grow up.</string>
                              <string>Awww! You&apos;re so funny, you gigantic cunt. </string>
                              <string>THERE SEEMS TO BE A SAND NIGGER IN THE ROOM</string>
                              <string>DAE APPLE LITERALLY HITLER? ANDROID 4 LIEF!</string>
                              <string>pls respond LOLOLO SO ORIGINAL RITE GUISE?!</string>
                              <string>I DON&apos;T SEE YOU HERE, SCAREDY-CUNT! Ã¢â&#x84;¢Â¥</string>
                              <string>Shut up faggot Breaking Bad can do no wrong</string>
                              <string>Yellow sac spiders can suck. They can possibly cause that same necrotic condition than Recluses can, just not as often because they aren&apos;t nearly as poisonous.   Still. They&apos;re freaky little bastards. If they or their web is disturbed they drop to whatever surface is under them and take off, either to attack or escape. It makes them tough to catch (if you&apos;re like me and hate killing spiders. I catch them and toss them outside)</string>
                              <string>OH I&apos;M MORE INTERESTING THAN A WET PUSSY CAT</string>
                              <string>SORRY, BUT YOU DON&apos;T DESERVE TO PLAY FINALS!</string>
                              <string>MINUS? WHAT THE SHIT KIND OF NIGGER ARE YOU?</string>
                              <string>Well, on one hand, I feel bad for your cockblock... but on the other hand, I sympathize with ditched guy. The girls didn&apos;t think that through, they needed a distraction girl. The fat girl that is gonna be a virgin until she moves away for college then becomes a total slut girl. I&apos;m sure ditched goody goody guy would have loved a blow job too - it sucks being the ditched guy. You have a half-cool memory, ditched guy just feels left out :(</string>
                              <string>Nebraska @ Baylor in...2001? It was awhile back. I was in the bathroom, and the urinal is essentially one giant tub that everyone pisses into so you&apos;re usually to close to whoever is next to you. A Nebraska fan is relieving himself and a Baylor fan walks up to him and yells Cornfucker Cornfucker!&quot; about 2 inches from his face. The Nebraska fan proceeds to turn and piss on the Baylor fan and the two start fighting. Highlight of the game.&quot;</string>
                              <string>i feel like i agree with every break up reason except for cheating. if she was a bitch, or mean, or crazy, or whatever just say things didnt work out. otherwise you look bad when you just trash talk an ex. besides everything is opinion based and theres always 2 sides to a story, so it didnt work out is usually the best way to go.  but with cheating its such a clear cut factual thing that happened, and its easy to just say she cheated, its over.</string>
                              <string>Upvote for vascularity...and that huge dick. </string>
                              <string>**EMPTY IT ALL INTO A SINGLE RUBBERMAID BIN.**</string>
                              <string>My DICK is good at analyzing bad advice.  BAM!</string>
                              <string>^(S^u)^(p)e^(r^s)^(c)r^(i^p)^(t) i^s ^(^f)^(u)n</string>
                              <string>^^^^FUCK ^^^YOU ^^NIGGER ^WIDE EYES BRIGHT CUNT</string>
                              <string>A^A^A^A^A^A^A^A^A^A^A^A^A^A^A^A^A^A^A^A^A^A^A^A</string>
                              <string>He&apos;s a judas cunt and a glorious piece of shit.</string>
                              <string>5k budget = COCAINE.  10k budget = MORE COCAINE.</string>
                              <string>You&apos;re just a jealous cunt and you&apos;re pathetic. </string>
                              <string>Give me a source or shut your dirty whore mouth.</string>
                              <string>Niggers are the greatest enigma of planet earth.</string>
                              <string>Go suck Cristiano Ronaldo&apos;s dick you sad faggot.</string>
                              <string>This is such a dick move stop doing this please </string>
                              <string>#IDONTKNOWWHATWEREHASHTAGGINGABOUT   #LOUDNOISES</string>
                              <string>But he&apos;s still a cunt. And good at being a cunt. </string>
                              <string>YOU SAID YOUD GET ME A DRINK AND YOU LIIIIIIED!!!</string>
                              <string>PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC •PC﻿ • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC • PC •</string>
                              <string>Whore</string>
                              <string>LAMAO</string>
                              <string>HEY GUYZ LUKE ME LEIK PERKIMANZ U WUV MYE NAW RIT?</string>
                              <string>why have i NEVER HEARD OF SUCH A BEAUTIFUL THING?  </string>
                              <string>YO THAT SPENCER GRAPPLE WHILE HE WAS CRUMPLED HRNGGH</string>
                              <string>Riiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiisp</string>
                              <string>WHAT AN INTERCEPTION!!!   DERRON SMITH!!!!   MY MAN!</string>
                              <string>OI FUCK YOU ARSENAL CUNT. GO AWAY YOU GOONING WANKER</string>
                              <string>You shouldn&apos;t do it because litterers are faggots.  </string>
                              <string>IF MY CAT DID THAT, IT WOULD BE HIS LAST DAY ON EARTH</string>
                              <string>SPECULATED PORNOGRAPHY OF CHOICE: Pornbot films. </string>
                              <string>Why do faggots like you make fun of CoD for no reason</string>
                              <string>ur such a faggot u got carried by tryndamere stfu lol</string>
                              <string>Because when all else fails.... Crowbar that fucker. </string>
                              <string>░░░░░░░░░▄░░░░░░░░░░░░░░▄ ░░░░░░░░▌▒█░░░░░░░░░░░▄▀▒▌ ░░░░░░░░▌▒▒█░░░░░░░░▄▀▒▒▒▐ ░░░░░░░▐▄▀▒▒▀▀▀▀▄▄▄▀▒▒▒▒▒▐ ░░░░░▄▄▀▒░▒▒▒▒▒▒▒▒▒█▒▒▄█▒▐ ░░░▄▀▒▒▒░░░▒▒▒░░░▒▒▒▀██▀▒▌ ░░▐▒▒▒▄▄▒▒▒▒░░░▒▒▒▒▒▒▒▀▄▒▒▌ ░░▌░░▌█▀▒▒▒▒▒▄▀█▄▒▒▒▒▒▒▒█▒▐ ░▐░░░▒▒▒▒▒▒▒▒▌██▀▒▒░░░▒▒▒▀▄▌ ░▌░▒▄██▄▒▒▒▒▒▒▒▒▒░░░░░░▒▒▒▒▌ ▀▒▀▐▄█▄█▌▄░▀▒▒░░░░░░░░░░▒▒▒▐ ▐▒▒▐▀▐▀▒░▄▄▒▄▒▒▒▒▒▒░▒░▒░▒▒▒▒▌ ▐▒▒▒▀▀▄▄▒▒▒▄▒▒▒▒▒▒▒▒░▒░▒░▒▒▐ ░▌▒▒▒▒▒▒▀▀▀▒▒▒▒▒▒░▒░▒░▒░▒▒▒▌ ░▐▒▒▒▒▒▒▒▒▒▒▒▒▒▒░▒░▒░▒▒▄▒▒▐ ░░▀▄▒▒▒▒▒▒▒▒▒▒▒░▒░▒░▒▄▒▒▒▒▌ ░░░░▀▄▒▒▒▒▒▒▒▒▒▒▄▄▄▀▒▒▒▒▄▀ ░░░░░░▀▄▄▄▄▄▄▀▀▀▒▒▒▒▒▄▄▀ ░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▀▀ wow so brutal</string>
                              <string>WWWHHHHEEEEAAAATTTTTOOONNN!!! ...and felicia day!!!!!!</string>
                              <string>SHE TOOK THE MIDNIGHT TRAIN GOIN AAAA NEEEEE WHEEEERE </string>
                              <string>Fuck well there goes my attempt :P i started after him</string>
                              <string>1.  did you fuck her? 2. have you fucked anyone since?</string>
                              <string>HOLY SHIT WHAT A THROW I DIDN&apos;T KNOW NOVA COULD DO THAT</string>
                              <string>I wish he brushes his teeth regularly. Filthy scumbag. </string>
                              <string>No. Because I desire to fuck attractive female humans. </string>
                              <string>DO IT, OR I&apos;LL POUR IT ON THE FLOOR AND BLAME IT ON YOU!</string>
                              <string>Well I think more highly of you. Fuck those sand niggers</string>
                              <string>OMG ANIMAL MASKS HOTLINE MIAMI INVENTED THAT GET SUED BRO</string>
                              <string>&apos;*ELLO LUV! I ON ME BLOODY RAG, JESS TOSS IT EN ME DUMPUH*</string>
                              <string>What the fuck? Bayern got their ratings fucked in the ass.</string>
                              <string>you dont need a credit card, you&apos;ve already fucked up once.</string>
                              <string>HEY TITS LETS SEE WHICH QB CAN OVERTHROW HIS RECEIVER WORSE</string>
                              <string>You back in the days when you were a cunt?  Yeah, probably.</string>
                              <string>Yep. It hurts pretty fucking bad to get hit in your crotch.</string>
                              <string>Nigger</string>
                              <string>nigger</string>
                              <string>Whore!</string>
                              <string>Faggot</string>
                              <string>Bitch!</string>
                              <string>BABOU!</string>
                              <string>HOLY FUCKING SHIT! PUT HER IN GYMNASTICS SEE HIW SHE DOES XD</string>
                              <string>IT DEPENDS ON THE PERSON. POP RADIO OR NPR IS PROBABLY SAFEST</string>
                              <string>OMFG I KIN MAK AL DE KEDZ GIT REKT NW THT I NO HW 2 PEE VEE PEE</string>
                              <string>I AM AN AMERICAN HEAR ME ROAR WITH IGNORANCE!!! RAAAAAHHHHH!!!!</string>
                              <string>Raven needs to give this clowns lessons on how to play a psychic</string>
                              <string>Ask your doctor and stop medicating yourself, you fucking dumbass</string>
                              <string>Thanks my nigger. We really out here. Shut up ^x15^strawberry Milk</string>
                              <string>&amp;gt- i.imgur.com/V4xESNu.gif  You, sir, are a faggot and a retard.</string>
                              <string>I should also mention that at the end of each semester, ask what they do with lost and found. At my school we throw it out because in order to donate it somewhere it needs to be sorted, washed, and driven to said location with an assortment of paperwork. I now own several name brand hoodies (Columbia, Under Armour, Victoria Secret Pink etc) and I found this really awesome Doctor Who water bottle. Boil and sterilize everything and its brand new.   I make decent money, but I will never throw away my money at businesses like these, but free is always good. I&apos;ll also sell to consignment shops like Plato&apos;s Closet and get some cash in hand.   Frugal mother fuckers.</string>
                              <string>I&apos;D SAY THAT IS A PRETTY MAJOR SCORE - A MAJOR WIN - FOR ALL PARTIES</string>
                              <string>I think it&apos;s busy being a daddy-long bitch to consider that thought.</string>
                              <string>HUH. I DIDN&apos;T KNOW HIRDY WAS USING THE MEDIA AS A LEGAL DEFENCE. TIL.</string>
                              <string>CALM THE FUCK DOWN YOU SILLY CUNT AND DON&apos;T READ IT THEN. with love x</string>
                              <string>OP is being intentionally thick here. He&apos;s either an idiot or a troll</string>
                              <string>NIGGERS</string>
                              <string>Niggers</string>
                              <string>HEY TOLEDO I HOPE WHATEVER YOU DO WITH THE $800,000 WE GAVE YOU SUCKS. </string>
                              <string>Viva this account, you motherfucker.  You almost have a million karmas.</string>
                              <string>Skydoesminecraft fans.  DAE BUDDER!?!11!!  I AM NOW BUDDER MAN!!! XD XD </string>
                              <string>Commence upvote? Sorry, all I can do is commence downvote. Fuck you, OP.</string>
                              <string>If there&apos;s one thing I learned while watching anime, it&apos;s this:  There are three different types of anime watchers/fans.  1. The weeaboo narutard. Essentially everything that makes you hate anime fans. Fuck these guys.  2. Hardcore anime otaku. The other end of the spectrum. Generally bearable, but they&apos;ll nerd out over anime stuff. Basically like any other nerd/geek, but with anime. If you are used to being around nerds/geeks, these guys are usually fine.  3. Regular viewers. Basically anyone who watches anime casually or doesn&apos;t fit into one of the first two categories. I know lots of people who didn&apos;t know much about anime and are now in this category. Normal people + anime.  I&apos;d say check out: Death Note, Mirai Nikki, Steins;Gate, and Time of Eve. This&apos;ll get you started if you really want to check it out. Not all of it is the negative-stereotype stuff.</string>
                              <string>#NIGGER FAGGOT JEW SHITHEAD. ALL FAGGOTS ARE SINFUL FUCKTARDS WHO SUCK DICK</string>
                              <string>What if I told you, as a fat ass american, I lost 60lbs this year (225 &amp;gt;&amp;gt; 165) doing nothing but cutting out HFCS (fructose glucose for our UK friends signing in).  Gorge away, just be cognizant.  This meal sounds delicious.  I learned this simple shit over 2 weeks while in London,  No one is fat, no one drinks soda.  It&apos;s not irony.  It&apos;s lies.</string>
                              <string>Hahaha. God look at his flair too. What a total cunt. (the troll, not NuKeX)</string>
                              <string>I&apos;m old old enough that I don&apos;t give a shit about civility.  I may have had a couple when I replied.  But truth be told at my age, I&apos;m sick of this bullshit that we as men have to keep our emotions bottled up inside.  It&apos;s not fucking healthy.  And it doesn&apos;t make us weak to express how we feel.  All it does is make us one step to blowing our brains out.  So fucking what, we express our emotions.  If you really believe that a partner is going to disrespect you for that, then you shouldn&apos;t be with her to begin with (assuming you&apos;re a straight male).  It&apos;s time as men that we claim that we are emotion beings as much as any one and deal with the fallout.  The only thing stoicism will give you is ulcers and cold sores.  Fuck off and get over your bad self.</string>
                              <string>Wenger... Wenger... Wenger... WENGEEEEEEEEERRRRRRRR!!!!  What?  DANGER ZONE!!!</string>
                              <string>hahahaha HaHaHaHa HAHAHAHA  HA!HA!HA!HA! Down vote for making me laugh so hard</string>
                              <string>Because your an asshole, and you don&apos;t like dogs.  Also, whats wrong with you.</string>
                              <string>lolololololololololololololololololololololololoololololololoolololololoolololoololololololoololololololololololololololololololololololololololololololoololololololoolololololoolololoololololololoololololololololololololololololololololololololololololololoololololololoolololololoolololoololololololoololololololololololololololololololololololololololololololoololololololoolololololoolololoololololololoololololololololololololololololololololololololololololololoololololololoolololololoolololoololololololoololololololololololololololololololololololololololololololoololololololoolololololoolololoololololololoololololololololololololololololololololololololololololololoololololololoolololololoolololoololololololoololololololo......sorry obama, cameron can&apos;t come out and play.</string>
                              <string>Bastard.</string>
                              <string>OMG ALMOST10 WRECKS?  AND YOUR SOME KIND OF CAREER DRIVER?  WOW.  no.  move over. </string>
                              <string>OHHHHHH MY GOD WHAT THE FUUUCK! YOU STUPID CUNTS FAIL AT THESE HEISTS. 420YOLOSWAG!</string>
                              <string>THIS MOTHERFUCKER STOLE AN AK AND IS TRYING TO DISGUISE IT. NIGGER THAT AINT A BIKE </string>
                              <string>now we need someone to make it say something like, deal with it or, fuck the police.</string>
                              <string>Yo mama is such a ghetto slut, she has blown more black guys than hurricane Katrina. </string>
                              <string>THERE&apos;S NO MORE HYPE ANY MORE CAN I HAVE A BETA KEY PLEASE FUCK YOU NO REALLY PLEAES?</string>
                              <string>I WISH WE COULD PLAY THE BILLS 8TH STRING DEFENSE EVERY WEEK. ALSO LEINART LOLOLOLOLOL</string>
                              <string>take me 2 ur leaduhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhollllllllllllllllllllllllll</string>
                              <string>Yeah those three in Western Canada are a bitch. I&apos;ve never managed to figure them out.</string>
                              <string>Actually, all of the damage was from me fucking your fat whore of a mother on top of it</string>
                              <string>Agreed so much. I think that the further removed these things are, it becomes something &apos;okay&apos; to joke about. And it&apos;s not okay.   I&apos;m older than the average redditor, and I will never forget the all consuming fury I felt (still feel) at the movie Titanic.  How dare Hollywood take this horrible, horrible tragedy to make money out of a love story... They could have made the same damn story and called it something else. Fuck those guys! There were still Titanic survivors alive at that time, how do you think they felt at their trauma turned into a blockbuster?   Same fury at Hitler jokes, and I call them out when I hear them.  Thankfully not often.  Just because the people directly involved are mostly dead now does NOT make it okay to turn their pain and anguish into your goddamn punch line!  How will those people feel when they are old and hear 9/11 jokes, I wonder? </string>
                              <string>CAN&apos;T ANYONE SEE THAT TIME IS AGAINST THIS MAN?! GIVE HIM HIS FUCKING CROISSANTS, STAT! </string>
                              <string>Idiot... </string>
                              <string>I WOULD UPVOTE YOU FOR CORRECTNESS, BUT THE BUTTON DOESN&apos;T SEEM TO BE WORKING.  NO IDEA WHY.</string>
                              <string>TYPICAL SYDNEY, THROWS MILLIONS AT A PLAYER AND DOESN&apos;T CARE TO LEARN HOW TO SPELL HIS NAME.</string>
                              <string>I GUESS THEY DON&apos;T TEACH YOU HOW TO USE YOUR MOUTH AT THE SAME TIME AS YOUR HANDS IN CHICAGO.</string>
                              <string>EEEEEXXXXXXPPPPPPPPPLLLLLLLLOOOOOOOOOOOOOOSSSSSIIIIOOOONNNNNNNNNNSSSSSSSSSSS!!!! F**K YEAH!!!!</string>
                              <string>I&apos;ll steer the luxury cruise ship that is your shlong into the glorious harbor of masturbation.</string>
                              <string>You... Bastard...  You guys get all the hot sorority girls! Give us the bicurious ones at least!</string>
                              <string>LIKE, OMIGAWD, I&apos;M OUT OF LIKE, PRINGLES. THIS IS LIKE, AFRICA #nofood #hunger #nobodyunderstands</string>
                              <string>Thank you!</string>
                              <string>walter jr?</string>
                              <string>Damn sexy.</string>
                              <string>*slashes both swords across the yetis stomach, turns and jogs toward you holding my side again* Tyler!</string>
                              <string>Aw, man.  I not only drop $20 on scallops, I drop it on ~frozen~ seafood. I need to move to the coast. </string>
                              <string>**NPC**  ---  *The badger walks away, not caring about a silly human, but near by he finds three herbs*</string>
                              <string>and [mint glazed baby carrots](http://www.food.com/recipe/minted-glazed-baby-carrots-506239), obviously!</string>
                              <string>Bots crawl this subreddit for steam keys. Instantly steal them before humans can. Clever, yet fucked up.</string>
                              <string>I lost it at &quot;Good for you lad.&quot;   This was fun to read! I hope if you go again that you do another.</string>
                              <string>My only account left in NA is lvl 14 /: I transferred recently to play with friends (and with better ping).</string>
                              <string>Also in the Time Machine episode why doesn&apos;t Fry get out of the time machine when they get to the year 2000</string>
                              <string>I find bringing a bottle of disinfecting spray to the toilet weirder then putting a layer of TP on the seat.</string>
                              <string>I honestly disagree, I think around 15/16 a good majority of teens are mature enough to be considered adults.</string>
                              <string>Same here! </string>
                              <string>Shit title.</string>
                              <string>It is a law in some (read: too many) states. However, the thought of those sham clinics scares and enrages me.</string>
                              <string>Yikes, this is all wrong. I&apos;d recommend something more like.. 1. Finish your L50 Job Quest. 2. Finish your Story by doing Castrum Meridianum and the Praetorium. 3. Unlock Amdapor Keep and Ifrit Hard. 4. If your weapon is particularly bad (ie. sub-level 40), get the highest level GC weapon you can (42? 44? I forget!) 5. Wanderer&apos;s Palace! Lots of ilvl 55 gear here, including Darklight weapons, and 50 Philosophy stones a run. A speedrun strategy can get this to sub-20 minutes even with fresh 50 groups. 6. Once you&apos;re comfortable, powerbomb Ifrit Hard for a weapon. 7. Unlock Garuda Hard from this while you&apos;re at it. 8. Amdapor Keep! There are a lot of words already said about this place - just keep your head together and keep at it. It gets easier as you gear up. 9. Progress your Relic Quest up to HM Primals once you&apos;re mostly fully geared from Amdapor. Hydra don&apos;t play around, not to mention Garuda and Titan. 10. Garuda Hard. Yay! 11. Titan Hard. Also yay. 12. Relic! Coil! More yay! Honestly if you can beat Titan you don&apos;t even need this kind of advice anymore, what are you reading this for.</string>
                              <string>[archetypal projection](http://upload.wikimedia.org/wikipedia/commons/thumb/7/72/OHP-sch.JPG/300px-OHP-sch.JPG)</string>
                              <string>Very interesting. I never even realized the ash statues were able to be spread around the island via smuggling. </string>
                              <string>yo estaba en casa porque era el ddia del maestro y no habia clases, y lo vi en las noticias pero no entendi mucho</string>
                              <string>probably not</string>
                              <string>Unless Season 8 does something jaw-dropping in the last two eps, Season 8 will 100% go down as the worst season, no doubt.</string>
                              <string>They&apos;re just innocent spoilers that build hype! Besides, it&apos;s not like you&apos;ll remember all of them when you&apos;re playing! :P</string>
                              <string>My small company&apos;s servers don&apos;t run antivirus software. My home computers do not. They run Windows Server 2012 and 7, respectively.  Tell me, why should a server have AV software if it is not a file server? Why should every single byte read or written be scanned by a process, effectively making the performance tank and completely negate the performance of any SSDs?  Antivirus software IS NOT a replacement for proper security measures:   - Never run any software on any server that has not been verified.   - Monitor all connections in and out of any servers   - Lock down all processes, ports, services that are not required.   - If possible, explicitly block every IP except the ones needing access.   - Never run any non-verified software.   - The web browser on a server is nothing more than a glorified file download app.  For a file server (and preferably backup storage as well), you need to have antivirus software, regardless of the operating system of the server or any clients accessing it (even if you&apos;re 100% Linux users - you never know when you&apos;ll need to plug a Windows laptop into the network). For any other purpose, antivirus software on a properly managed server is outright detrimental to its intended usage.</string>
                              <string>After we got on the same page we&apos;ve been fighting side by side for a long time. God bless America and God bless Great Britain.</string>
                              <string>The fluency question is bit different depending on country.  In some places (Asia in particular), it is actually preferred that you don&apos;t know the language.  In others, although English fluency is the primary requirement, it is also kind of expected that you know the local language.  This seems to be the case in many Spanish speaking countries, especially since Spanish is a pretty widely spoken language.  Once again, generally not required per se, but often expected.  I think your level of Spanish would probably be just fine, and you would pick it up quickly once you were there.  Usually the boyfriend is an issue.  If he has a job and can come, great.  If he can find another job, great.  But as for teaching, that can be difficult.  There are some programs that will place couples together on teaching assigments, but a lot won&apos;t do it--they don&apos;t want to risk losing 2 teachers at once if the relationship goes south.  So boyfriend coming along can be complicated.    As to living situations, as the other poster mentioned, it really depends.  If oyu go through a program, yes, they do usually help find you a place to live.  If you just apply to a school directly, you are often on your own.  Depends on which route you take.    Good luck, and I hope it all works out for you!</string>
                              <string>It has some good episodes, especially in the second season. But it&apos;s also got some really stupid episodes. Very hit or miss IMHO.</string>
                              <string>Thank you! :D</string>
                              <string>Who doesn&apos;t? </string>
                              <string>[Here&apos;s the original post.](http://www.reddit.com/r/whatsthisplant/comments/1lhe5a/this_plant_with_long_thin_leaves_is_growing_near/)</string>
                              <string>Okay, awesome, going to eat this every day. But not today, because the name made me fetch in a way I haven&apos;t since two girls one cup.</string>
                              <string>It sounds like Gamestop is giving out an extra gold pack a week as it&apos;s promotion ASIDE from the EA Season Ticket. So it might be 2. </string>
                              <string>Only 2 fighters that come to mind when I hear the word &quot;Shook&quot; , Muhammad Ali and Danny Garcia, both for totally different reasons.</string>
                              <string>Sure, you could make the argument that we wouldn&apos;t have gone to Iraq either considering the UN was a big factor in that.  I don&apos;t buy this at all. I mean, the US seemed especially willing to go into Syria before the Russian diplomatic effort to utilize the UN to clear chemical weapons from Syria. US intelligence reports differed from UN reports and President Obama didn&apos;t have a willingness to send US intelligence to the UN so that they would have a say in the US military intervention into Syria.  It&apos;s pretty obvious that the US would have gone into Syria, without any approval from the UN. The diplomatic effort vis-a-vis Russia was introduced in a forum outside of the UN and it&apos;s not as if their security counsel actually diplomatically negotiated anything with the US.  Furthermore, I don&apos;t think that the US Congress would have voted to go into Syria and if they had, it wouldn&apos;t have been a legitimate representation of the American public&apos;s will. If anything the UN could have been used to justify Syrian intervention had the circumstance existed in which their intelligence could have been used as justification for military intervention, like it did in Iraq.  Obviously, &quot;might&quot; is the operative word in the title of this article. I just don&apos;t think its a logical argument. I think it&apos;s just some liberal slander against Dr. Paul, libertarianism and non-interventionism.</string>
                              <string>I took on above an [overpass](http://i.imgur.com/zKRWt5O.jpg), there was at least 3 other cars stopped just for taking the photo there too.</string>
                              <string>I just snorted</string>
                              <string>I first read the thing the girl says as &quot;Yes, I win.&quot; My next thought was that it was the boy who should be saying that if she said yes.</string>
                              <string>Yes. Though I wouldn&apos;t be in favor of like cameras in beds, however removal of it from TVs, ads, public support and the like would be ideal.</string>
                              <string>Why can&apos;t we post Arencibia&apos;s stats? This is the blue jays sub, his stats and anybody elses on the jays can go here no matter how good or bad.</string>
                              <string>Oh, in that case. Wasn&apos;t thinking about Irish Gaelic for some reason. In that case, I agree it would probably not be the easiest to pronounce.</string>
                              <string>Could be the same guy, I have no idea! I&apos;ve never met him myself- she was just telling me this the other day. Glad your uncle is alive and well</string>
                              <string>I really can&apos;t see this going far. Seems like Fluker took the cash separate from the university, and the NCAA is kind of in a tough spot anyway.</string>
                              <string>Wow, the 1990s called, they want their joke back.  How is that &quot;INSANE WOLFFFFF&quot; material?  How about: No means yes and yes means a 1 man 1 jar.</string>
                              <string>Ah, true love. The kind of lover you can tell your significant other that you want to fucking murder them. I hope my relationship is so open one day.</string>
                              <string>Good ole S&apos;Port</string>
                              <string>What up tittays</string>
                              <string>Rats won&apos;t get nearly this high because meth and money aren&apos;t worth as much as weapons and gold, which make up a good portion of the bags in firestarter.</string>
                              <string>I&apos;ve heard people say they&apos;re more unsure of their opinions after education and maybe this has to do with it. The world is complex and they feel helpless.</string>
                              <string>Nope. &quot;Character actor&quot; is used in a couple of different ways. It can mean a &quot;character&quot; type, as in not a leading man/woman, as the above casting director stated. It can also mean an actor who aims to transform into something quite different from their normal persona instead of the role being written to the persona. Harrison Ford will never be a character actor- he is always playing Harrison Ford, because that&apos;s what people want.   &quot;Method&quot; describes a particular school of acting that got a lot of press from the late 50s through mid-70s (though it was founded in the 30s and got its first Broadway exposure then). This ultimately had more to do with the press savvy of its primary guru, Lee Strasberg. Though it was most associated with Marlon Brando&apos;s particular brand of raw, animal talent, Brando did not ever acknowledge learning anything from Strasberg. (In fact, he claimed his training was all from Stella Adler, who consciously split from Strasberg&apos;s &quot;Method&quot; by 1934.)  Strasberg&apos;s work has largely fallen out of favor, and even those who teach in his name mostly teach an amalgam of his and other people&apos;s work. The &quot;Method&quot; term has become an oft-overused descriptor of any type of acting process that someone thinks overwrought or self-indulgent. Even though the term is misapplied in 90% of cases, it is true that these were some qualities of Strasberg&apos;s training that his peers and students came to distrust and disagree with.  Source: Ph.D. Candidate in Theatre specializing in Acting theory and cognitive science.</string>
                              <string>this amazes me. you&apos;re actually asking what to talk about with your friends when you take drugs? you must be the most boring person (people) on the planet.</string>
                              <string>Needs red tears.</string>
                              <string>I want to play  </string>
                              <string>I don&apos;t want to.</string>
                              <string>whats in the box</string>
                              <string>This is a brilliant idea Ogel. It will force cities to limit how many factories they have, causing overpowered cities to turn into Industrial Revolution London.</string>
                              <string>Muslims over the world have been crying this for ages now, and the only reason any hatred that exists in Muslim world is because of it.  No one hates USA itself.</string>
                              <string>Remember the last meet up? It was in sf and i guess this one is too because of the picture (golden gate bridge). It&apos;s on September 25. Which is my birthday too :D</string>
                              <string>I&apos;ve no experience with it. It probably won&apos;t get much love in this sub-reddit. Check out [/r/zeos](http://www.reddit.com/r/zeos)  Start with a 2.0 or 2.1 system.</string>
                              <string>I had to take mine out from my Sabertooth, those 40 mm&apos;s were the only noise-makers in my entire case- they even went through the sound-dampening from my Corsair 550D.</string>
                              <string>Does this polypropylene rope look okay to use? http://www.homedepot.com/p/Everbilt-1-4-in-x-100-ft-Diamond-Braid-Poly-Rope-in-Yellow-Black-17984/202048183#.UjEEy8bkt8E</string>
                              <string>My Jack Russell gets so excited when I come home, she leaps up at me to catch her, and then proceeds to scramble up onto my shoulders and stands there, licking my ear.</string>
                              <string>I&apos;d kill in defense of myself. I&apos;m thinking that&apos;s more human nature than anything. &quot;Flight or fight&quot;. Are there any other religions that have that type of clause?</string>
                              <string>Awww :(  Keep in mind that maybe you&apos;re messaging people who aren&apos;t subscribers.  So they can&apos;t even see who messaged them and maybe they don&apos;t want to pay to read it!</string>
                              <string>Uncritical support for Israel (which spent a lot of time occupying Lebanon a couple of decades back).  This was actually one of Bin Laden&apos;s chief grievances with the US.</string>
                              <string>You made a smart.</string>
                              <string>He has nice buns.</string>
                              <string>[](/rariwhy)I really, *really* hope that when you replay a mission, it includes cutscenes, too. (Completely skippable, of course.)  OR LET US PAUSE THE CUTSCENE, PLEASE. :O</string>
                              <string>Nice to see you -)</string>
                              <string>You aren&apos;t alone. </string>
                              <string>I&apos;m envious that you get to enjoy it all for the first time right now, haha! But regardless - have fun, you are in for a hell of a ride, and prepare for an addiction like no other.</string>
                              <string>He&apos;s a scrappy dude, I&apos;ll give him that, but it was absolutely terrifying having him on the field trying to cover guys.  He was a decent tackler, but he could not stick with anybody.</string>
                              <string>Find a friend on the team and have hin train you on stance, stance, more stance and finally stance.  Then you can move onto other basics. Hone your basics and I gurantee you&apos;ll do well</string>
                              <string>Does the laptop have a discrete graphics card or just integrated?   Also since it&apos;s a laptop nothing is going to be easy to replace, especially since opening it up voids your warranty. </string>
                              <string>I made it all the way through. Mostly to confirm all of it was terrible. At the end they ask, &quot;Do you think you could write a list for us?&quot; Well, not after you set that bar so high.</string>
                              <string>As a healer, yea they suck unless there is adds on the boss.  2k HP over Pallys, yet they get ripped 3x quicker and have no interupts.  Hopefully over time some changes will happen though.</string>
                              <string>Wow I didn&apos;t know that fox show was based of this comic....That being said is the show any good? Because I&apos;ve just been reading these comics now for the past half hour and it&apos;s been great. </string>
                              <string>Very much a repost.</string>
                              <string>If you are not on a family plan there are a lot of good / better options outside the major carriers so I would highly advise against starting a contract if your family is not paying for it. </string>
                              <string>No, I agree with you.  The landlord seems to be concerned with his property, which is his right. While he might be a bit nosy, I don&apos;t see it as invasive- he gave 24 hours notice of inspection.</string>
                              <string>If I was to pick a second team, i think it would have to be NYI as well, they were in my shortlist of who to pick and I like the idea of keeping the orange/blue theme going.  Also, happy cake day!</string>
                              <string>You might use a switch statement:  switch (race) { case 1: cout &amp;lt-&amp;lt- &quot;You are Sir &quot; &amp;lt-&amp;lt- fname &amp;lt-&amp;lt-  &quot; &quot; &amp;lt-&amp;lt- lname &amp;lt-&amp;lt- &quot; The Elf!&quot; &amp;lt-&amp;lt- endl-  case 2: etc. etc...</string>
                              <string>Eh... I&apos;m in the UK and remembrance Sunday isn&apos;t really a big deal unless you are part of a uniformed organisation. My SJA division pressured me to join a march, but beyond that, no-one gave a shit.</string>
                              <string>My fingerprints? lol</string>
                              <string>that is impressive. </string>
                              <string>The spice must flow.</string>
                              <string>The police here are pretty chill. Our street has a speed limit of 30, my roommate always goes at least 45, and she&apos;s never been pulled over in her life.  Plus, I can&apos;t be bothered to do math in my head.</string>
                              <string>people do that in real life? dang, I am glad I haven&apos;t been hired at any of those kind of places. everywhere I have worked I have never had to worry about anything like that, and neither did my coworkers.</string>
                              <string>I actually though Limsa Lominsa is the 2nd easiest to navigate (next to gridania). U&apos;ldah is just the worse. I remember starting in that area and I spent about 3 hours trying to navigate to a city quest. </string>
                              <string>Oh yeah definitely. I used to drink a shitload of black tea, and it was incredibly detrimental and I couldn&apos;t believe the effect it had on me after I stopped. So I watch out for that now. Thanks though really.</string>
                              <string>he doesn&apos;t need to justify it to the people before it happens... that&apos;s the whole point of a republic, it would be impossible to get anything done if the president had to get popular approval for every action.</string>
                              <string>I&apos;m shocked. Really. </string>
                              <string>Happy dirty thirty!!!</string>
                              <string>All you need to do is:  a) provide extremely thorough context for the statement which got you accused of being a racist,  b) not sound like you&apos;re reaching,  c) not accidentally say something else racist while doing so.</string>
                              <string>My dog is so dumb but lovable. She&apos;s walked into walls, trees, cars etc because she&apos;s looking behind her to check if we&apos;re following her. She also keeps trying to eat the hedgehog in our garden. Good luck with that one.</string>
                              <string>Understood! Thank you.</string>
                              <string>So the seed is 13w36b?</string>
                              <string>Which one did you get?</string>
                              <string>I know exactly how you feel. The meds I&apos;m on make me really tired so I find it easy to sleep my days away and hide from my problems. Getting up in the morning feels like climbing Everest and staying up feels like climbing again. </string>
                              <string>The other girl is cute </string>
                              <string>You can&apos;t explain that.</string>
                              <string>Tesla&apos;s work, no doubt. </string>
                              <string>I didn&apos;t know until now.</string>
                              <string>There is always the 360.</string>
                              <string>I like. any more oldies?</string>
                              <string>Made to look like cocaine</string>
                              <string>My classic army LWRC M6a2</string>
                              <string>Don&apos;t forget that tongue!</string>
                              <string>Kia Rio or 2014 Kia Forte</string>
                              <string>Joique Bell, hands down. Even if he doesn&apos;t get a lot of carries, he gets a lot of passes thrown his way. Especially if you are in a ppr league. He&apos;ll get like 30 yds rushing, 60 yds receiving and maybe a TD. His upside is even higher if Bush goes out. </string>
                              <string>I think it means that the contrast between &quot;God speaking by the prophets&quot; and &quot;God speaking by his Son&quot; implies a radical disjunction between the two. Or at least, that God speaking by his Son is qualitatively superior to God speaking by the prophets.</string>
                              <string>Keep at it.  I was in the same boat about a year ago. Gains after 17mph take more and more effort. I am good for 19-20mph for 60+ rides now.   Add distance to the efforts and the shorter ones will get faster as well.   https://www.strava.com/activities/81498963 </string>
                              <string>Looks like he&apos;s wearing 33?</string>
                              <string>I&apos;d enslave marry for sure.</string>
                              <string>sure if you still have one.</string>
                              <string>Seen them live about 7-8 times over the past 4 years. Solid set. I mean if you like pendulum you will most likely hear some of your favorites.  I am still salty about the break up of pendulum though, I have heard it was Rob Swire idea.  Tl-dr: it will be OK. Have fun kid.</string>
                              <string>Small World. I remember you!</string>
                              <string>I don&apos;t believe in Lucifer. </string>
                              <string>He may not a license either.</string>
                              <string>Oh pick me pick me I&apos;ll help</string>
                              <string>Uhhh did you hear what the provincial government of Quebec just proposed? It is called the Quebec Charter of Values and it pretty much is about as ignorant and shocking as what this guy is suggesting, but it is in French.  Turns out we have some pretty crazy wing nuts up in Canada too :(</string>
                              <string>Thanks mine is an e-type ots.</string>
                              <string>You weren&apos;t &quot;friendzoned&quot;, you just ARE FRIENDS. Besides, you said - in front of her - that you don&apos;t want to date her, so I don&apos;t know what you expect? You: I don&apos;t want to date her. Her: Want to go on a date with me?   If you would like to date her, ask her out. It&apos;s really that simple.</string>
                              <string>Very normal. If I want to get my lady in the mood I&apos;ll put in a movie that has one of her favorite male actors in it. I know that she probably thinks about them when we&apos;re getting it on, kinda turns me on to be a proxy for her kinks.    That&apos;s just me. I&apos;m kinda&apos; overly secure about myself though. </string>
                              <string>529</string>
                              <string>No.</string>
                              <string>plant something, like a tree. </string>
                              <string>http://i.imgur.com/BLdfAHo.jpg</string>
                              <string>http://i.imgur.com/mfzjSBe.gif</string>
                              <string>most have a better chance of beating superman 64.   but if a person is a passionate gamer, CRAZY hard-worker, humble, intelligent, etc., then Riot really appreciates that. They are hiring a lot of people right now, so I recommend anyone who fits the aforementioned bill to check out the careers page.</string>
                              <string>Just looking at it I&apos;m baked...</string>
                              <string>But..popping them feels so good</string>
                              <string>I attended a summer music program this year (I&apos;m a pretty damn decent percussionist) and as an orchestra we focused a lot on contemporary composers. Hearing a summerful of atonal music changed my views of what i put and and what i dont. Don&apos;t follow rules because you feel you have to. Create whatever YOU want to.</string>
                              <string>As I mentioned, I don&apos;t think it&apos;s universal.  Plus, some women&apos;s view of other people as potential sex partners change as they get older and have been in a relationship longer.  You might find yourself viewing others differently in the future, and it might not be that you only view your significant other that way.  </string>
                              <string>Yeah that&apos;s a pretty nice trade!</string>
                              <string>#[](/backwardsk)^Aoki K^SanFran </string>
                              <string>while masturbating on the subway</string>
                              <string>Oh damn---well, look, you made this lady-boner, so it&apos;s only fair that now you come take care of it...  kinda odd saying this to a stranger, but your cock is gorgeous,  not veiny and lumpy, but straight, fat, glossy, perfect cockhead. Aesthetically pleasing to the eye, physically pleasing everywhere else,  I just know. =p</string>
                              <string>Eventually. I have some rats right now that I love and I don&apos;t want them to be in fear since I&apos;ll be moving into an apartment this weekend they wouldn&apos;t have separate rooms. My Gf still thinks snakes are evil but I&apos;m hoping to change her mind about it or at least let me have a snake that she doesn&apos;t personally have to hold.</string>
                              <string>After seeing [this TED talk about stress](http://www.ted.com/talks/kelly_mcgonigal_how_to_make_stress_your_friend.html?utm_content=awesm-publisher&amp;amp-utm_medium=on.ted.com-static&amp;amp-utm_campaign=&amp;amp-utm_source=t.co&amp;amp-awesm=on.ted.com_UpsideOfStress), everything else I read/hear about stress seems kind of ... secondary.</string>
                              <string>Would work, but we know him well.</string>
                              <string>So pretty much nothing as usual..</string>
                              <string>There&apos;s an MMO in beta stages right now called Wildstar. The company plasters your username all over the whole entire screen yet the game is still playable. Also, Blizzard has a watermark system that whenever you take a screenshot (using their screenshot software) your name is watermarked yet it&apos;s invisible. It&apos;s some weird stuff...</string>
                              <string>AFAIK the size/shape of our currency is partly designed for the blind.  The edge ridges on a $1 coin, the small but solid weight of the $2 (not the light flat 5c) and the huge 50c.  Our currency has character and is durable as ****.  If notes are way to mangled its as easy as ironing them under a towel and presto they are brand new. </string>
                              <string>Have you been to Wykikamoocau yet?</string>
                              <string>I would pay more to spite a crook.</string>
                              <string>&amp;gt- ... it&apos;s clearly in public interests those of the victim that other victims come forward if they exist.  &amp;gt- ... it&apos;s incredibly important to protect the innocent ...  Then the solution is to find a way to persuade victims to come forward in the first place, rather than hoping in vain to fix the public and the news media&apos;s false take.  </string>
                              <string>Thank you for this. I was raised by a single mother, but I spent time with my father a lot as well. If I hadn&apos;t, that would probably hold true for me too. Children require both parents in a healthy marriage to turn out normal and healthy. A child is not a tool that you use to gain society pity points, keep your man, or live vicariously through.</string>
                              <string>Don&apos;t snatch!  -Vinny, Snatch</string>
                              <string>&amp;gt- Can you imagine? A for-profit company imagining new ways to pay people they don&apos;t need?  Having worked in for-profit companies before, I can promise you- there are *tons* of jobs where the people do next to nothing. Hell, *I&apos;ve* had one of those jobs before. By the time I got another job, I seriously wondered why I was paid at all at my last one.</string>
                              <string>You are supposed to put that after the name of Allah (SWT) which stands for: Subhanahu wa ta&apos;ala. That translates into &quot;May He be Glorified and Exalted&quot;. After the Prophet Muhammed (SAW) you put SAW which stands for: á¹£all AllÄ&#x81;hu Ê¿alay-hi wa-sallam. It translates into &quot;May Allah honor him and grant him peace&quot; or more simply &quot;peace be upon him&quot;.</string>
                              <string>Classic American behavior, actually.</string>
                              <string>they are open now... did you add me?</string>
                              <string>Call me a shithead or a hipster or whatever but I think it&apos;s cool. I actually bought Carcass&apos; new cassette just to experience it. I remember buying Symphonies of Sickness on tape back in the day. It sounded like shit and it was metal as fuck! I like vinyl an CD&apos;s but i can&apos;t wait to hear this. Also I like giving bands money when they offer something I want. </string>
                              <string>Ol&apos; Staff Sarn&apos; gonna get that ass...</string>
                              <string>Yeah why can&apos;t.i see your art work:( </string>
                              <string>you can see out the penthouse windows</string>
                              <string>Do not get Lancer unless it&apos;s the evo</string>
                              <string>Then you provide a source that says otherwise. If you bothered to read the article http://en.wikipedia.org/wiki/Marker_degradation it spells out exactly how production is done for test. Not that it would matter since you clearly have no reading comprehension, your only evidence was citing a pheromone called androstenone synthesized from horse urine and claiming it was an AAS.</string>
                              <string>anyone on xbox wants to play hit me up</string>
                              <string>Directions unclear, key stuck in head.</string>
                              <string>A good lion face always makes me smile</string>
                              <string>But is it tax deductible? I think not.</string>
                              <string>They live in a safe place. Swat Valley</string>
                              <string>[yes?](http://i.imgur.com/TKH6v8Q.gif)</string>
                              <string>I lived in KCMO for less than a year going on 9 years ago now. When I was there it was a complete shithole. We actually broke our lease to leave. Bittersweet, because we found our way back to Canada from there. I now see it has turned around significantly for the better. (not just a picture, but what I have been reading over the past few years). Would love to give er a go again. </string>
                              <string>As a long term smoker this is not unusual. I try to take regular breaks from smoking (once a month for at least a week) to clear my system and get my head straight. Sometimes I take 18 month breaks to get my head straight, it&apos;s just about being in tune with your mind. If you feel fucked up and in a bad place every time you smoke then my advise is just go with it and don&apos;t smoke -)</string>
                              <string>Those are some powerful bright lights. </string>
                              <string>I think Jesse was too emotionally unstable to start cooking in the first place. He&apos;s just not cut out to be a criminal, and he would like to think he&apos;s a better person than he actually is. He can&apos;t seem to grasp the concept that Walter can kill people and ruin lives with no remorse. When your life is in danger, do you set all your moral values aside and do what&apos;s necessary, or do you die?</string>
                              <string>DayZ doesn&apos;t take place in the 80&apos;s you twit. The little story it has implies it takes place a few years from now. Aside from that, I&apos;m not going to bother arguing with you until you formulate real sentences instead of typing like a 14 year old on facebook.   That said I do agree with some of the things you wrote, though your presentation is horrid, makes you come off like a class A asshole. </string>
                              <string>GOAT</string>
                              <string>someone help that poor, retarded child. </string>
                              <string>Waffle? Don&apos;t you mean carrots? HAHAHAHA</string>
                              <string>i&apos;ll look at this too while i&apos;m there :3</string>
                              <string>What ratio corresponds to a faster bike?</string>
                              <string>Most disappointing 16-0 playoff run ever.</string>
                              <string>As a Canadian-American, you&apos;re thankcome.</string>
                              <string>Ok, that one freaked me the hell out man.</string>
                              <string>What did cookie monster ever do to you!?!</string>
                              <string>I was 13 when it happened. It&apos;s never really bothered me, but my parents definitely cared. Now, I look at events like 9/11 and laugh. Don&apos;t ask me why, maybe its how I cope with tragedy, I don&apos;t know. I&apos;m not convinced that 9/11 wasn&apos;t an inside job, but its much easier to believe that it was just a cruel act of a few insane men. Believe what you want I guess, but once you start digging, you really cant go back. </string>
                              <string>You have been banned from /r/TrueChristian</string>
                              <string>It&apos;s supposed to be a solid green light with no blinking whatsoever.  A blinking light indicates a problem, and my first instinct here is that it&apos;s a warranty problem.  Try plugging it into a different power outlet in your house (go outside your room to test).  Try the car adapter that came with the PA v2 as well.  This well help determine exactly where the problem lies, at which step in the chain between the wall and the MFLB.</string>
                              <string>Thanks for the math! this helps tremendously</string>
                              <string>Gus mentions &quot;the lawyer&quot; once or twice.</string>
                              <string>thanks for an answer instead of a downvote. </string>
                              <string>The frequent use of the word feminist in this article kills me. Feminism supports women&apos;s choices, and if a woman wants to stay at home and raise children, feminists support that life style- in fact, they work for a world in which women (and men) will not be judged or discriminated for making that choice. They are perpetuating such a harmful stereotype saying that &quot;bad&quot; feminism forces &quot;good&quot; women out of the home and into the workplace.</string>
                              <string>Waaaaait, do you have a urinal at your house?</string>
                              <string>Over 40 people + a $500 bonus pot in singles.</string>
                              <string>Mmmprobably not. *sticks out tongue playfully*</string>
                              <string>sorry for my ignorance, what game is this for?</string>
                              <string>Of course a guy with a fedora is in the group.</string>
                              <string>It costs the bank incredibly little to store your money, certainly not anywhere near the realm of $15. Besides that, they are not paying almost any of it back to anyone. .5% interest, really banks? They are making tons of money off loans. The banks can afford not to jab the poor people, but they do, because they can.   Not to mention the copious amounts of money they have made because of deregulation and the FED giving them near 0% loans, to load out to people. </string>
                              <string>Would that mean that Mike could still be used? </string>
                              <string>Still not sure Watt exactly is going on here...</string>
                              <string>You&apos;re really bashing on a ten year old&apos;s name? </string>
                              <string>No . . . the entire world revolves around ALLAH.</string>
                              <string>Nothing I&apos;ve seen has beaten [Yu-Gi-Oh! Zexal](http://www.google.com/imgres?imgurl=http://images4.wikia.nocookie.net/__cb20120905022614/yugiohenespanol/es/images/6/6b/Yugioh_zexal_20.jpg&amp;amp-imgrefurl=http://es.yugioh.wikia.com/wiki/Yu-Gi-Oh!_ZEXAL&amp;amp-h=1024&amp;amp-w=1280&amp;amp-sz=647&amp;amp-tbnid=yEH0HX_T3Gue5M:&amp;amp-tbnh=90&amp;amp-tbnw=113&amp;amp-zoom=1&amp;amp-usg=__4aDn5aE2DONiuhK3LiFUbr2NIM0=&amp;amp-docid=PoM-SKH4e_nbyM&amp;amp-sa=X&amp;amp-ei=TA0xUsndEYLvqwGfhYGQDQ&amp;amp-ved=0CGkQ9QEwCA&amp;amp-dur=4967) yet.</string>
                              <string>this is totally gonna happen in humble bundle 10 </string>
                              <string>For me it was all the kids bullying me at school.</string>
                              <string>I too, have hunted all over the place for one =( </string>
                              <string>This is from the 9/11 Memorial Museum near ground zero. I went in 2009, and the atmosphere was very somber. No one said a word, even the kids were quiet. They had a wall set up where you could listen to the emergency response team communications, and other plaques like this one with the voice recording that went with it. Just a glimpse of what those people endured, and the way it was remembered, is very powerful. I felt deeply sad by the time we&apos;d made our way through the whole place...</string>
                              <string>BIRD!</string>
                              <string>Agree</string>
                              <string>Ooey.</string>
                              <string>Why don&apos;t you go back to your home on whore island</string>
                              <string>What if I told you that this Rick repelled tigers?</string>
                              <string>Uh that would stop all human progress.... forever.</string>
                              <string>I met Ibra when ah fuck it I&apos;ve never met Ibra  :(</string>
                              <string>&amp;gt- are heading to the online world at a rapid pace, and bandwidth to the home needs to increase to deal with it.  This is a really important thing that I think a lot of people are forgetting when it comes to internet speed. It&apos;s not like computers where it&apos;s really simple to upgrade hardware when we need it (e.g. adding a new hard drive). When you&apos;re talking about large infrastructure as this, we *need* to plan it for the future, and in IT, that means making it for usage that may seem ridiculous now.</string>
                              <string>is there a subreddit for this kinda of awesomeness?</string>
                              <string>But not his career...really dropped the ball there. </string>
                              <string>Anything by Wham or George Michael. I love that shit.</string>
                              <string>No. Give me your wand.   OOC: Alright. No problem. :)</string>
                              <string>that&apos;s cool, that&apos;s cool...but no conkers bad fur day</string>
                              <string>Patrol car or head of a shoulder-fired rocket weapon.</string>
                              <string>[I love experiences!](http://i.imgur.com/OrWal7B.jpg)</string>
                              <string>From another of my comments:   &amp;gt-&quot;blogspam&quot; is &quot;an article which copies a majority of its text from another article, with or without attribution, and usually with very little added commentary.&quot; The other sources you mention are not usually that way, in the same fashion that articles from the CATO Institute are rarely (in their case, never) blogspam.   Note that this differentiates from &quot;spam.&quot;   Also note that the liberal site Upworthy was banned from /r/politics soon after it took off because of the same blogspam issue.</string>
                              <string>Talk about not even doing the barest of due diligence.</string>
                              <string>I&apos;d put money on it not being a body. Any takers? 10$?</string>
                              <string>VW tech here. All services up to 30k are free. For now</string>
                              <string>How&apos;s that cultural relativism working out for you?   </string>
                              <string>Pants. Pants pants pants pant. Pants pants pants pants. </string>
                              <string>This line always hits me in the gut, I don&apos;t know why...</string>
                              <string>Hell, I&apos;ve been doing that to my body for a lot longer. </string>
                              <string>COD, halo, mass effect, minecraft, and assassin creed. U?</string>
                              <string>But really I haven&apos;t seen Nod in a couple weeks at least.</string>
                              <string>do you happen to have a gold or secret rare thunder king?</string>
                              <string>I&apos;m a new fan and Planet of Ice was the album recommended to me first so that was the one I listened to first. Talking about an album as a whole with people about that album specifically is perfectly fine even if the person doesn&apos;t know any other albums by that band. I just wanted to talk about the album I recently listened to by a band I just started getting into. Complaining that I haven&apos;t listened to another album by this band is a little unfair. Ill be sure to pick up Menos el Oso whenever I can though as it will be the next album I&apos;ll listen to by this band. </string>
                              <string>I feel like the German bootcamp would be more effective -)</string>
                              <string>All my juice has gone to shit because of our wacky weather in New England. I had three 15ml bottles from a local B&amp;amp-M and they lost their flavor first after three weeks. I&apos;ve got a flavor called Desert, not a clue who it&apos;s made by, almost done with the 30ml bottle and I&apos;ve had it since I started vaping...so five or six weeks for that one. Still has its taste. I went through a 15ml bottle of Impearmint from Seduce Juice in a two weeks, neglecting all the rest of my flavors. Currently my daily vape is Snickerdoodle from Seduce Juice and after two weeks it still has its flavor. </string>
                              <string>Do it!</string>
                              <string>Agreed</string>
                              <string>Much better, now this will keep things organized and simple.</string>
                              <string>Rayman will always be the best helicopter animal in my book </string>
                              <string>That&apos;s enough for me. Gotta wait for the BluRay release now.</string>
                              <string>I&apos;m a girl lol and putting food in me seems like a bad idea ?</string>
                              <string>[Definitely this.](www.youtube.com/watch?v=cs7CW6xDbL8#t=1m0s)</string>
                              <string>could you briefly describe the process of how 1 usd is created?</string>
                              <string>Can of Coke- worked out to seven or eight dollars in Bora Bora.</string>
                              <string>That is the only cupcake/cookie I tried from them. Quite tasty.</string>
                              <string>I still fap to this movie sometimes. Hell i still would do her.</string>
                              <string>Works beautifully. I use the side button to Toggle eraser on/off</string>
                              <string>Plot twist: OP put it on himself and is shamelessly karmawhoring.</string>
                              <string>Yeah, I want something hanging off the bottom of my my phone /s. </string>
                              <string>So nice to come home from work and have dinner waiting for you...</string>
                              <string>Abused women aren&apos;t foolish have. You&apos;re a terribly cruel person :(</string>
                              <string>they will learn the valuable lesson of nothing at all  [](/furious)</string>
                              <string>Thank you, that was the most though I put into writing in a while. :)</string>
                              <string>It&apos;s quite similar to bird law, simple stuff if you know who to ask. </string>
                              <string>Perhaps</string>
                              <string>I came.</string>
                              <string>Socrates, Zidane, Baggio? Maybe though op asked for best players ever.</string>
                              <string>Yea i&apos;d probably take it down a bit to get it to a more uniform length.</string>
                              <string>This lineup seems to be hitting fine without Harper in it. But today...</string>
                              <string>I see. OOC, what rotation do you use for 40 durability items like mats?</string>
                              <string>If you think about it the dude contributes literally NOTHING to society.</string>
                              <string>Crochet ALL the hobo mittens! I will knit them. It will be a team effort.</string>
                              <string>Can I get more info?  Got a $300 infraction and broke as fuck to pay it. </string>
                              <string>Yeah pops really isn&apos;t cynical at all. I was just fuckin around with words</string>
                              <string>I love these guys, but I don&apos;t expect them to make it out of Group stage :(</string>
                              <string>Yeah...      I wouldn&apos;t know, my swimming is to the pool bar normally so...</string>
                              <string>&amp;gt-Here&apos;s my question, after all this typing: should I be mad at our mutual friends? Should I cause a scene about this? Should I even be friends with these people if they are friends of the rapist? Some of them are like, &quot;yeah, I&apos;m trying to remain impartial because I don&apos;t really know what happened,&quot; but others are like, &quot;she&apos;s lying.&quot;  No, don&apos;t remain friends with them. Don&apos;t go to their parties, don&apos;t remain their friends on facebook, cut them out completely. People like this are not people you want in your life, even casually.   Report the sexual assault to the real police. More often than not it seems that the role of campus police is to cover up the fact that any crime took place so that the university&apos;s reputation isn&apos;t damaged.</string>
                              <string>Miller. I get the feeling SD will be playing from behind and passing a lot. </string>
                              <string>I was just in the area when I took the screenshot haha. I was heading north. </string>
                              <string>&amp;gt-Silly child that wasn&apos;t Santa that was Satan. Easy mistake.  Stalin  FTFY</string>
                              <string>If a person does that, it&apos;s a quick way of letting me know they are an idiot. </string>
                              <string>The cross-hatching behind Washington is wrong. It stood out to me immediately.</string>
                              <string>kk. Hopefully it works, I suspect it was just the guy, but we&apos;ll try it anyways</string>
                              <string>Dead, died, will die, guess I better pack my stuff up and start the job hunt...</string>
                              <string>No homo.</string>
                              <string>I concur</string>
                              <string>This one is by far my favorite ever.  http://www.youtube.com/watch?v=fpYrQplZ0VQ</string>
                              <string>Cheetos in Cherry Garcia, I got called &quot;the holy spirit of snacking&quot; for it.</string>
                              <string>Could you give me the original of this so i can use it? Cuz I love this wallpaper.</string>
                              <string>I think it would be better if he cried stony-faced, than his weird facial scrunch.</string>
                              <string>But you can keep the cross around your neck. That was never the cause of violence.</string>
                              <string>I can&apos;t chew on stogies... shit o get finnicky when I get too much spit on them...</string>
                              <string>Until the leaks come out. I am going to use all willpower to avoid watching streams</string>
                              <string>Yep. Most LCDs are 60hz, the best are 144hz, but even mediocre CRTs are like 160hz.</string>
                              <string>Yeah, but when does that ever happen? Once every thousand years? The man needs help.</string>
                              <string>Just apply the cream directly to the anus for 5-6 days to help with the inflammation.</string>
                              <string>Soooo.... Why is this so controversial that the news sites AND imgur are removing it?</string>
                              <string>I don&apos;t necessarily think that the Liberals walk the walk as much as people think on this front.  That&apos;s why they didn&apos;t get my primary vote in the House, and my first Senate preference went to the LDP.  By &quot;smaller government&quot;, I mean a smaller scope, less regulation, and lower taxing and spending.  I believe that all of these contribute to prosperity, as individuals are given more power to decide how best to use their own resources.  There is an entire school of economic thoughtâ&#x80;&#x94;Austrianâ&#x80;&#x94;that subscribes to this view.  It isn&apos;t just about efficiency in running things.  It&apos;s about choosing to build those things in the first place.  I believe that if a strong business case can&apos;t be made for something, then it means it wouldn&apos;t provide enough value to people to warrant being undertaken.  Projects decreed by government fiat circumvent this market test.</string>
                              <string>Whenever TBM&apos;s advocate for teetotalism I like to throw this quote at them:   &quot;It is a mistake to think that Christians ought all to be tee-totallers- Mohammedanism, not Christianity, is the teetotal religion. Of course it may be the duty of a particular Christian, or of any Christian, at a particular time, to abstain from strong drink, either because he is the sort of man who cannot drink at all without drinking too much, or because he is with people who are inclined to drunkenness and must not encourage them by drinking himself. But *the whole point is that he is abstaining, for a good reason, from something which he does not condemn and which he likes to see other people enjoying.* One of the marks of a certain type of bad man is that he cannot give up a thing himself without wanting every one else to give it up.&quot;  -C.S. Lewis (my italics added for emphasis)</string>
                              <string>Darker is a little better. I&apos;m a hairstylist, so that&apos;s why I mentioned your hair colour specifically.  I think that due to your prominent nose (nothing&apos;s wrong with it), how you do your bangs are important.  The colour in this pic is good, but the bangs are a little too straight/flat.  Everyone keeps mentioning pic 5. Of course it&apos;s sexy, but the hair also has a little volume and the bangs have volume with a slight bend and go off to the side a little bit, instead of coming straight down.  I think that pic looks the best.  Your glasses are fine.  I just think that you might appear prettier  if you stay away from super straight, blunt cuts and flat hair.  I would go with more razored stylings and try to keep a little wave or volume in your hair.  I&apos;m similar, I love to have pin straight, super razord/jagged edges, but I look best with kind of wavy, sweet &quot;new girl&quot; hair.</string>
                              <string>Thanks.  </string>
                              <string>The Spurs</string>
                              <string>s-o-c-k-s</string>
                              <string>That&apos;s a great find. Yeah, you did very well with that one. Nice wheels on the Miata, too:</string>
                              <string>I don&apos;t get the hate of the keyboard. I type at about 60 wpm, and have never had an issue.</string>
                              <string>I hate those, they make me feel awkward and uncomfortable and I never know what&apos;s going on.</string>
                              <string>I couldn&apos;t agree more, thankfully the dungeons in this are fun as hell all the way through.</string>
                              <string>I meet everything but the income.  Once I&apos;m done with college I should have that rectified. </string>
                              <string>Malkeru noticed the odd look and followed the girl thinking, &apos;God, this will be so much fun.&apos;</string>
                              <string>Sorry for the late reply, I just got Alice in another trade, so I&apos;ll have to pass on this one.</string>
                              <string>I&apos;m sure you&apos;re right. It was probably just a hologram airplane and some cheap special effects.</string>
                              <string>Denver, but I&apos;d look for a Defense with a better match up. This may not be a low scoring game. </string>
                              <string>Actually, decades are an arbitrary way to categorize culture. Everything bleeds into everything </string>
                              <string>Why?  What possible benefit could this have that would outweigh the risk to the employees lives?</string>
                              <string>thanks.  i am trying to decide if I should try to get insanely fit or just decide to let it go heh</string>
                            </indexToSymbolMap>
                          </nominalMapping>
                        </default>
                      </PolynominalAttribute>
                    </attribute>
                  </AttributeRole>
                  <AttributeRole id="47">
                    <special>false</special>
                    <attribute class="NumericalAttribute" id="48" serialization="custom">
                      <com.rapidminer.example.table.AbstractAttribute>
                        <default>
                          <annotations id="49">
                            <keyValueMap id="50"/>
                          </annotations>
                          <attributeDescription id="51">
                            <name>wordcount</name>
                            <valueType>3</valueType>
                            <blockType>1</blockType>
                            <defaultValue>0.0</defaultValue>
                            <index>3</index>
                          </attributeDescription>
                          <constructionDescription>wordcount</constructionDescription>
                          <statistics class="linked-list" id="52">
                            <NumericalStatistics id="53">
                              <sum>0.0</sum>
                              <squaredSum>0.0</squaredSum>
                              <valueCounter>0</valueCounter>
                            </NumericalStatistics>
                            <WeightedNumericalStatistics id="54">
                              <sum>0.0</sum>
                              <squaredSum>0.0</squaredSum>
                              <totalWeight>0.0</totalWeight>
                              <count>0.0</count>
                            </WeightedNumericalStatistics>
                            <com.rapidminer.example.MinMaxStatistics id="55">
                              <minimum>Infinity</minimum>
                              <maximum>-Infinity</maximum>
                            </com.rapidminer.example.MinMaxStatistics>
                            <UnknownStatistics id="56">
                              <unknownCounter>0</unknownCounter>
                            </UnknownStatistics>
                          </statistics>
                          <transformations id="57"/>
                        </default>
                      </com.rapidminer.example.table.AbstractAttribute>
                    </attribute>
                  </AttributeRole>
                  <AttributeRole id="58">
                    <special>false</special>
                    <attribute class="NumericalAttribute" id="59" serialization="custom">
                      <com.rapidminer.example.table.AbstractAttribute>
                        <default>
                          <annotations id="60">
                            <keyValueMap id="61"/>
                          </annotations>
                          <attributeDescription id="62">
                            <name>length</name>
                            <valueType>4</valueType>
                            <blockType>1</blockType>
                            <defaultValue>0.0</defaultValue>
                            <index>4</index>
                          </attributeDescription>
                          <constructionDescription>length</constructionDescription>
                          <statistics class="linked-list" id="63">
                            <NumericalStatistics id="64">
                              <sum>0.0</sum>
                              <squaredSum>0.0</squaredSum>
                              <valueCounter>0</valueCounter>
                            </NumericalStatistics>
                            <WeightedNumericalStatistics id="65">
                              <sum>0.0</sum>
                              <squaredSum>0.0</squaredSum>
                              <totalWeight>0.0</totalWeight>
                              <count>0.0</count>
                            </WeightedNumericalStatistics>
                            <com.rapidminer.example.MinMaxStatistics id="66">
                              <minimum>Infinity</minimum>
                              <maximum>-Infinity</maximum>
                            </com.rapidminer.example.MinMaxStatistics>
                            <UnknownStatistics id="67">
                              <unknownCounter>0</unknownCounter>
                            </UnknownStatistics>
                          </statistics>
                          <transformations id="68"/>
                        </default>
                      </com.rapidminer.example.table.AbstractAttribute>
                    </attribute>
                  </AttributeRole>
                  <AttributeRole id="69">
                    <special>false</special>
                    <attribute class="NumericalAttribute" id="70" serialization="custom">
                      <com.rapidminer.example.table.AbstractAttribute>
                        <default>
                          <annotations id="71">
                            <keyValueMap id="72"/>
                          </annotations>
                          <attributeDescription id="73">
                            <name>upperCaseLetters</name>
                            <valueType>4</valueType>
                            <blockType>1</blockType>
                            <defaultValue>0.0</defaultValue>
                            <index>5</index>
                          </attributeDescription>
                          <constructionDescription>upperCaseLetters</constructionDescription>
                          <statistics class="linked-list" id="74">
                            <NumericalStatistics id="75">
                              <sum>0.0</sum>
                              <squaredSum>0.0</squaredSum>
                              <valueCounter>0</valueCounter>
                            </NumericalStatistics>
                            <WeightedNumericalStatistics id="76">
                              <sum>0.0</sum>
                              <squaredSum>0.0</squaredSum>
                              <totalWeight>0.0</totalWeight>
                              <count>0.0</count>
                            </WeightedNumericalStatistics>
                            <com.rapidminer.example.MinMaxStatistics id="77">
                              <minimum>Infinity</minimum>
                              <maximum>-Infinity</maximum>
                            </com.rapidminer.example.MinMaxStatistics>
                            <UnknownStatistics id="78">
                              <unknownCounter>0</unknownCounter>
                            </UnknownStatistics>
                          </statistics>
                          <transformations id="79"/>
                        </default>
                      </com.rapidminer.example.table.AbstractAttribute>
                    </attribute>
                  </AttributeRole>
                  <AttributeRole id="80">
                    <special>false</special>
                    <attribute class="NumericalAttribute" id="81" serialization="custom">
                      <com.rapidminer.example.table.AbstractAttribute>
                        <default>
                          <annotations id="82">
                            <keyValueMap id="83"/>
                          </annotations>
                          <attributeDescription id="84">
                            <name>upperCaseLettersCombo</name>
                            <valueType>4</valueType>
                            <blockType>1</blockType>
                            <defaultValue>0.0</defaultValue>
                            <index>6</index>
                          </attributeDescription>
                          <constructionDescription>upperCaseLettersCombo</constructionDescription>
                          <statistics class="linked-list" id="85">
                            <NumericalStatistics id="86">
                              <sum>0.0</sum>
                              <squaredSum>0.0</squaredSum>
                              <valueCounter>0</valueCounter>
                            </NumericalStatistics>
                            <WeightedNumericalStatistics id="87">
                              <sum>0.0</sum>
                              <squaredSum>0.0</squaredSum>
                              <totalWeight>0.0</totalWeight>
                              <count>0.0</count>
                            </WeightedNumericalStatistics>
                            <com.rapidminer.example.MinMaxStatistics id="88">
                              <minimum>Infinity</minimum>
                              <maximum>-Infinity</maximum>
                            </com.rapidminer.example.MinMaxStatistics>
                            <UnknownStatistics id="89">
                              <unknownCounter>0</unknownCounter>
                            </UnknownStatistics>
                          </statistics>
                          <transformations id="90"/>
                        </default>
                      </com.rapidminer.example.table.AbstractAttribute>
                    </attribute>
                  </AttributeRole>
                  <AttributeRole id="91">
                    <special>false</special>
                    <attribute class="NumericalAttribute" id="92" serialization="custom">
                      <com.rapidminer.example.table.AbstractAttribute>
                        <default>
                          <annotations id="93">
                            <keyValueMap id="94"/>
                          </annotations>
                          <attributeDescription id="95">
                            <name>sentencePunctuations</name>
                            <valueType>4</valueType>
                            <blockType>1</blockType>
                            <defaultValue>0.0</defaultValue>
                            <index>7</index>
                          </attributeDescription>
                          <constructionDescription>sentencePunctuations</constructionDescription>
                          <statistics class="linked-list" id="96">
                            <NumericalStatistics id="97">
                              <sum>0.0</sum>
                              <squaredSum>0.0</squaredSum>
                              <valueCounter>0</valueCounter>
                            </NumericalStatistics>
                            <WeightedNumericalStatistics id="98">
                              <sum>0.0</sum>
                              <squaredSum>0.0</squaredSum>
                              <totalWeight>0.0</totalWeight>
                              <count>0.0</count>
                            </WeightedNumericalStatistics>
                            <com.rapidminer.example.MinMaxStatistics id="99">
                              <minimum>Infinity</minimum>
                              <maximum>-Infinity</maximum>
                            </com.rapidminer.example.MinMaxStatistics>
                            <UnknownStatistics id="100">
                              <unknownCounter>0</unknownCounter>
                            </UnknownStatistics>
                          </statistics>
                          <transformations id="101"/>
                        </default>
                      </com.rapidminer.example.table.AbstractAttribute>
                    </attribute>
                  </AttributeRole>
                  <AttributeRole id="102">
                    <special>false</special>
                    <attribute class="NumericalAttribute" id="103" serialization="custom">
                      <com.rapidminer.example.table.AbstractAttribute>
                        <default>
                          <annotations id="104">
                            <keyValueMap id="105"/>
                          </annotations>
                          <attributeDescription id="106">
                            <name>sentencePunctuationsCombo</name>
                            <valueType>4</valueType>
                            <blockType>1</blockType>
                            <defaultValue>0.0</defaultValue>
                            <index>8</index>
                          </attributeDescription>
                          <constructionDescription>sentencePunctuationsCombo</constructionDescription>
                          <statistics class="linked-list" id="107">
                            <NumericalStatistics id="108">
                              <sum>0.0</sum>
                              <squaredSum>0.0</squaredSum>
                              <valueCounter>0</valueCounter>
                            </NumericalStatistics>
                            <WeightedNumericalStatistics id="109">
                              <sum>0.0</sum>
                              <squaredSum>0.0</squaredSum>
                              <totalWeight>0.0</totalWeight>
                              <count>0.0</count>
                            </WeightedNumericalStatistics>
                            <com.rapidminer.example.MinMaxStatistics id="110">
                              <minimum>Infinity</minimum>
                              <maximum>-Infinity</maximum>
                            </com.rapidminer.example.MinMaxStatistics>
                            <UnknownStatistics id="111">
                              <unknownCounter>0</unknownCounter>
                            </UnknownStatistics>
                          </statistics>
                          <transformations id="112"/>
                        </default>
                      </com.rapidminer.example.table.AbstractAttribute>
                    </attribute>
                  </AttributeRole>
                  <AttributeRole id="113">
                    <special>true</special>
                    <specialName>label</specialName>
                    <attribute class="BinominalAttribute" id="114" serialization="custom">
                      <com.rapidminer.example.table.AbstractAttribute>
                        <default>
                          <annotations id="115">
                            <keyValueMap id="116"/>
                          </annotations>
                          <attributeDescription id="117">
                            <name>class</name>
                            <valueType>6</valueType>
                            <blockType>1</blockType>
                            <defaultValue>0.0</defaultValue>
                            <index>9</index>
                          </attributeDescription>
                          <constructionDescription>class</constructionDescription>
                          <statistics class="linked-list" id="118">
                            <NominalStatistics id="119">
                              <mode>-1</mode>
                              <maxCounter>0</maxCounter>
                            </NominalStatistics>
                            <UnknownStatistics id="120">
                              <unknownCounter>0</unknownCounter>
                            </UnknownStatistics>
                          </statistics>
                          <transformations id="121"/>
                        </default>
                      </com.rapidminer.example.table.AbstractAttribute>
                      <BinominalAttribute>
                        <default>
                          <nominalMapping class="BinominalMapping" id="122">
                            <firstValue>bad</firstValue>
                            <secondValue>good</secondValue>
                          </nominalMapping>
                        </default>
                      </BinominalAttribute>
                    </attribute>
                  </AttributeRole>
                </attributes>
              </attributes>
            </default>
          </com.rapidminer.example.set.HeaderExampleSet>
        </headerExampleSet>
      </default>
    </com.rapidminer.operator.AbstractModel>
    <TreeModel>
      <default>
        <root id="123">
          <children class="linked-list" id="124">
            <com.rapidminer.operator.learner.tree.Edge id="125">
              <condition class="com.rapidminer.operator.learner.tree.GreaterSplitCondition" id="126">
                <attributeName>upperCaseLetters</attributeName>
                <value>0.25390625</value>
              </condition>
              <child id="127">
                <children class="linked-list" id="128">
                  <com.rapidminer.operator.learner.tree.Edge id="129">
                    <condition class="com.rapidminer.operator.learner.tree.GreaterSplitCondition" id="130">
                      <attributeName>length</attributeName>
                      <value>4.5</value>
                    </condition>
                    <child id="131">
                      <children class="linked-list" id="132">
                        <com.rapidminer.operator.learner.tree.Edge id="133">
                          <condition class="com.rapidminer.operator.learner.tree.GreaterSplitCondition" id="134">
                            <attributeName>upperCaseLetters</attributeName>
                            <value>13.0</value>
                          </condition>
                          <child id="135">
                            <label>good</label>
                            <children class="linked-list" id="136"/>
                            <counterMap class="linked-hash-map" id="137">
                              <entry>
                                <string>bad</string>
                                <int>3</int>
                              </entry>
                              <entry>
                                <string>good</string>
                                <int>6</int>
                              </entry>
                            </counterMap>
                          </child>
                        </com.rapidminer.operator.learner.tree.Edge>
                        <com.rapidminer.operator.learner.tree.Edge id="138">
                          <condition class="com.rapidminer.operator.learner.tree.LessEqualsSplitCondition" id="139">
                            <attributeName>upperCaseLetters</attributeName>
                            <value>13.0</value>
                          </condition>
                          <child id="140">
                            <label>bad</label>
                            <children class="linked-list" id="141"/>
                            <counterMap class="linked-hash-map" id="142">
                              <entry>
                                <string>bad</string>
                                <int>118</int>
                              </entry>
                              <entry>
                                <string>good</string>
                                <int>1</int>
                              </entry>
                            </counterMap>
                          </child>
                        </com.rapidminer.operator.learner.tree.Edge>
                      </children>
                      <counterMap class="linked-hash-map" id="143"/>
                    </child>
                  </com.rapidminer.operator.learner.tree.Edge>
                  <com.rapidminer.operator.learner.tree.Edge id="144">
                    <condition class="com.rapidminer.operator.learner.tree.LessEqualsSplitCondition" id="145">
                      <attributeName>length</attributeName>
                      <value>4.5</value>
                    </condition>
                    <child id="146">
                      <label>good</label>
                      <children class="linked-list" id="147"/>
                      <counterMap class="linked-hash-map" id="148">
                        <entry>
                          <string>bad</string>
                          <int>0</int>
                        </entry>
                        <entry>
                          <string>good</string>
                          <int>2</int>
                        </entry>
                      </counterMap>
                    </child>
                  </com.rapidminer.operator.learner.tree.Edge>
                </children>
                <counterMap class="linked-hash-map" id="149"/>
              </child>
            </com.rapidminer.operator.learner.tree.Edge>
            <com.rapidminer.operator.learner.tree.Edge id="150">
              <condition class="com.rapidminer.operator.learner.tree.LessEqualsSplitCondition" id="151">
                <attributeName>upperCaseLetters</attributeName>
                <value>0.25390625</value>
              </condition>
              <child id="152">
                <label>good</label>
                <children class="linked-list" id="153"/>
                <counterMap class="linked-hash-map" id="154">
                  <entry>
                    <string>bad</string>
                    <int>179</int>
                  </entry>
                  <entry>
                    <string>good</string>
                    <int>291</int>
                  </entry>
                </counterMap>
              </child>
            </com.rapidminer.operator.learner.tree.Edge>
          </children>
          <counterMap class="linked-hash-map" id="155"/>
        </root>
      </default>
    </TreeModel>
  </TreeModel>
</object-stream>