import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Scanner;

public class Process {

	private static int	BODY_INDEX	= 2;

	public static void main(final String[] args) throws FileNotFoundException {
		final File fin = new File(args[0]);
		for (final File in : fin.listFiles()) {
			final File out = new File(in.getParentFile(), in.getName() + "processed.csv");
			final PrintStream sout = new PrintStream(out);
			final Scanner sin = new Scanner(in);
			sout.println(sin.nextLine() + ";wordcount;length;upperCaseLetters;upperCaseLettersCombo;sentencePunctuations;sentencePunctuationsCombo;"); // copy header
			while (sin.hasNextLine()) {
				try {
					final String line = sin.nextLine();
					final String[] parts = line.split(";");
					final String body = parts[Process.BODY_INDEX];
					final String[] words = body.split(" +");
					final int wordcount = words.length;
					final float letters = body.length();
					final float upperCaseLetters = Process.countUppercase(body) / letters;
					final float upperCaseLettersWithoutSpace = Process.countUpperCaseCombo(body);
					final float sentencePunctuations = Process.countPunctuations(body) / letters;
					final float sentencePunctuationsWithoutSpace = Process.countPunctuationsCombo(body);
					sout.println(line + ";" + wordcount + ";" + letters + ";" + upperCaseLetters + ";" + upperCaseLettersWithoutSpace + ";" + sentencePunctuations + ";" + sentencePunctuationsWithoutSpace);
				} catch (final Exception e) {
					e.printStackTrace();
				}
			}
			sin.close();
			sout.close();
		}
	}

	private static int countPunctuationsCombo(final String body) {
		int thyngis = 0;
		int maxthyngis = 0;
		for (int i = 0; i < body.length(); i++) {
			if (body.charAt(i) == '.' || body.charAt(i) == ',' || body.charAt(i) == ':' || body.charAt(i) == ';' || body.charAt(i) == '!' || body.charAt(i) == '?') {
				thyngis++;
			} else {
				if (thyngis > maxthyngis) {
					maxthyngis = thyngis;
				}
				thyngis = 0;
			}
		}
		return maxthyngis;
	}

	private static int countPunctuations(final String body) {
		int thyngis = 0;
		for (int i = 0; i < body.length(); i++) {
			if (body.charAt(i) == '.' || body.charAt(i) == ',' || body.charAt(i) == ':' || body.charAt(i) == ';' || body.charAt(i) == '!' || body.charAt(i) == '?') {
				thyngis++;
			}
		}
		return thyngis;
	}

	private static int countUppercase(final String body) {
		int upperCaseCount = 0;
		for (int i = 0; i < body.length(); i++) {
			for (char c = 'A'; c <= 'Z'; c++) {
				if (body.charAt(i) == c) {
					upperCaseCount++;
				}
			}
		}
		return upperCaseCount;
	}

	private static int countUpperCaseCombo(final String body) {
		int maxupperCaseCount = 0;
		int upperCaseCount = 0;
		for (int i = 0; i < body.length(); i++) {
			if (Character.isUpperCase(body.charAt(i))) {
				upperCaseCount++;
			} else {
				if (upperCaseCount > maxupperCaseCount) {
					maxupperCaseCount = upperCaseCount;
				}
				upperCaseCount = 0;

			}
		}
		return maxupperCaseCount;
	}
}
